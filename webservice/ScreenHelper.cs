﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class ScreenHelper
    {
        public static List<ScreenModel> CovertScreens(List<Screen> screens)
        {
            var screenModels = screens.ConvertAll(sc => new ScreenModel
            {
                ScreenID = sc.ScreenID,
                ScreenName=sc.ScreenName,
                ScreenNameEN=sc.ScreenNameEN,
                Ordinal=sc.Ordinal,

            });

            return screenModels;
        }
    }
}
