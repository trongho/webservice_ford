﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class WRHeaderHelper
    {
        public static List<WRHeaderModel> Covert(List<WRHeader> entrys)
        {
            var models = entrys.ConvertAll(sc => new WRHeaderModel
            {
                WRNumber = sc.WRNumber,
                ModalityID = sc.ModalityID,
                ModalityName = sc.ModalityName,
                WRDate = sc.WRDate,
                ReferenceNumber = sc.ReferenceNumber,
                WRRNumber = sc.WRRNumber,
                WRRReference = sc.WRRReference,
                WarehouseID = sc.WarehouseID,
                WarehouseName = sc.WarehouseName,
                SupplierID = sc.SupplierID,
                SupplierName = sc.SupplierName,
                CustomerID = sc.CustomerID,
                CustomerName = sc.CustomerName,
                OrderNumber = sc.OrderNumber,
                OrderDate = sc.OrderDate,
                ContractNumber = sc.ContractNumber,
                ContractDate = sc.ContractDate,
                IONumber = sc.IONumber,
                PriceTypeID = sc.PriceTypeID,
                PriceTypeName = sc.PriceTypeName,
                PaymentTypeID = sc.PaymentTypeID,
                PaymentTypeName = sc.PaymentTypeName,
                HandlingStatusID = sc.HandlingStatusID,
                HandlingStatusName = sc.HandlingStatusName,
                BranchID = sc.BranchID,
                BranchName = sc.BranchName,
                LocalOrderType = sc.LocalOrderType,
                TotalDiscountAmount = sc.TotalDiscountAmount,
                TotalVATAmount = sc.TotalVATAmount,
                TotalAmountExcludedVAT = sc.TotalAmountExcludedVAT,
                TotalAmountIncludedVAT = sc.TotalAmountIncludedVAT,
                TotalAmount = sc.TotalAmount,
                TotalQuantity = sc.TotalQuantity,
                Note = sc.Note,
                CheckerID1 = sc.CheckerID1,
                CheckerName1 = sc.CheckerName1,
                CheckerID2 = sc.CheckerID2,
                CheckerName2 = sc.CheckerName2,
                Status = sc.Status,
                CreatedUserID = sc.CreatedUserID,
                CreatedDate = sc.CreatedDate,
                UpdatedUserID = sc.UpdatedUserID,
                UpdatedDate = sc.UpdatedDate,
            });

            return models;
        }       
    }
}
