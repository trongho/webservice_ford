﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace webservice.Helpers
{
    public class RGBLightHelper
    {
        [DllImport("Ux64_dllc.dll")]
        public static extern void Usb_Qu_Open();
        [DllImport("Ux64_dllc.dll")]
        public static extern void Usb_Qu_Close();
        [DllImport("Ux64_dllc.dll")]
        public static unsafe extern bool Usb_Qu_write(byte Q_index, byte Q_type, byte* pQ_data);
        [DllImport("Ux64_dllc.dll")]
        public static extern int Usb_Qu_Getstate();

        public static void turnOnGreen()
        {
            const byte C_lampoff = 0;
            const byte C_lampon = 1;
            const byte C_lampblink = 2;
            const byte C_D_not = 100;  //  // Don't care  // Do not change before state



            bool bchk = false;
            unsafe
            {
                byte* bbb = stackalloc byte[6];
                bbb[0] = C_lampoff;
                bbb[1] = C_lampoff;
                bbb[2] = C_lampblink;
                bbb[3] = C_lampoff;
                bbb[4] = C_lampoff;

                bbb[5] = 3; // sound select
                bchk = Usb_Qu_write(0, 0, bbb);
            }
        }

        public static void turnOnRed()
        {
            const byte C_lampoff = 0;
            const byte C_lampon = 1;
            const byte C_lampblink = 2;
            const byte C_D_not = 100;  //  // Don't care  // Do not change before state



            bool bchk = false;
            unsafe
            {
                byte* bbb = stackalloc byte[6];
                bbb[0] = C_lampblink;
                bbb[1] = C_lampoff;
                bbb[2] = C_lampoff;
                bbb[3] = C_lampoff;
                bbb[4] = C_lampoff;

                bbb[5] = 3; // sound select
                bchk = Usb_Qu_write(0, 0, bbb);
            }
        }

        public static void turnOnYellow()
        {
            const byte C_lampoff = 0;
            const byte C_lampon = 1;
            const byte C_lampblink = 2;
            const byte C_D_not = 100;  //  // Don't care  // Do not change before state



            bool bchk = false;
            unsafe
            {
                byte* bbb = stackalloc byte[6];
                bbb[0] = C_lampoff;
                bbb[1] = C_lampblink;
                bbb[2] = C_lampoff;
                bbb[3] = C_lampoff;
                bbb[4] = C_lampoff;

                bbb[5] = 3; // sound select
                bchk = Usb_Qu_write(0, 0, bbb);
            }
        }

        public static void turnOnGreenLampon()
        {
            const byte C_lampoff = 0;
            const byte C_lampon = 1;
            const byte C_lampblink = 2;
            const byte C_D_not = 100;  //  // Don't care  // Do not change before state



            bool bchk = false;
            unsafe
            {
                byte* bbb = stackalloc byte[6];
                bbb[0] = C_lampoff;
                bbb[1] = C_lampoff;
                bbb[2] = C_lampon;
                bbb[3] = C_lampoff;
                bbb[4] = C_lampoff;

                bbb[5] = 3; // sound select
                bchk = Usb_Qu_write(0, 0, bbb);
            }
        }

        public static void turnOnRedLampon()
        {
            const byte C_lampoff = 0;
            const byte C_lampon = 1;
            const byte C_lampblink = 2;
            const byte C_D_not = 100;  //  // Don't care  // Do not change before state



            bool bchk = false;
            unsafe
            {
                byte* bbb = stackalloc byte[6];
                bbb[0] = C_lampon;
                bbb[1] = C_lampoff;
                bbb[2] = C_lampoff;
                bbb[3] = C_lampoff;
                bbb[4] = C_lampoff;

                bbb[5] = 3; // sound select
                bchk = Usb_Qu_write(0, 0, bbb);
            }
        }

        public static void turnOnYellowLampon()
        {
            const byte C_lampoff = 0;
            const byte C_lampon = 1;
            const byte C_lampblink = 2;
            const byte C_D_not = 100;  //  // Don't care  // Do not change before state



            bool bchk = false;
            unsafe
            {
                byte* bbb = stackalloc byte[6];
                bbb[0] = C_lampoff;
                bbb[1] = C_lampon;
                bbb[2] = C_lampoff;
                bbb[3] = C_lampoff;
                bbb[4] = C_lampoff;

                bbb[5] = 3; // sound select
                bchk = Usb_Qu_write(0, 0, bbb);
            }
        }


        public static void turnOff()
        {
            const byte C_lampoff = 0;
            const byte C_lampon = 1;
            const byte C_lampblink = 2;
            const byte C_D_not = 100;  //  // Don't care  // Do not change before state



            bool bchk = false;
            unsafe
            {
                byte* bbb = stackalloc byte[6];
                bbb[0] = C_lampoff;
                bbb[1] = C_lampoff;
                bbb[2] = C_lampoff;
                bbb[3] = C_lampoff;
                bbb[4] = C_lampoff;

                bbb[5] = 0; // sound select
                bchk = Usb_Qu_write(0, 0, bbb);
            }
        }
    }
}
