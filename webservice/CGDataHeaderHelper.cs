﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class CGDataHeaderHelper
    {
        public static List<CGDataHeaderModel> Covert(List<CGDataHeader> entrys)
        {
            var models = entrys.ConvertAll(sc => new CGDataHeaderModel
            {
                CGDNumber = sc.CGDNumber,
                CGDDate = sc.CGDDate,
                ReferenceNumber = sc.ReferenceNumber,
                WRRNumber = sc.WRRNumber,
                WRRReference = sc.WRRReference,
                HandlingStatusID = sc.HandlingStatusID,
                HandlingStatusName = sc.HandlingStatusName,
                Note = sc.Note,
                BranchID = sc.BranchID,
                BranchName = sc.BranchName,
                TotalQuantity = sc.TotalQuantity,
                TotalQuantityOrg = sc.TotalQuantityOrg,
                Status = sc.Status,
                CreatedUserID = sc.CreatedUserID,
                CreatedDate = sc.CreatedDate,
                UpdatedUserID = sc.UpdatedUserID,
                UpdatedDate = sc.UpdatedDate,
            });

            return models;
        }
    }
}
