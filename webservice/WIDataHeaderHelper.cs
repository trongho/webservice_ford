﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class WIDataHeaderHelper
    {
        public static List<WIDataHeaderModel> Covert(List<WIDataHeader> entrys)
        {
            var models = entrys.ConvertAll(sc => new WIDataHeaderModel
            {
                WIDNumber = sc.WIDNumber,
                WIDDate = sc.WIDDate,
                ReferenceNumber = sc.ReferenceNumber,
                WIRNumber = sc.WIRNumber,
                WIRReference = sc.WIRReference,
                HandlingStatusID = sc.HandlingStatusID,
                HandlingStatusName = sc.HandlingStatusName,
                Note = sc.Note,
                BranchID = sc.BranchID,
                BranchName = sc.BranchName,
                TotalQuantity = sc.TotalQuantity,
                TotalQuantityOrg = sc.TotalQuantityOrg,
                Status = sc.Status,
                CreatedUserID = sc.CreatedUserID,
                CreatedDate = sc.CreatedDate,
                UpdatedUserID = sc.UpdatedUserID,
                UpdatedDate = sc.UpdatedDate,
            });

            return models;
        }
    }
}
