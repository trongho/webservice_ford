﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class TSDataGeneralHelper
    {
        public static List<TSDataGeneralModel> Covert(List<TSDataGeneral> entrys)
        {
            var models = entrys.ConvertAll(sc => new TSDataGeneralModel
            {
              BranchID=sc.BranchID,
              WarehouseID=sc.WarehouseID,
              WarehouseName=sc.WarehouseName,
              TallyDate=sc.TallyDate,
              Ordinal=sc.Ordinal,
              GoodsID=sc.GoodsID,
              GoodsName=sc.GoodsName,
              StockUnitID=sc.StockUnitID,
              TallyPrice=sc.TallyPrice,
              TallyQuantity=sc.TallyQuantity,
              TallyAmount=sc.TallyAmount,
              LedgerQuantity=sc.LedgerQuantity,
              LedgerAmount=sc.LedgerAmount,
              DifferenceQuantity=sc.DifferenceQuantity,
              DifferenceAmount=sc.DifferenceAmount,
              Status=sc.Status,

            });

            return models;
        }
    }
}
