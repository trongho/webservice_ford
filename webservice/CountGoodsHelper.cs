﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class CountGoodsHelper
    {
        public static List<CountGoodsModel> Covert(List<CountGoods> entrys)
        {
            var models = entrys.ConvertAll(sc => new CountGoodsModel
            {
                LocationID=sc.LocationID,
                GoodsID=sc.GoodsID,
                GoodsName=sc.GoodsName,
                Quantity=sc.Quantity,
                Status=sc.Status,
                CountDate=sc.CountDate,
            });

            return models;
        }
    }
}
