﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class InventoryDataHelper
    {
        public static List<InventoryDataModel> Covert(List<InventoryData> entrys)
        {
            var models = entrys.ConvertAll(sc => new InventoryDataModel
            {
                GoodsID = sc.GoodsID,              
                GoodsName = sc.GoodsName,
                Description = sc.Description,
                LocationID=sc.LocationID,
                Quantity=sc.Quantity,
                Amount=sc.Amount,
                Status = sc.Status,               
                CreatedUserID = sc.CreatedUserID,
                CreatedDate = sc.CreatedDate,
                UpdatedUserID = sc.UpdatedUserID,
                UpdatedDate = sc.UpdatedDate,

            });

            return models;
        }
    }
}
