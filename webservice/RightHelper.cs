﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class RightHelper
    {
        public static List<RightModel> CovertRights(List<Right> rights)
        {
            var rightModels = rights.ConvertAll(right => new RightModel
            {
                RightID = right.RightID,
                Ordinal= right.Ordinal,
                RightName = right.RightName,
                RightNameEN = right.RightNameEN,
            });

            return rightModels;
        }
    }
}
