﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class DataPrintGeneralHelper
    {
        public static List<DataPrintGeneralModel> Covert(List<DataPrintGeneral> entrys)
        {
            var models = entrys.ConvertAll(sc => new DataPrintGeneralModel
            {
                 DataPrintNumber=sc.DataPrintNumber,
                 GoodsID=sc.GoodsID,
                 Ordinal=sc.Ordinal,
                 GoodsName=sc.GoodsName,
                 Quantity=sc.Quantity,
                 Status=sc.Status,
                 PackingVolume=sc.PackingVolume,
                 PackingQuantity=sc.PackingQuantity,
            });

            return models;
        }
    }
}
