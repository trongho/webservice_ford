﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class DataPrintDetailHelper
    {
        public static List<DataPrintDetailModel> Covert(List<DataPrintDetail> entrys)
        {
            var models = entrys.ConvertAll(sc => new DataPrintDetailModel
            {
                DataPrintNumber = sc.DataPrintNumber,
                GoodsID = sc.GoodsID,
                Ordinal = sc.Ordinal,
                Quantity = sc.Quantity,
                Status = sc.Status,
                DateString=sc.DateString,
            });

            return models;
        }
    }
}
