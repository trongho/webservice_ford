﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class UserRightOnScreenHelper
    {
        public static List<UserRightOnScreenModel> Covert(List<UserRightOnScreen> entrys)
        {
            var models = entrys.ConvertAll(sc => new UserRightOnScreenModel
            {
                UserID=sc.UserID,
                Ordinal = sc.Ordinal,
                ScreenID = sc.ScreenID,
                ScreenName = sc.ScreenName,
                ScreenNameEN = sc.ScreenNameEN,
                SaveNewRight=sc.SaveNewRight,
                SaveChangeRight=sc.SaveChangeRight,
                DeleteRight=sc.DeleteRight,
                PrintRight=sc.PrintRight,
                ImportRight=sc.ImportRight,
                ExportRight=sc.ExportRight,

            });

            return models;
        }
    }
}
