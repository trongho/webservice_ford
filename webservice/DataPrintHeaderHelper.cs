﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class DataPrintHeaderHelper
    {
        public static List<DataPrintHeaderModel> Covert(List<DataPrintHeader> entrys)
        {
            var models = entrys.ConvertAll(sc => new DataPrintHeaderModel
            {
                  DataPrintNumber=sc.DataPrintNumber,
                  DataPrintDate=sc.DataPrintDate,
                  Status=sc.Status,
                  TotalGoodsID=sc.TotalGoodsID,
                  TotalGoodsIDPrinted=sc.TotalGoodsIDPrinted,
                  TotalLabel=sc.TotalLabel,
                  TotalLabelPrinted=sc.TotalLabelPrinted,
                  CreatedUserID=sc.CreatedUserID,
                  CreatedDate=sc.CreatedDate,
                  UpdatedUserID=sc.UpdatedUserID,
                  UpdatedDate=sc.UpdatedDate,
                  HandlingStatusID=sc.HandlingStatusID,
                  HandlingStatusName=sc.HandlingStatusName,
            });

            return models;
        }
    }
}
