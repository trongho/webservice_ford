﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class WIDataGeneralHelper
    {
        public static List<WIDataGeneralModel> Covert(List<WIDataGeneral> entrys)
        {
            var models = entrys.ConvertAll(sc => new WIDataGeneralModel
            {
                WIDNumber = sc.WIDNumber,
                Ordinal = sc.Ordinal,
                GoodsID = sc.GoodsID,
                GoodsGroupID=sc.GoodsGroupID,
                PickerName=sc.PickerName,
                GoodsName = sc.GoodsName,
                IDCode = sc.IDCode,
                LocationID = sc.LocationID,
                Quantity = sc.Quantity,
                TotalQuantity = sc.TotalQuantity,
                TotalGoods = sc.TotalGoods,
                QuantityOrg = sc.QuantityOrg,
                TotalQuantityOrg = sc.TotalGoodsOrg,
                TotalGoodsOrg = sc.TotalGoodsOrg,
                LocationIDOrg = sc.LocationIDOrg,
                CreatorID = sc.CreatorID,
                CreatedDateTime = sc.CreatedDateTime,
                EditerID = sc.EditerID,
                EditedDateTime = sc.EditedDateTime,
                Status = sc.Status,
                PackingVolume = sc.PackingVolume,
                QuantityByPack = sc.QuantityByPack,
                QuantityByItem = sc.QuantityByItem,
                Note = sc.Note,
                ScanOption = sc.ScanOption,
                PackingQuantity = sc.PackingQuantity,
            });

            return models;
        }
    }
}
