﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IColorService
    {
        Task<List<Color>> GetAll();
        Task<Boolean> Create(Color entry);
        Task<List<Color>> GetUnderId(String id);
        Task<Boolean> Update(String id,Color entry);
        Task<Boolean> Delete(String id);
        Task<Boolean> checkExist(String id);
    }
}
