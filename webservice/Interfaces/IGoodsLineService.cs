﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IGoodsLineService
    {
        Task<List<GoodsLine>> GetAllGoodsLine();
        Task<Boolean> CreateGoodsLine(GoodsLine goodsLine);
        Task<GoodsLine> GetGoodsLineUnderId(String id);
        Task<Boolean> UpdateGoodsLine(String id, GoodsLine goodsLine);
        Boolean DeleteGoodsLine(String id);
        List<GoodsLine> ImportGoodsLine();
        String ExportGoodsLine();
        Task<Boolean> checkExist(String id);
    }
}
