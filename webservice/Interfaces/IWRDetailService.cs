﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IWRDetailService
    {
        Task<List<WRDetail>> GetAll();
        Task<List<WRDetail>> GetUnderId(String id);
        Task<List<WRDetail>> GetUnderId(String wRNumber, String goodsID, int Ordinal);
        Task<Boolean> Create(WRDetail wRDetail);
        Task<Boolean> Update(String wRNumber, String goodsID, int Ordinal, WRDetail wRDetail);
        Task<Boolean> Delete(String wRNumber);
        Task<String> GetLastId();
        Task<Boolean> checkExist(String wRNumber);
        Task<Boolean> checkDataChanged();

    }
}
