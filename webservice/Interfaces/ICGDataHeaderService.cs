﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface ICGDataHeaderService
    {
        Task<List<CGDataHeader>> GetAll();
        Task<List<CGDataHeader>> GetUnderId(String id);
        Task<Boolean> Create(CGDataHeader CGDataHeader);
        Task<Boolean> Update(String id, CGDataHeader CGDataHeader);
        Task<Boolean> Delete(String id);
        Task<String> GetLastId();
        Task<Boolean> checkExist(String id);

        Task<List<CGDataHeader>> GetUnderDate(DateTime fromDate, DateTime toDate);
        Task<List<CGDataHeader>> GetUnderBranchID(String branchID);
        Task<List<CGDataHeader>> GetUnderHandlingStatusID(String handlingStatusID);

        Task<List<CGDataHeader>> GetUnderOrderStatusID(String orderStatusID);
    }
}
