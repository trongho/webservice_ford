﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface ILocationService
    {
        Task<List<Location>> GetAll();
        Task<Boolean> Create(Location entry);
        Task<List<Location>> GetUnderId(String id);
        Task<Boolean> Update(String id, Location entry);
        Task<Boolean> Delete(String id);
        Task<Boolean> checkExist(String id);
    }
}
