﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IUserRightService
    {
        Task<List<UserRight>> GetUserRightByUserId(String id);
        Task<List<UserRight>> GetUserRightByRightId(String id);
        Task<List<UserRight>> GetUserRightByUserRightId(String userId,String rightId);
        Task<Boolean> CreateUserRight(UserRight userRight);
        Task<Boolean> UpdateUserRight(String userId,String rightId, UserRight userRight);
        Task<Boolean> DeleteUserRight(String userId,String rightId);
    }
}
