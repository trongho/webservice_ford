﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IInventoryOfLastMonthsService
    {
        Task<List<InventoryOfLastMonths>> GetAll();
        Task<List<InventoryOfLastMonths>> GetUnderId(Int16 year, Int16 month, String warehouseID);
        Task<List<InventoryOfLastMonths>> GetUnderId(Int16 year, Int16 month, String warehouseID, String goodsID);
        Task<Boolean> Create(InventoryOfLastMonths InventoryOfLastMonths);
        Task<Boolean> Update(Int16 year, Int16 month, String warehouseID, String goodsID, InventoryOfLastMonths InventoryOfLastMonths);
        Task<Boolean> Delete(Int16 year, Int16 month, String warehouseID, String goodsID);
        Task<Boolean> Delete(Int16 year, Int16 month, String warehouseID);
        Task<Boolean> checkExist(Int16 year, Int16 month, String warehouseID);
        Task<Boolean> checkExist(Int16 year, Int16 month, String warehouseID, String goodsID);
    }
}
