﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IWRDataGeneralService
    {
        Task<List<WRDataGeneral>> GetAll();
        Task<List<WRDataGeneral>> GetUnderId(String wRDNumber);
        Task<List<WRDataGeneral>> GetUnderId(String wRDNumber, String goodsID, int Ordinal);
        Task<List<WRDataGeneral>> GetUnderId(String wRDNumber, String goodsID);
        Task<List<WRDataGeneral>> GetUnderIdCode(String wRDNumber, String goodsID, String IDCode);
        Task<Boolean> Create(WRDataGeneral wRDataGeneral);
        Task<Boolean> Update(String wRDNumber, String goodsID, int Ordinal,WRDataGeneral wRDataGeneral);
        Task<Boolean> Delete(String wRDNumber);
        Task<String> GetLastId();
        Task<Boolean> checkExist(String wRDNumber,String goodIDs);
        Task<Boolean> checkGoodsIDDuplicate(String wRDNumber, String goodsID);

        Task<Boolean> checkIDCodeIsScaned(String wrdNumber,String idCode, String goodIDs);

        Task<String> checkQuantity(String wRDNumber, DateTime editedDateTime);

        Task<Decimal?> getTotalQuantity(String wrdNumber);
        Task<int> getTotalGoods(String wrdNumber);

        Task<List<WRDataGeneral>> GetByNgayNhanHang(DateTime fromDate, DateTime toDate);
    }
}
