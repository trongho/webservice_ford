﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IDataPrintDetailService
    {
        Task<List<DataPrintDetail>> GetAll();
        Task<Boolean> Create(DataPrintDetail entry);
        Task<List<DataPrintDetail>> GetUnderId(String id);
        Task<List<DataPrintDetail>> GetUnderId(String id, String goodsID, int ordinal);
        Task<Boolean> Update(String id, String goodsID, int ordinal, DataPrintDetail entry);
        Task<Boolean> Delete(String id);
        Task<Boolean> checkExist(String id, String goodsID, int ordinal);
    }
}
