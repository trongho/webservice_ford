﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IDataPrintHeaderService
    {
        Task<List<DataPrintHeader>> GetAll();
        Task<Boolean> Create(DataPrintHeader entry);
        Task<List<DataPrintHeader>> GetUnderId(String id);
        Task<Boolean> Update(String id, DataPrintHeader entry);
        Task<Boolean> Delete(String id);
        Task<Boolean> checkExist(String id);
        Task<String> GetLastId();
    }
}
