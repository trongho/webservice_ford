﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IWIRHeaderService
    {
        Task<List<WIRHeader>> GetAll();
        Task<List<WIRHeader>> GetUnderId(String id);
        Task<Boolean> Create(WIRHeader wIRHeader);
        Task<Boolean> Update(String id, WIRHeader wIRHeader);
        Task<Boolean> Delete(String id);
        Task<String> GetLastId();
        Task<Boolean> checkExist(String id);
        Task<List<WIRHeader>> GetUnderDate(DateTime fromDate, DateTime toDate);
        Task<List<WIRHeader>> GetUnderBranchID(String branchID);
        Task<List<WIRHeader>> GetUnderHandlingStatusID(String handlingStatusID);

        Task<List<WIRHeader>> Filter(String branchID, DateTime fromDate, DateTime toDate, String handlingStatusID);
        Task<List<WIRHeader>> FilterByDateAndHandlingStatus(DateTime fromDate, DateTime toDate, String handlingStatusID);
        Task<List<WIRHeader>> FilterByDateAndBranch(DateTime fromDate, DateTime toDate, String branchID);
    }
}
