﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IInventoryService
    {
        Task<List<Inventory>> GetAll();
        Task<List<Inventory>> GetUnderId(Int16 year,Int16 month,String warehouseID);
        Task<List<Inventory>> GetUnderId(Int16 year, Int16 month, String warehouseID,String goodsID);
        Task<Boolean> Create(Inventory inventory);
        Task<Boolean> Update(Int16 year, Int16 month, String warehouseID, String goodsID, Inventory inventory);
        Task<Boolean> Delete(Int16 year, Int16 month, String warehouseID, String goodsID);
        Task<Boolean> Delete(Int16 year, Int16 month, String warehouseID);
        Task<Boolean> checkExist(Int16 year, Int16 month, String warehouseID);
        Task<Boolean> checkExist(Int16 year, Int16 month, String warehouseID,String goodsID);

    }
}
