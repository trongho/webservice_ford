﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IDataPrintGeneralService
    {
        Task<List<DataPrintGeneral>> GetAll();
        Task<Boolean> Create(DataPrintGeneral entry);
        Task<List<DataPrintGeneral>> GetUnderId(String id);
        Task<List<DataPrintGeneral>> GetUnderId(String id,String goodsID,int ordinal);
        Task<Boolean> Update(String id, String goodsID, int ordinal, DataPrintGeneral entry);
        Task<Boolean> Delete(String id);
        Task<Boolean> checkExist(String id, String goodsID, int ordinal);
    }
}
