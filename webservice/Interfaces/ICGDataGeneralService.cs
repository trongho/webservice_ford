﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface ICGDataGeneralService
    {
        Task<List<CGDataGeneral>> GetAll();
        Task<List<CGDataGeneral>> GetUnderId(String CGDNumber);
        Task<List<CGDataGeneral>> GetUnderId(String CGDNumber, String goodsID, int Ordinal);
        Task<List<CGDataGeneral>> GetUnderId(String CGDNumber, String goodsID);
        Task<List<CGDataGeneral>> GetUnderIdCode(String CGDNumber, String goodsID, String IDCode);
        Task<Boolean> Create(CGDataGeneral CGDataGeneral);
        Task<Boolean> Update(String CGDNumber, String goodsID, int Ordinal, CGDataGeneral CGDataGeneral);
        Task<Boolean> Delete(String CGDNumber);
        Task<String> GetLastId();
        Task<Boolean> checkExist(String CGDNumber, String goodIDs);
    }
}
