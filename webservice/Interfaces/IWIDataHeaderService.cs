﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IWIDataHeaderService
    {
        Task<List<WIDataHeader>> GetAll();
        Task<List<WIDataHeader>> GetUnderId(String id);
        Task<Boolean> Create(WIDataHeader wIDataHeader);
        Task<Boolean> Update(String id, WIDataHeader wIDataHeader);
        Task<Boolean> Delete(String id);
        Task<String> GetLastId();
        Task<Boolean> checkExist(String id);
        Task<List<WIDataHeader>> GetUnderDate(DateTime fromDate, DateTime toDate);
        Task<List<WIDataHeader>> GetUnderBranchID(String branchID);
        Task<List<WIDataHeader>> GetUnderHandlingStatusID(String handlingStatusID);
        Task<List<WIDataHeader>> GetUnderOrderStatusID(String orderStatusID);

        Task<List<WIDataHeader>> Filter(String branchID, DateTime fromDate, DateTime toDate, String handlingStatusID);
        Task<List<WIDataHeader>> FilterByDateAndHandlingStatus(DateTime fromDate, DateTime toDate, String handlingStatusID);
        Task<List<WIDataHeader>> FilterByDateAndBranch(DateTime fromDate, DateTime toDate, String branchID);

    }
}
