﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IInventoryDataService
    {
        Task<List<InventoryData>> GetAll();
        Task<Boolean> Create(InventoryData entry);
        Task<List<InventoryData>> GetUnderId(String id);
        Task<Boolean> Update(String id, InventoryData entry);
        Task<Boolean> Delete(String id);
        Task<Boolean> checkExist(String id);
    }
}
