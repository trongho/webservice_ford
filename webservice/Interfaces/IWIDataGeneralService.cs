﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IWIDataGeneralService
    {
        Task<List<WIDataGeneral>> GetAll();
        Task<List<WIDataGeneral>> GetUnderId(String wIDNumber);
        Task<List<WIDataGeneral>> GetUnderPickerName(String wIDNumber,String PickerName);
        Task<List<WIDataGeneral>> GetUnderGoodsGroupID(String wIDNumber,String GoodsGroupID);
        Task<List<WIDataGeneral>> GetUnderScanOption(String wIDNumber, Int16 ScanOption);
        Task<List<WIDataGeneral>> Filter(String wIDNumber, String GoodsGroupID, String PickerName, Int16 ScanOption);
        Task<List<WIDataGeneral>> FilterByGoodsGroupIDAndPickerName(String wIDNumber, String GoodsGroupID, String PickerName);
        Task<List<WIDataGeneral>> FilterByGoodsGroupIDAndScanOption(String wIDNumber, String GoodsGroupID, Int16 ScanOption);
        Task<List<WIDataGeneral>> FilterByGoodsPickerNameAndScanOption(String wIDNumber, String PickerName, Int16 ScanOption);
        Task<List<String>> GetGoodsGroupID(String wIDNumber);
        Task<List<String>> GetPickerName(String wIDNumber);
        Task<List<WIDataGeneral>> GetUnderId(String wIDNumber, String goodsID, int Ordinal);
        Task<List<WIDataGeneral>> GetUnderId(String wIDNumber, String goodsID, int Ordinal,String GoodsGroupID);
        Task<List<WIDataGeneral>> GetUnderId(String wIDNumber, String goodsID);
        Task<List<WIDataGeneral>> GetUnderId(String wIDNumber, String goodsID,String GoodsGroupID);
        Task<List<WIDataGeneral>> GetUnderIdCode(String wIDNumber, String goodsID, String IDCode);
        Task<Boolean> Create(WIDataGeneral WIDataGeneral);
        Task<Boolean> Update(String wIDNumber, String goodsID, int Ordinal, String goodsGroupID, WIDataGeneral WIDataGeneral);
        Task<Boolean> Delete(String wIDNumber);
        Task<String> GetLastId();
        Task<Boolean> checkExist(String wIDNumber,String goodsID);
    }
}
