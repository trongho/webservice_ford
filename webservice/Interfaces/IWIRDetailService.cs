﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IWIRDetailService
    {
        Task<List<WIRDetail>> GetAll();
        Task<List<WIRDetail>> GetUnderId(String wIRNumber, String goodsID, int Ordinal);
        Task<List<WIRDetail>> GetUnderId(String wIRNumber);
        Task<Boolean> Create(WIRDetail wIRDetail);
        Task<Boolean> Update(String wIRNumber, String goodsID, int Ordinal, String goodsGroupID, WIRDetail wIRDetail);
        Task<Boolean> Delete(String wIRNumber);
        Task<String> GetLastId();
        Task<Boolean> checkExist(String wIRNumber);
        Task<List<WIRDetail>> GetUnderPickerName(String wIRNumber, String PickerName);
        Task<List<WIRDetail>> GetUnderGoodsGroupID(String wIRNumber, String GoodsGroupID);
        Task<List<WIRDetail>> GetUnderId(String wIRNumber, String goodsID, int Ordinal, String GoodsGroupID);
    }
}
