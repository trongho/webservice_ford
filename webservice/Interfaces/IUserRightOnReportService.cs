﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IUserRightOnReportService
    {
        Task<List<UserRightOnReport>> GetRightByUserId(String id);
        Task<List<UserRightOnReport>> GetRightByReportId(String id);
        Task<List<UserRightOnReport>> GetRightByUserReportId(String userId, String reportId);
        Task<Boolean> CreateUserRightOnReport(UserRightOnReport userRightOnReport);
        Task<Boolean> UpdateUserRightOnReport(String userId, String reportId, UserRightOnReport userRightOnReport);
        Task<Boolean> DeleteUserRightOnReport(String userId, String reportId);
        Task<Boolean> checkExist(String userId, String reportId);
    }
}
