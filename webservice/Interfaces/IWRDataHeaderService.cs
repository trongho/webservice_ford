﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IWRDataHeaderService
    {
        Task<List<WRDataHeader>> GetAll();
        Task<List<WRDataHeader>> GetUnderId(String id);
        Task<Boolean> Create(WRDataHeader wRDataHeader);
        Task<Boolean> Update(String id, WRDataHeader wRDataHeader);
        Task<Boolean> Delete(String id);
        Task<String> GetLastId();
        Task<Boolean> checkExist(String id);

        Task<List<WRDataHeader>> GetUnderDate(DateTime fromDate, DateTime toDate);
        Task<List<WRDataHeader>> GetUnderBranchID(String branchID);
        Task<List<WRDataHeader>> GetUnderHandlingStatusID(String handlingStatusID);

        Task<List<WRDataHeader>> Filter(String branchID,DateTime fromDate, DateTime toDate ,String handlingStatusID);
        Task<List<WRDataHeader>> FilterByDateAndHandlingStatus(DateTime fromDate, DateTime toDate, String handlingStatusID);
        Task<List<WRDataHeader>> FilterByDateAndBranch(DateTime fromDate, DateTime toDate, String branchID);

        Task<List<WRDataHeader>> GetUnderOrderStatusID(String orderStatusID);
        Task<String> checkTotalQuantity(String wRDNumber);
    }
}
