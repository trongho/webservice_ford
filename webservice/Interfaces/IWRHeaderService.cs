﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IWRHeaderService
    {
        Task<List<WRHeader>> GetAll();
        Task<List<WRHeader>> GetUnderId(String id);
        Task<List<WRHeader>> GetUnderWRDate(String monthYear);
        Task<List<WRHeader>> GetUnderLastWRDate(String monthYear);
        Task<Boolean> Create(WRHeader wRHeader);
        Task<Boolean> Update(String id, WRHeader wRHeader);
        Task<Boolean> Delete(String id);
        Task<String> GetLastId();
        Task<Boolean> checkExist(String id);
        Task<List<WRHeader>> GetUnderDate(DateTime fromDate, DateTime toDate);
        Task<List<WRHeader>> GetUnderBranchID(String branchID);
        Task<List<WRHeader>> GetUnderHandlingStatusID(String handlingStatusID);
        Task<List<WRHeader>> GetUnderModalityID(String modalityID);


    }
}
