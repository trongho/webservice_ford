﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IWRDataDetailService
    {
        Task<List<WRDataDetail>> GetAll();
        Task<List<WRDataDetail>> GetUnderId(String wRDNumber);
        Task<List<WRDataDetail>> GetUnderId(String wRDNumber, String goodsID, int Ordinal);
        Task<List<WRDataDetail>> GetUnderIdCode(String wRDNumber, String goodsID, String IDCode);
        Task<Boolean> Create(WRDataDetail wRDataDetail);
        Task<Boolean> Update(String wRDNumber, String goodsID, int Ordinal, WRDataDetail wRDataDetail);
        Task<Boolean> Delete(String wRDNumber);
        Task<String> GetLastId();
        Task<Boolean> checkExist(String wRDNumber,String goodIDs);
    }
}
