﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface ICountGoodsService
    {
        Task<List<CountGoods>> GetAll();
        Task<List<CountGoods>> GetUnderId(String locationID);
        Task<List<CountGoods>> GetUnderId(String locationID, String goodsID);
        Task<Boolean> Create(CountGoods CountGoods);
        Task<Boolean> Update(String locationID, String goodsID, CountGoods CountGoods);
        Task<Boolean> Delete(String locationID);
        Task<Boolean> Delete(String locationID, String goodsID);
       
        Task<Boolean> checkExist(String locationID, String goodIDs);
    }
}
