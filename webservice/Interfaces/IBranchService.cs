﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IBranchService
     {
        Task<List<Branch>> GetAll();
        Task<List<Branch>> GetUnderId(String id);
        Task<Boolean> Create(Branch branch);
        Task<Boolean> Update(String id, Branch branch);
        Task<Boolean> Delete(String id);
        Task<Boolean> checkExist(String id);
    }
}
