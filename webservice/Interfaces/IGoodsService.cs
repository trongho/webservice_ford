﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IGoodsService
    {
        Task<List<Goods>> GetAllGoods();
        Task<List<Goods>> GetUnderId(String id);
        Task<Boolean> Create(Goods goods);
        Task<Boolean> Update(String id, Goods goods);
        Task<Boolean> Delete(String id);
        Task<Boolean> checkExist(String id);
    }
}
