﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface ITSDetailService
    {
        Task<Boolean> Create(TSDetail entry);
        Task<List<TSDetail>> GetUnderId(String id);
        Task<List<TSDetail>> GetMultiId(String tsNumber, String goodsID, int Ordinal);
        Task<Boolean> Update(String TSNumber,int Ordinal,String GoodsID, TSDetail entry);
        Task<Boolean> Delete(String TSNumber);
        Task<Boolean> Delete(String TSNumber,int Ordinal,String GoodsId);
    }
}
