﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IGGDataGeneralService
    {
        Task<List<GGDataGeneral>> GetAll();
        Task<List<GGDataGeneral>> GetUnderId(String gGDNumber);
        Task<List<GGDataGeneral>> GetUnderId(String gGDNumber, String goodsID, int Ordinal);
        Task<List<GGDataGeneral>> GetUnderId(String gGDNumber, String goodsID);
        Task<Boolean> Create(GGDataGeneral GGDataGeneral);
        Task<Boolean> Update(String gGDNumber, String goodsID, int Ordinal, GGDataGeneral GGDataGeneral);
        Task<Boolean> Delete(String gGDNumber);
        Task<String> GetLastId();
        Task<Boolean> checkExist(String gGDNumber, String goodIDs);
    }
}
