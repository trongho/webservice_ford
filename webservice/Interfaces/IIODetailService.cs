﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IIODetailService
    {
        Task<List<IODetail>> GetAll();
        Task<List<IODetail>> GetUnderId(String id);
        Task<List<IODetail>> GetUnderId(String IONumber, String goodsID, int Ordinal);
        Task<Boolean> Create(IODetail iODetail);
        Task<Boolean> Update(String IONumber, String goodsID, int Ordinal, IODetail iODetail);
        Task<Boolean> Delete(String IONumber);
        Task<String> GetLastId();
        Task<Boolean> checkExist(String IONumber);
        Task<Boolean> checkDataChanged();
    }
}
