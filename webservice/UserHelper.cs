﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class UserHelper
    {
        public static List<UserModel> CovertUsers(List<User> users)
        {
            var userModels = users.ConvertAll(user => new UserModel
            {
                UserID =user.UserID,
                Role=user.Role,
                UserName=user.UserName,
                Adress=user.Adress,
                Position=user.Position,
                BranchID=user.BranchID,
                BranchName=user.BranchName,
                Email=user.Email,
                LoginName=user.LoginName,
                Password=user.Password,
                PasswordSalt=user.PasswordSalt,
                Description=user.Description,
                Status=user.Status,
                CreatedUserID=user.CreatedUserID,
                CreatedDate=user.CreatedDate,
                UpdatedUserID=user.UpdatedUserID,
                UpdatedDate=user.UpdatedDate,
                Active=user.Active,
                Blocked=user.Blocked,
            });

            return userModels;
        }
    }
}
