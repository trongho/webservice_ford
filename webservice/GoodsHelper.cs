﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class GoodsHelper
    {
        public static List<GoodsModel> Covert(List<Goods> entrys)
        {
            var models = entrys.ConvertAll(sc => new GoodsModel
            {
                GoodsID =sc.GoodsID,
                GoodsName =sc.GoodsName,
                OtherGoodsName =sc.OtherGoodsName,
                StockUnitID =sc.StockUnitID,
                GoodsTypeID =sc.GoodsTypeID,
                GoodsLineID =sc.GoodsLineID,
                GoodsGroupID =sc.GoodsGroupID,
                GoodsCategoryID =sc.GoodsCategoryID,
                InternalGoodsID =sc.InternalGoodsID,
                GeneralGoodsID =sc.GeneralGoodsID,
                Model =sc.Model,
                SerialNumber =sc.SerialNumber,
                ProductionDate =sc.ProductionDate,
                ExpiryDate =sc.ExpiryDate,
                WarrantyEndDate =sc.WarrantyEndDate,
                ScanOption =sc.ScanOption,
                PLUType =sc.PLUType,
                Packing =sc.Packing,
                Description =sc.Description,
                GoodsStatus =sc.GoodsStatus,
                SupplierID =sc.SupplierID,
                ManufacturerID =sc.ManufacturerID,
                PurchaseUnitID =sc.PurchaseUnitID,
                PurchaseUnitRate =sc.PurchaseUnitRate,
                RetailUnitID =sc.RetailUnitID,
                RetailUnitRate =sc.RetailUnitRate,
                WholesaleUnitID =sc.WholesaleUnitID,
                WholesaleUnitRate =sc.WholesaleUnitRate,
                StyleID =sc.StyleID,
                SizeID =sc.SizeID,
                ColorID =sc.ColorID,
                SeasonID =sc.SeasonID,
                MaterialID =sc.MaterialID,
                Length =sc.Length,
                LengthUnit =sc.LengthUnit,
                Width =sc.Width,
                WidthUnit =sc.WidthUnit,
                Height =sc.Height,
                HeightUnit =sc.HeightUnit,
                Weight =sc.Weight,
                WeightUnit =sc.WeightUnit,
                Diameter =sc.Diameter,
                DiameterUnit =sc.DiameterUnit,
                Gauge =sc.Gauge,
                GaugeUnit =sc.GaugeUnit,
                Volume =sc.Volume,
                VolumeUnit =sc.VolumeUnit,
                Density =sc.Density,
                VATInput =sc.VATInput,
                VATOutput =sc.VATOutput,
                ImportTax =sc.ImportTax,
                SpecialTax =sc.SpecialTax,
                TransportRate =sc.TransportRate,
                OtherRate =sc.OtherRate,
                DiscountInput =sc.DiscountInput,
                DiscountOutput =sc.DiscountOutput,
                CostPrice =sc.CostPrice,
                RetailSalePrice =sc.RetailSalePrice,
                WholesalePrice =sc.WholesalePrice,
                InternalSalePrice =sc.InternalSalePrice,
                Margin =sc.Margin,
                DiscountInternal =sc.DiscountInternal,
                MinQuantity =sc.MinQuantity,
                MaxQuantity =sc.MaxQuantity,
                Status =sc.Status,
                CreatedUserID =sc.CreatedUserID,
                CreatedDate =sc.CreatedDate,
                UpdatedUserID =sc.UpdatedUserID,
                UpdatedDate =sc.UpdatedDate,
    });

            return models;
        }
    }
}
