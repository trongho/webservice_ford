﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class TSDetailHelper
    {
        public static List<TSDetailModel> Covert(List<TSDetail> entrys)
        {
            var models = entrys.ConvertAll(sc => new TSDetailModel
            {
               TSNumber=sc.TSNumber,
               Ordinal=sc.Ordinal,
               GoodsID=sc.GoodsID,
               GoodsName=sc.GoodsName,
               TallyUnitID=sc.TallyUnitID,
               UnitRate=sc.UnitRate,
               StockUnitID=sc.StockUnitID,
               Quantity=sc.Quantity,
               PriceIncludedVAT=sc.PriceIncludedVAT,
               PriceExcludedVAT=sc.PriceExcludedVAT,
               Price=sc.Price,
               Discount=sc.Discount,
               DiscountAmount=sc.DiscountAmount,
               VAT=sc.VAT,
               VATAmount=sc.VATAmount,
               AmountExcludedVAT=sc.AmountExcludedVAT,
               AmountIncludedVAT=sc.AmountIncludedVAT,
               Amount=sc.Amount,
               ExpiryDate=sc.ExpiryDate,
               SerialNumber=sc.SerialNumber,
               Note=sc.Note,
               Status=sc.Status,
            });

            return models;
        }
    }
}
