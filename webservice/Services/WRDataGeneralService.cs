﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class WRDataGeneralService : IWRDataGeneralService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public WRDataGeneralService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(string wRDNumber, String goodsID)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.WRDataGenerals.FirstOrDefault(g => g.WRDNumber.Equals(wRDNumber) && g.GoodsID.Equals(goodsID));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> checkGoodsIDDuplicate(string wRDNumber, string goodsID)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.WRDataGenerals.FirstOrDefault(g => g.WRDNumber.Equals(wRDNumber) && g.GoodsID.Equals(goodsID)&&g.Quantity>0);
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> checkIDCodeIsScaned(string wrdNumber, string idCode, string goodIDs)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.WRDataGenerals.Where(e => e.WRDNumber.Equals(wrdNumber) && e.GoodsID.Equals(goodIDs)).FirstOrDefault();
            String idCodeString = entrys.IDCode;
            String[] arr = idCodeString.Split(",");
            List<String> list = new List<String>(arr.ToList());
            foreach (String i in list)
            {
                if (idCode.Equals(i))
                {
                    flag = true;
                    break;
                }
            }

            return flag;
        }

        public async Task<string> checkQuantity(string wRDNumber, DateTime editedDateTime)
        {
            String light = "off";
            //String format = "2";
            //String dateNowString = DateTime.Now.ToString(format);
            var entrys = warehouseDbContext.WRDataGenerals.Where(u => u.WRDNumber.Equals(wRDNumber)
            && (u.EditedDateTime.Value.ToString().CompareTo(editedDateTime.ToString()) == 0
            || u.EditedDateTime.Value.AddSeconds(1).CompareTo(editedDateTime) == 0
            || u.EditedDateTime.Value.AddSeconds(-1).CompareTo(editedDateTime) == 0)).FirstOrDefault();

            if (entrys != null)
            {

                if (entrys.Quantity < entrys.QuantityOrg)
                {
                    light = "red";
                }
                if (entrys.Quantity == entrys.QuantityOrg)
                {
                    light = "green";
                }
                if (entrys.Quantity > entrys.QuantityOrg)
                {
                    light = "yellow";
                }
            }
            else
            {
                light = editedDateTime.ToString("yyyy-MM-dd HH:mm:ss");
            }

            return light;
        }


        public async Task<bool> Create(WRDataGeneral WRDataGeneral)
        {
            var entrys = warehouseDbContext.WRDataGenerals.AddAsync(WRDataGeneral);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string wRDNumber)
        {
            var entrys = warehouseDbContext.WRDataGenerals.Where(o => o.WRDNumber.Equals(wRDNumber));
            warehouseDbContext.WRDataGenerals.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<WRDataGeneral>> GetAll()
        {
            var entrys = warehouseDbContext.WRDataGenerals;
            return await entrys.ToListAsync();
        }

        public async Task<List<WRDataGeneral>> GetByNgayNhanHang(DateTime fromDate, DateTime toDate)
        {
            var entrys = warehouseDbContext.WRDataGenerals.Where(w => w.EditedDateTime.Value>= fromDate && w.EditedDateTime.Value<= toDate);

            return await entrys.ToListAsync();
        }

        public async Task<string> GetLastId()
        {
            var curentID = warehouseDbContext.WRDataGenerals.OrderByDescending(x => x.WRDNumber).Take(1).Select(x => x.WRDNumber).ToList().FirstOrDefault();
            return curentID;
        }

        public async Task<int> getTotalGoods(string wrdNumber)
        {
            int totalGoods = 0;
            var entrys = warehouseDbContext.WRDataGenerals.Where(u => u.WRDNumber.Equals(wrdNumber)&&u.Quantity>0).ToList();
            if (entrys != null)
            {
                totalGoods = entrys.Count;
            }
            return totalGoods;
        }

        public async Task<decimal?> getTotalQuantity(string wrdNumber)
        {
            decimal? totalQuantity = 0m;
            var entrys = warehouseDbContext.WRDataGenerals.Where(u => u.WRDNumber.Equals(wrdNumber));
            if (entrys != null)
            {
                foreach (WRDataGeneral wRDataGeneral in entrys)
                {
                    if (wRDataGeneral.Quantity > 0)
                    {
                        totalQuantity += wRDataGeneral.Quantity;
                    }
                }
            }
            return totalQuantity;
        }

        public async Task<List<WRDataGeneral>> GetUnderId(string wRDNumber)
        {
            var entrys = warehouseDbContext.WRDataGenerals.Where(u => u.WRDNumber.Equals(wRDNumber));
            return await entrys.ToListAsync();
        }

        public async Task<List<WRDataGeneral>> GetUnderId(string wRDNumber, string goodsID, int Ordinal)
        {
            var entrys = warehouseDbContext.WRDataGenerals.Where(u => u.WRDNumber.Equals(wRDNumber) && u.GoodsID.Equals(goodsID) && u.Ordinal == Ordinal);
            return await entrys.ToListAsync();
        }

        public async Task<List<WRDataGeneral>> GetUnderId(string wRDNumber, string goodsID)
        {
            var entrys = warehouseDbContext.WRDataGenerals.Where(u => u.WRDNumber.Equals(wRDNumber) && u.GoodsID.Equals(goodsID));
            return await entrys.ToListAsync();
        }

        public async Task<List<WRDataGeneral>> GetUnderIdCode(string wRDNumber, string goodsID, string IDCode)
        {
            //int idcodeLenght = IDCode.Length;
            //int percentIndex = IDCode.IndexOf("%");
            //String s1 = IDCode.Substring(0, 1);
            //String s2 = IDCode.Substring(percentIndex, 3);
            //String s3 = IDCode.Substring(percentIndex + 3, idcodeLenght - s1.Length - s2.Length);

            //s2 = s2.Replace(s2, "/");
            //IDCode = s1 + s2 + s3;

            var entrys = warehouseDbContext.WRDataGenerals.Where(u => u.WRDNumber.Equals(wRDNumber) && u.GoodsID.Equals(goodsID) && u.IDCode.Equals(IDCode));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string wRDNumber, string goodsID, int Ordinal, WRDataGeneral WRDataGeneral)
        {
            var entrys = warehouseDbContext.WRDataGenerals.FirstOrDefault(o => o.WRDNumber.Equals(wRDNumber) && o.GoodsID.Equals(goodsID) && o.Ordinal == Ordinal);
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(WRDataGeneral);
            warehouseDbContext.SaveChanges();
            return true;
        }


    }
}
