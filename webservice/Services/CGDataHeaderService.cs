﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class CGDataHeaderService: ICGDataHeaderService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public CGDataHeaderService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(string id)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.CGDataHeaders.FirstOrDefault(g => g.CGDNumber.Equals(id));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(CGDataHeader CGDataHeader)
        {
            var entrys = warehouseDbContext.CGDataHeaders.AddAsync(CGDataHeader);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = warehouseDbContext.CGDataHeaders.FirstOrDefault(o => o.CGDNumber.Equals(id));
            warehouseDbContext.CGDataHeaders.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }


        public async Task<List<CGDataHeader>> GetAll()
        {
            var entrys = warehouseDbContext.CGDataHeaders;
            return await entrys.ToListAsync();
        }

        public async Task<string> GetLastId()
        {
            var curentID = warehouseDbContext.CGDataHeaders.OrderByDescending(x => x.CGDNumber).Take(1).Select(x => x.CGDNumber).ToList().FirstOrDefault();
            return curentID;
        }

        public async Task<List<CGDataHeader>> GetUnderBranchID(string branchID)
        {
            var entrys = warehouseDbContext.CGDataHeaders.Where(u => u.BranchID.Equals(branchID));
            return await entrys.ToListAsync();
        }

        public async Task<List<CGDataHeader>> GetUnderDate(DateTime fromDate, DateTime toDate)
        {
            var entrys = warehouseDbContext.CGDataHeaders.Where(u => u.CGDDate >= fromDate && u.CGDDate <= toDate);
            return await entrys.ToListAsync();
        }

        public async Task<List<CGDataHeader>> GetUnderHandlingStatusID(string handlingStatusID)
        {
            var entrys = warehouseDbContext.CGDataHeaders.Where(u => u.HandlingStatusID.Equals(handlingStatusID));
            return await entrys.ToListAsync();
        }

        public async Task<List<CGDataHeader>> GetUnderOrderStatusID(string orderStatusID)
        {
            var entrys = warehouseDbContext.CGDataHeaders.Where(u => u.Status.Equals(orderStatusID));
            return await entrys.ToListAsync();
        }

        public async Task<List<CGDataHeader>> GetUnderId(string id)
        {
            var entrys = warehouseDbContext.CGDataHeaders.Where(u => u.CGDNumber.Equals(id));
            return await entrys.ToListAsync();
        }


        public async Task<bool> Update(string id, CGDataHeader CGDataHeader)
        {
            var entrys = warehouseDbContext.CGDataHeaders.FirstOrDefault(o => o.CGDNumber.Equals(id));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(CGDataHeader);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
