﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Helpers;
using webservice.Interfaces;
using webservice.Requests;
using webservice.Responses;

namespace webservice.Services
{
    public class UserService : IUserService
    {
        private readonly WarehouseDbContext warehouseDbContext;
        public UserService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<Boolean> CreateUser(User user)
        {
            user.PasswordSalt = "MTIzNDU2Nzg5MTIzNDU2Nw==";
            user.Password = HashingHelper.HashUsingPbkdf2(user.Password, user.PasswordSalt);
            var users = warehouseDbContext.Users.AddAsync(user);
            warehouseDbContext.SaveChanges();

            return users.IsCompleted;
        }

        public async Task<Boolean> DeleteUser(string id)
        {
            var users = warehouseDbContext.Users.FirstOrDefault(o => o.UserID.Equals(id));
            warehouseDbContext.Users.Remove(users);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<User>> GetAllUser()
        {
            var users = warehouseDbContext.Users;
            return await users.ToListAsync();
        }

        public async Task<List<User>> GetUserUnderId(String id)
        {
            var user = warehouseDbContext.Users.Where(u =>u.UserID.Equals(id));
            return await user.ToListAsync();
        }

        public async Task<Boolean> UpdateUser(String id,User user)
        {
            var users = warehouseDbContext.Users.FirstOrDefault(o=>o.UserID.Equals(id));
            user.PasswordSalt = "MTIzNDU2Nzg5MTIzNDU2Nw==";
            user.Password = HashingHelper.HashUsingPbkdf2(user.Password, user.PasswordSalt);
            warehouseDbContext.Entry(users).CurrentValues.SetValues(user);
            warehouseDbContext.SaveChanges();
            return true;
        }


        public async Task<LoginResponse> Login(LoginRequest loginRequest)
        {
            var user = warehouseDbContext.Users.SingleOrDefault(user=>user.Active&& user.UserName == loginRequest.Username);
            if (user == null)
            {
                return null;
            }
            var passwordHash = HashingHelper.HashUsingPbkdf2(loginRequest.Password, user.PasswordSalt);

            if (user.Password != passwordHash)
            {
                return null;
            }

            var token = await Task.Run(() => TokenHelper.GenerateToken(user));

            return new LoginResponse { Username = user.UserName,Password=user.Password, LoginName =user.LoginName, Token = token };
        }

        public async Task<string> GetLastUserId()
        {
            var userID = warehouseDbContext.Users.OrderByDescending(x => x.UserID).Take(1).Select(x => x.UserID).ToList().FirstOrDefault();
            return userID;
        }

        public async Task<Boolean> checkExist(string id)
        {
            Boolean flag = false;
            var user = warehouseDbContext.Users.FirstOrDefault(g => g.UserID.Equals(id));
            if (user != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<List<User>> GetUserUnderUsername(string username)
        {
            var user = warehouseDbContext.Users.Where(u =>u.UserName.Equals(username));
            return await user.ToListAsync();
        }
    }
}

