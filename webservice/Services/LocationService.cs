﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class LocationService : ILocationService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public LocationService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }
        public async Task<bool> checkExist(string id)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.Locations.FirstOrDefault(g => g.LocationID.Equals(id));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(Location entry)
        {
            var entrys = warehouseDbContext.Locations.AddAsync(entry);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = warehouseDbContext.Locations.FirstOrDefault(o => o.LocationID.Equals(id));
            warehouseDbContext.Locations.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<Location>> GetAll()
        {
            var entrys = warehouseDbContext.Locations;
            return await entrys.ToListAsync();
        }

        public async Task<List<Location>> GetUnderId(string id)
        {
            var entrys = warehouseDbContext.Locations.Where(u => u.LocationID.Equals(id));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string id, Location entry)
        {
            var entrys = warehouseDbContext.Locations.FirstOrDefault(o => o.LocationID.Equals(id));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(entry);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
