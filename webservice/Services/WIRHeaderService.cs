﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class WIRHeaderService : IWIRHeaderService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public WIRHeaderService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(string id)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.WIRHeaders.FirstOrDefault(g => g.WIRNumber.Equals(id));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(WIRHeader wIRHeader)
        {
            var entrys = warehouseDbContext.WIRHeaders.AddAsync(wIRHeader);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = warehouseDbContext.WIRHeaders.FirstOrDefault(o => o.WIRNumber.Equals(id));
            warehouseDbContext.WIRHeaders.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<WIRHeader>> Filter(string branchID, DateTime fromDate, DateTime toDate, string handlingStatusID)
        {
            var entrys = warehouseDbContext.WIRHeaders.Where(u => u.BranchID.Equals(branchID)
               && u.WIRDate >= fromDate && u.WIRDate <= toDate
               && u.HandlingStatusID.Equals(handlingStatusID));
            return await entrys.ToListAsync();
        }

        public async Task<List<WIRHeader>> FilterByDateAndBranch(DateTime fromDate, DateTime toDate, string branchID)
        {
            var entrys = warehouseDbContext.WIRHeaders.Where(u => u.BranchID.Equals(branchID)
                && u.WIRDate >= fromDate && u.WIRDate <= toDate);
            return await entrys.ToListAsync();
        }

        public async Task<List<WIRHeader>> FilterByDateAndHandlingStatus(DateTime fromDate, DateTime toDate, string handlingStatusID)
        {
            var entrys = warehouseDbContext.WIRHeaders.Where(u => u.WIRDate >= fromDate && u.WIRDate <= toDate
               && u.HandlingStatusID.Equals(handlingStatusID));
            return await entrys.ToListAsync();
        }

        public async Task<List<WIRHeader>> GetAll()
        {
            var entrys = warehouseDbContext.WIRHeaders;
            return await entrys.ToListAsync();
        }

        public async Task<string> GetLastId()
        {
            var curentID = warehouseDbContext.WIRHeaders.OrderByDescending(x => x.WIRNumber).Take(1).Select(x => x.WIRNumber).ToList().FirstOrDefault();
            return curentID;
        }

        public async Task<List<WIRHeader>> GetUnderBranchID(string branchID)
        {
            var entrys = warehouseDbContext.WIRHeaders.Where(u => u.BranchID.Equals(branchID));
            return await entrys.ToListAsync();
        }

        public async Task<List<WIRHeader>> GetUnderDate(DateTime fromDate, DateTime toDate)
        {
            var entrys = warehouseDbContext.WIRHeaders.Where(u => u.WIRDate >= fromDate && u.WIRDate <= toDate);
            return await entrys.ToListAsync();
        }

        public async Task<List<WIRHeader>> GetUnderHandlingStatusID(string handlingStatusID)
        {
            var entrys = warehouseDbContext.WIRHeaders.Where(u => u.HandlingStatusID.Equals(handlingStatusID));
            return await entrys.ToListAsync();
        }

        public async Task<List<WIRHeader>> GetUnderId(string id)
        {
            var entrys = warehouseDbContext.WIRHeaders.Where(u => u.WIRNumber.Equals(id));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string id, WIRHeader wIRHeader)
        {
            var entrys = warehouseDbContext.WIRHeaders.FirstOrDefault(o => o.WIRNumber.Equals(id));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(wIRHeader);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
