﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class WIRDetailService:IWIRDetailService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public WIRDetailService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(string wIRNumber)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.WIRDetails.FirstOrDefault(g => g.WIRNumber.Equals(wIRNumber));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(WIRDetail wIRDetail)
        {
            var entrys = warehouseDbContext.WIRDetails.AddAsync(wIRDetail);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = warehouseDbContext.WIRDetails.Where(o => o.WIRNumber.Equals(id));
            warehouseDbContext.WIRDetails.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<WIRDetail>> GetAll()
        {
            var entrys = warehouseDbContext.WIRDetails;
            return await entrys.ToListAsync();
        }

        public async Task<string> GetLastId()
        {
            var curentID = warehouseDbContext.WIRDetails.OrderByDescending(x => x.WIRNumber).Take(1).Select(x => x.WIRNumber).ToList().FirstOrDefault();
            return curentID;
        }

        public async Task<List<WIRDetail>> GetUnderGoodsGroupID(string wIRNumber, string GoodsGroupID)
        {
            var entrys = warehouseDbContext.WIRDetails.Where(u => u.WIRNumber.Equals(wIRNumber) && u.GoodsGroupID.Equals(GoodsGroupID));
            return await entrys.ToListAsync();
        }

        public async Task<List<WIRDetail>> GetUnderId(string wIRNumber, string goodsID, int Ordinal)
        {
            var entrys = warehouseDbContext.WIRDetails.Where(u => u.WIRNumber.Equals(wIRNumber)&&u.GoodsID.Equals(goodsID)&&u.Ordinal==Ordinal);
            return await entrys.ToListAsync();
        }

        public async Task<List<WIRDetail>> GetUnderId(string wIRNumber)
        {
            var entrys = warehouseDbContext.WIRDetails.Where(u => u.WIRNumber.Equals(wIRNumber));
            return await entrys.ToListAsync();
        }

        public async Task<List<WIRDetail>> GetUnderId(string wIRNumber, string goodsID, int Ordinal, string GoodsGroupID)
        {
            var entrys = warehouseDbContext.WIRDetails.Where(u => u.WIRNumber.Equals(wIRNumber) && u.GoodsID.Equals(goodsID) && u.Ordinal == Ordinal && u.GoodsGroupID == GoodsGroupID);
            return await entrys.ToListAsync();
        }

        public async Task<List<WIRDetail>> GetUnderPickerName(string wIRNumber, string PickerName)
        {
            var entrys = warehouseDbContext.WIRDetails.Where(u => u.WIRNumber.Equals(wIRNumber) && u.PickerName.Equals(PickerName));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string wIRNumber, String goodsID, int Ordinal,String goodsGroupID, WIRDetail wIRDetail)
        {
            var entrys = warehouseDbContext.WIRDetails.FirstOrDefault(o => o.WIRNumber.Equals(wIRNumber) && o.GoodsID.Equals(goodsID) && o.Ordinal==Ordinal&&o.GoodsGroupID==goodsGroupID);
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(wIRDetail);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
