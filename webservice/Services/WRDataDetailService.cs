﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class WRDataDetailService: IWRDataDetailService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public WRDataDetailService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(string wRDNumber,String goodsID)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.WRDataDetails.FirstOrDefault(g => g.WRDNumber.Equals(wRDNumber)&&g.GoodsID.Equals(goodsID));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(WRDataDetail wRDataDetail)
        {
            var entrys = warehouseDbContext.WRDataDetails.AddAsync(wRDataDetail);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string wRDNumber)
        {
            var entrys = warehouseDbContext.WRDataDetails.Where(o => o.WRDNumber.Equals(wRDNumber));
            warehouseDbContext.WRDataDetails.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<WRDataDetail>> GetAll()
        {
            var entrys = warehouseDbContext.WRDataDetails;
            return await entrys.ToListAsync();
        }

        public async Task<string> GetLastId()
        {
            var curentID = warehouseDbContext.WRDataDetails.OrderByDescending(x => x.WRDNumber).Take(1).Select(x => x.WRDNumber).ToList().FirstOrDefault();
            return curentID;
        }

        public async Task<List<WRDataDetail>> GetUnderId(string wRDNumber)
        {
            var entrys = warehouseDbContext.WRDataDetails.Where(u => u.WRDNumber.Equals(wRDNumber));
            return await entrys.ToListAsync();
        }

        public async Task<List<WRDataDetail>> GetUnderId(string wRDNumber, string goodsID, int Ordinal)
        {
            var entrys = warehouseDbContext.WRDataDetails.Where(u => u.WRDNumber.Equals(wRDNumber) && u.GoodsID.Equals(goodsID) && u.Ordinal==Ordinal);
            return await entrys.ToListAsync();
        }

        public async Task<List<WRDataDetail>> GetUnderIdCode(string wRDNumber, string goodsID, string IDCode)
        {
            int idcodeLenght = IDCode.Length;
            int percentIndex = IDCode.IndexOf("%");
            String s1 = IDCode.Substring(0, 1);
            String s2 = IDCode.Substring(percentIndex, 3);
            String s3 = IDCode.Substring(percentIndex+3,idcodeLenght-s1.Length-s2.Length);
           
                s2 = s2.Replace(s2, "/");
                IDCode = s1 + s2 + s3;
           
            var entrys = warehouseDbContext.WRDataDetails.Where(u => u.WRDNumber.Equals(wRDNumber) && u.GoodsID.Equals(goodsID) && u.IDCode == IDCode);
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string wRDNumber, string goodsID, int Ordinal, WRDataDetail wRDataDetail)
        {
            var entrys = warehouseDbContext.WRDataDetails.FirstOrDefault(o => o.WRDNumber.Equals(wRDNumber) && o.GoodsID.Equals(goodsID) && o.Ordinal==Ordinal);
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(wRDataDetail);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
