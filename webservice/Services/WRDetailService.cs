﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class WRDetailService: IWRDetailService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public WRDetailService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(string wRNumber)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.WRDetails.FirstOrDefault(g => g.WRNumber.Equals(wRNumber));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(WRDetail wRDetail)
        {
            var entrys = warehouseDbContext.WRDetails.AddAsync(wRDetail);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string wRNumber)
        {
            var entrys = warehouseDbContext.WRDetails.Where(o => o.WRNumber.Equals(wRNumber));
            warehouseDbContext.WRDetails.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<WRDetail>> GetAll()
        {
            var entrys = warehouseDbContext.WRDetails;
            return await entrys.ToListAsync();
        }

        public async Task<string> GetLastId()
        {
            var curentID = warehouseDbContext.WRDetails.OrderByDescending(x => x.WRNumber).Take(1).Select(x => x.WRNumber).ToList().FirstOrDefault();
            return curentID;
        }

        public async Task<List<WRDetail>> GetUnderId(string id)
        {
            var entrys = warehouseDbContext.WRDetails.Where(u => u.WRNumber.Equals(id));
            return await entrys.ToListAsync();
        }

        public async Task<List<WRDetail>> GetUnderId(string wRNumber, string goodsID, int Ordinal)
        {
            var entrys = warehouseDbContext.WRDetails.Where(u => u.WRNumber.Equals(wRNumber) && u.GoodsID.Equals(goodsID) && u.Ordinal==Ordinal);
            return await entrys.ToListAsync();
        }

      
        public async Task<bool> Update(string wRNumber, string goodsID, int Ordinal, WRDetail wRDetail)
        {
            var entrys = warehouseDbContext.WRDetails.FirstOrDefault(o => o.WRNumber.Equals(wRNumber) && o.GoodsID.Equals(goodsID) && o.Ordinal==Ordinal);
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(wRDetail);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<bool> checkDataChanged()
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.WRDetails;
            foreach(WRDetail wRDetail in entrys.ToList())
            {
                if (wRDetail.Status =="0")
                {
                    flag = true;
                    break;
                }
            }
            return flag;
        }
    }
}
