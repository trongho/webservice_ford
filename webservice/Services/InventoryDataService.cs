﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class InventoryDataService: IInventoryDataService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public InventoryDataService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(string id)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.InventoryDatas.FirstOrDefault(g => g.GoodsID.Equals(id));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

      
        public async Task<bool> Create(InventoryData entry)
        {
            var entrys = warehouseDbContext.InventoryDatas.AddAsync(entry);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = warehouseDbContext.InventoryDatas.FirstOrDefault(o => o.GoodsID.Equals(id));
            warehouseDbContext.InventoryDatas.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<InventoryData>> GetAll()
        {
            var entrys = warehouseDbContext.InventoryDatas;
            return await entrys.ToListAsync();
        }

        public async Task<List<InventoryData>> GetUnderId(string id)
        {
            var entrys = warehouseDbContext.InventoryDatas.Where(u => u.GoodsID.Equals(id));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string id, InventoryData entry)
        {
            var entrys = warehouseDbContext.InventoryDatas.FirstOrDefault(o => o.GoodsID.Equals(id));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(entry);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
