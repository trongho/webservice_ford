﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class DataPrintHeaderService : IDataPrintHeaderService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public DataPrintHeaderService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;

        }

        public async Task<bool> checkExist(string id)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.DataPrintHeaders.FirstOrDefault(g => g.DataPrintNumber.Equals(id));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(DataPrintHeader entry)
        {
            var entrys = warehouseDbContext.DataPrintHeaders.AddAsync(entry);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = warehouseDbContext.DataPrintHeaders.FirstOrDefault(o => o.DataPrintNumber.Equals(id));
            warehouseDbContext.DataPrintHeaders.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<DataPrintHeader>> GetAll()
        {
            var entrys = warehouseDbContext.DataPrintHeaders;
            return await entrys.ToListAsync();
        }

        public async Task<List<DataPrintHeader>> GetUnderId(string id)
        {
            var entrys = warehouseDbContext.DataPrintHeaders.Where(u => u.DataPrintNumber.Equals(id));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string id, DataPrintHeader entry)
        {
            var entrys = warehouseDbContext.DataPrintHeaders.FirstOrDefault(o => o.DataPrintNumber.Equals(id));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(entry);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<string> GetLastId()
        {
            var curentID = warehouseDbContext.DataPrintHeaders.OrderByDescending(x => x.DataPrintNumber).Take(1).Select(x => x.DataPrintNumber).ToList().FirstOrDefault();
            return curentID;
        }
    }
}
