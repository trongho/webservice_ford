﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class DataPrintGeneralService : IDataPrintGeneralService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public DataPrintGeneralService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;

        }

        public async Task<bool> checkExist(string id, string goodsID, int ordinal)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.DataPrintGenerals.FirstOrDefault(g => g.DataPrintNumber.Equals(id)
             &&g.GoodsID.Equals(goodsID)&&g.Ordinal==ordinal);
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(DataPrintGeneral entry)
        {
            var entrys = warehouseDbContext.DataPrintGenerals.AddAsync(entry);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = warehouseDbContext.DataPrintGenerals.Where(o => o.DataPrintNumber.Equals(id));
            warehouseDbContext.DataPrintGenerals.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<DataPrintGeneral>> GetAll()
        {
            var entrys = warehouseDbContext.DataPrintGenerals;
            return await entrys.ToListAsync();
        }

        public async Task<List<DataPrintGeneral>> GetUnderId(string id)
        {
            var entrys = warehouseDbContext.DataPrintGenerals.Where(u => u.DataPrintNumber.Equals(id));
            return await entrys.ToListAsync();
        }

        public async Task<List<DataPrintGeneral>> GetUnderId(string id, string goodsID, int ordinal)
        {
            var entrys = warehouseDbContext.DataPrintGenerals.Where(g => g.DataPrintNumber.Equals(id)
             && g.GoodsID.Equals(goodsID) && g.Ordinal == ordinal);
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string id, string goodsID, int ordinal, DataPrintGeneral entry)
        {
            var entrys = warehouseDbContext.DataPrintGenerals.FirstOrDefault(o => o.DataPrintNumber.Equals(id) && o.GoodsID.Equals(goodsID) && o.Ordinal ==ordinal);
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(entry);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
