﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class DataPrintDetailService : IDataPrintDetailService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public DataPrintDetailService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;

        }
        public async Task<bool> checkExist(string id, string goodsID, int ordinal)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.DataPrintDetails.FirstOrDefault(g => g.DataPrintNumber.Equals(id)
             && g.GoodsID.Equals(goodsID) && g.Ordinal == ordinal);
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(DataPrintDetail entry)
        {
            var entrys = warehouseDbContext.DataPrintDetails.AddAsync(entry);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = warehouseDbContext.DataPrintDetails.Where(o => o.DataPrintNumber.Equals(id));
            warehouseDbContext.DataPrintDetails.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<DataPrintDetail>> GetAll()
        {
            var entrys = warehouseDbContext.DataPrintDetails;
            return await entrys.ToListAsync();
        }

        public async Task<List<DataPrintDetail>> GetUnderId(string id)
        {
            var entrys = warehouseDbContext.DataPrintDetails.Where(u => u.DataPrintNumber.Equals(id));
            return await entrys.ToListAsync();
        }

        public async Task<List<DataPrintDetail>> GetUnderId(string id, string goodsID, int ordinal)
        {
            var entrys = warehouseDbContext.DataPrintDetails.Where(g => g.DataPrintNumber.Equals(id)
            && g.GoodsID.Equals(goodsID) && g.Ordinal == ordinal);
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string id, string goodsID, int ordinal, DataPrintDetail entry)
        {
            var entrys = warehouseDbContext.DataPrintDetails.FirstOrDefault(o => o.DataPrintNumber.Equals(id) && o.GoodsID.Equals(goodsID) && o.Ordinal == ordinal);
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(entry);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
