﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class GGDataHeaderService : IGGDataHeaderService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public GGDataHeaderService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(string id)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.GGDataHeaders.FirstOrDefault(g => g.GGDNumber.Equals(id));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(GGDataHeader entry)
        {
            var entrys = warehouseDbContext.GGDataHeaders.AddAsync(entry);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = warehouseDbContext.GGDataHeaders.FirstOrDefault(o => o.GGDNumber.Equals(id));
            warehouseDbContext.GGDataHeaders.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<GGDataHeader>> GetAll()
        {
            var entrys = warehouseDbContext.GGDataHeaders;
            return await entrys.ToListAsync();
        }

        public async Task<string> GetLastId()
        {
            var curentID = warehouseDbContext.GGDataHeaders.OrderByDescending(x => x.GGDNumber).Take(1).Select(x => x.GGDNumber).ToList().FirstOrDefault();
            return curentID;
        }

        public async Task<List<GGDataHeader>> GetUnderBranchID(string branchID)
        {
            var entrys = warehouseDbContext.GGDataHeaders.Where(u => u.BranchID.Equals(branchID));
            return await entrys.ToListAsync();
        }

        public async Task<List<GGDataHeader>> GetUnderDate(DateTime fromDate, DateTime toDate)
        {
            var entrys = warehouseDbContext.GGDataHeaders.Where(u => u.GGDDate>= fromDate && u.GGDDate <= toDate);
            return await entrys.ToListAsync();
        }

        public async Task<List<GGDataHeader>> GetUnderHandlingStatusID(string handlingStatusID)
        {
            var entrys = warehouseDbContext.GGDataHeaders.Where(u => u.HandlingStatusID.Equals(handlingStatusID));
            return await entrys.ToListAsync();
        }

        public async Task<List<GGDataHeader>> GetUnderId(string id)
        {
            var entrys = warehouseDbContext.GGDataHeaders.Where(u => u.GGDNumber.Equals(id));
            return await entrys.ToListAsync();
        }

        public async Task<List<GGDataHeader>> GetUnderOrderStatusID(string orderStatusID)
        {
            var entrys = warehouseDbContext.GGDataHeaders.Where(u => u.Status.Equals(orderStatusID));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string id, GGDataHeader entry)
        {
            var entrys = warehouseDbContext.GGDataHeaders.FirstOrDefault(o => o.GGDNumber.Equals(id));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(entry);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
