﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class ColorService:IColorService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public ColorService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(string id)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.Colors.FirstOrDefault(g => g.ColorID.Equals(id));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(Color entry)
        {
            var entrys = warehouseDbContext.Colors.AddAsync(entry);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = warehouseDbContext.Colors.FirstOrDefault(o => o.ColorID.Equals(id));
            warehouseDbContext.Colors.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<Color>> GetAll()
        {
            var entrys = warehouseDbContext.Colors;
            return await entrys.ToListAsync();
        }

        public async Task<List<Color>> GetUnderId(string id)
        {
            var entrys = warehouseDbContext.Colors.Where(u => u.ColorID.Equals(id));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string id, Color entry)
        {
            var entrys = warehouseDbContext.Colors.FirstOrDefault(o => o.ColorID.Equals(id));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(entry);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
