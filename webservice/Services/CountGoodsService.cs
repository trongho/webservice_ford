﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class CountGoodsService: ICountGoodsService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public CountGoodsService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<List<CountGoods>> GetAll()
        {
            var entrys = warehouseDbContext.CountGoodss;
            return await entrys.ToListAsync();
        }

        public async Task<List<CountGoods>> GetUnderId(string locationID)
        {
            var entrys = warehouseDbContext.CountGoodss.Where(u => u.LocationID.Equals(locationID));
            return await entrys.ToListAsync();
        }

        public async Task<List<CountGoods>> GetUnderId(string locationID, string goodsID)
        {
            var entrys = warehouseDbContext.CountGoodss.Where(u => u.LocationID.Equals(locationID)&&u.GoodsID.Equals(goodsID));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Create(CountGoods CountGoods)
        {
            var entrys = warehouseDbContext.CountGoodss.AddAsync(CountGoods);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Update(string locationID, string goodsID, CountGoods CountGoods)
        {
            var entrys = warehouseDbContext.CountGoodss.FirstOrDefault(o => o.LocationID.Equals(locationID) && o.GoodsID.Equals(goodsID));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(CountGoods);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<bool> Delete(string locationID,string goodsID)
        {
            var entrys = warehouseDbContext.CountGoodss.Where(o => o.LocationID.Equals(locationID)&&o.GoodsID.Equals(goodsID));
            warehouseDbContext.CountGoodss.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

       

        public async Task<bool> checkExist(string locationID, string goodIDs)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.CountGoodss.FirstOrDefault(g => g.LocationID.Equals(locationID) && g.GoodsID.Equals(goodIDs));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Delete(string locationID)
        {
            var entrys = warehouseDbContext.CountGoodss.Where(o => o.LocationID.Equals(locationID));
            warehouseDbContext.CountGoodss.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
