﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class WIDataHeaderService : IWIDataHeaderService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public WIDataHeaderService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(string id)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.WIDataHeaders.FirstOrDefault(g => g.WIDNumber.Equals(id));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(WIDataHeader wIDataHeader)
        {
            var entrys = warehouseDbContext.WIDataHeaders.AddAsync(wIDataHeader);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = warehouseDbContext.WIDataHeaders.FirstOrDefault(o => o.WIDNumber.Equals(id));
            warehouseDbContext.WIDataHeaders.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<WIDataHeader>> Filter(string branchID, DateTime fromDate, DateTime toDate, string handlingStatusID)
        {
            var entrys = warehouseDbContext.WIDataHeaders.Where(u => u.BranchID.Equals(branchID)
                && u.WIDDate >= fromDate && u.WIDDate <= toDate
                && u.HandlingStatusID.Equals(handlingStatusID));
            return await entrys.ToListAsync();
        }

        public async Task<List<WIDataHeader>> FilterByDateAndBranch(DateTime fromDate, DateTime toDate, string branchID)
        {
            var entrys = warehouseDbContext.WIDataHeaders.Where(u => u.BranchID.Equals(branchID)
                && u.WIDDate >= fromDate && u.WIDDate <= toDate);
            return await entrys.ToListAsync();
        }

        public async Task<List<WIDataHeader>> FilterByDateAndHandlingStatus(DateTime fromDate, DateTime toDate, string handlingStatusID)
        {
            var entrys = warehouseDbContext.WIDataHeaders.Where(u => u.WIDDate >= fromDate && u.WIDDate <= toDate
                && u.HandlingStatusID.Equals(handlingStatusID));
            return await entrys.ToListAsync();
        }

        public async Task<List<WIDataHeader>> GetAll()
        {
            var entrys = warehouseDbContext.WIDataHeaders;
            return await entrys.ToListAsync();
        }

        public async Task<string> GetLastId()
        {
            var curentID = warehouseDbContext.WIDataHeaders.OrderByDescending(x => x.WIDNumber).Take(1).Select(x => x.WIDNumber).ToList().FirstOrDefault();
            return curentID;
        }

        public async Task<List<WIDataHeader>> GetUnderBranchID(string branchID)
        {
            var entrys = warehouseDbContext.WIDataHeaders.Where(u => u.BranchID.Equals(branchID));
            return await entrys.ToListAsync();
        }

        public async Task<List<WIDataHeader>> GetUnderDate(DateTime fromDate, DateTime toDate)
        {
            var entrys = warehouseDbContext.WIDataHeaders.Where(u => u.WIDDate >= fromDate && u.WIDDate <= toDate);
            return await entrys.ToListAsync();
        }

        public async Task<List<WIDataHeader>> GetUnderHandlingStatusID(string handlingStatusID)
        {
            var entrys = warehouseDbContext.WIDataHeaders.Where(u => u.HandlingStatusID.Equals(handlingStatusID));
            return await entrys.ToListAsync();
        }

        public async Task<List<WIDataHeader>> GetUnderId(string id)
        {
            var entrys = warehouseDbContext.WIDataHeaders.Where(u => u.WIDNumber.Equals(id));
            return await entrys.ToListAsync();
        }

        public async Task<List<WIDataHeader>> GetUnderOrderStatusID(string orderStatusID)
        {
            var entrys = warehouseDbContext.WIDataHeaders.Where(u => u.Status.Equals(orderStatusID));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string id, WIDataHeader wIDataHeader)
        {
            var entrys = warehouseDbContext.WIDataHeaders.FirstOrDefault(o => o.WIDNumber.Equals(id));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(wIDataHeader);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
