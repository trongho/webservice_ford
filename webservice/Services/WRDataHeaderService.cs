﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class WRDataHeaderService: IWRDataHeaderService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public WRDataHeaderService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(string id)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.WRDataHeaders.FirstOrDefault(g => g.WRDNumber.Equals(id));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(WRDataHeader wRDataHeader)
        {
            var entrys = warehouseDbContext.WRDataHeaders.AddAsync(wRDataHeader);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = warehouseDbContext.WRDataHeaders.FirstOrDefault(o => o.WRDNumber.Equals(id));
            warehouseDbContext.WRDataHeaders.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

       
        public async Task<List<WRDataHeader>> GetAll()
        {
            var entrys = warehouseDbContext.WRDataHeaders;
            return await entrys.ToListAsync();
        }

        public async Task<string> GetLastId()
        {
            var curentID = warehouseDbContext.WRDataHeaders.OrderByDescending(x => x.WRDNumber).Take(1).Select(x => x.WRDNumber).ToList().FirstOrDefault();
            return curentID;
        }

        public async Task<List<WRDataHeader>> GetUnderBranchID(string branchID)
        {
            var entrys = warehouseDbContext.WRDataHeaders.Where(u => u.BranchID.Equals(branchID));
            return await entrys.ToListAsync();
        }

        public async Task<List<WRDataHeader>> GetUnderDate(DateTime fromDate, DateTime toDate)
        {
            var entrys = warehouseDbContext.WRDataHeaders.Where(u => u.WRDDate >= fromDate && u.WRDDate <= toDate);
            return await entrys.ToListAsync();
        }

        public async Task<List<WRDataHeader>> GetUnderHandlingStatusID(string handlingStatusID)
        {
            var entrys = warehouseDbContext.WRDataHeaders.Where(u => u.HandlingStatusID.Equals(handlingStatusID));
            return await entrys.ToListAsync();
        }

        public async Task<List<WRDataHeader>> GetUnderOrderStatusID(string orderStatusID)
        {
            var entrys = warehouseDbContext.WRDataHeaders.Where(u => u.Status.Equals(orderStatusID));
            return await entrys.ToListAsync();
        }

        public async Task<List<WRDataHeader>> GetUnderId(string id)
        {
            var entrys = warehouseDbContext.WRDataHeaders.Where(u => u.WRDNumber.Equals(id));
            return await entrys.ToListAsync();
        }


        public async Task<bool> Update(string id, WRDataHeader wRDataHeader)
        {
            var entrys = warehouseDbContext.WRDataHeaders.FirstOrDefault(o => o.WRDNumber.Equals(id));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(wRDataHeader);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<string> checkTotalQuantity(string wRDNumber)
        {
            String light = "off";
            var entrys = warehouseDbContext.WRDataHeaders.Where(u => u.WRDNumber.Equals(wRDNumber)).FirstOrDefault();
            if (entrys != null)
            {
                if (entrys.TotalQuantity < entrys.TotalQuantityOrg)
                {
                    light = "red";
                }
                if (entrys.TotalQuantity == entrys.TotalQuantityOrg)
                {
                    light = "green";
                }
                if (entrys.TotalQuantity > entrys.TotalQuantityOrg)
                {
                    light = "yellow";
                }
            }
            return light;
        }


        public async Task<List<WRDataHeader>> Filter(string branchID, DateTime fromDate, DateTime toDate, string handlingStatusID)
        {
            var entrys = warehouseDbContext.WRDataHeaders.Where(u =>u.BranchID.Equals(branchID)
                &&u.WRDDate >= fromDate && u.WRDDate <= toDate
                &&u.HandlingStatusID.Equals(handlingStatusID));
            return await entrys.ToListAsync();
        }

        public async Task<List<WRDataHeader>> FilterByDateAndHandlingStatus(DateTime fromDate, DateTime toDate, string handlingStatusID)
        {
            var entrys = warehouseDbContext.WRDataHeaders.Where(u => u.WRDDate >= fromDate && u.WRDDate <= toDate
                && u.HandlingStatusID.Equals(handlingStatusID));
            return await entrys.ToListAsync();
        }

        public async Task<List<WRDataHeader>> FilterByDateAndBranch(DateTime fromDate, DateTime toDate, string branchID)
        {
            var entrys = warehouseDbContext.WRDataHeaders.Where(u => u.BranchID.Equals(branchID)
                && u.WRDDate >= fromDate && u.WRDDate <= toDate);
            return await entrys.ToListAsync();
        }
    }
}
