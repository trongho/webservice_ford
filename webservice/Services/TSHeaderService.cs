﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class TSHeaderService: ITSHeaderService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public TSHeaderService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> Create(TSHeader entry)
        {
            var entrys = warehouseDbContext.TSHeaders.AddAsync(entry);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = warehouseDbContext.TSHeaders.Where(o => o.TSNumber.Equals(id));
            warehouseDbContext.TSHeaders.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<string> GetLastId()
        {
            var curentID = warehouseDbContext.TSHeaders.OrderByDescending(x => x.TSNumber).Take(1).Select(x => x.TSNumber).ToList().FirstOrDefault();
            return curentID;
        }

        public async Task<List<TSHeader>> GetUnderBranchID(string branchID)
        {
            var entrys = warehouseDbContext.TSHeaders.Where(u => u.BranchID.Equals(branchID));
            return await entrys.ToListAsync();
        }

        public async Task<List<TSHeader>> GetUnderDate(DateTime fromDate, DateTime toDate)
        {
            var entrys = warehouseDbContext.TSHeaders.Where(u => u.TSDate >= fromDate && u.TSDate <= toDate);
            return await entrys.ToListAsync();
        }

        public async Task<List<TSHeader>> GetUnderHandlingStatusID(string handlingStatusID)
        {
            var entrys = warehouseDbContext.TSHeaders.Where(u => u.HandlingStatusID.Equals(handlingStatusID));
            return await entrys.ToListAsync();
        }

        public async Task<List<TSHeader>> GetUnderId(string id)
        {
            var entrys = warehouseDbContext.TSHeaders.Where(u => u.TSNumber.Equals(id));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string id, TSHeader entry)
        {
            var entrys = warehouseDbContext.TSHeaders.FirstOrDefault(o => o.TSNumber.Equals(id));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(entry);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
