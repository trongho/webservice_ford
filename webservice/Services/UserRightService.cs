﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class UserRightService : IUserRightService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public UserRightService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> CreateUserRight(UserRight userRight)
        {
            var entry = warehouseDbContext.UserRights.AddAsync(userRight);
            warehouseDbContext.SaveChanges();

            return entry.IsCompleted;
        }

        public async Task<bool> DeleteUserRight(String userId,String rightId)
        {
            var entry= warehouseDbContext.UserRights.FirstOrDefault(o => o.UserID.Equals(userId)&&o.RightID.Equals(rightId));
            warehouseDbContext.UserRights.Remove(entry);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<UserRight>> GetUserRightByRightId(String id)
        {
            var userRights =  warehouseDbContext.UserRights.Where(ur => ur.RightID.Equals(id));

            return await userRights.ToListAsync();
        }

        public async Task<List<UserRight>> GetUserRightByUserId(String id)
        {
            var userRights =warehouseDbContext.UserRights.Where(ur => ur.UserID.Equals(id));

            return await userRights.ToListAsync();
        }

        public async Task<List<UserRight>> GetUserRightByUserRightId(String userId, String rightId)
        {
            var userRights =warehouseDbContext.UserRights.Where(ur => ur.UserID.Equals(userId)&&ur.RightID.Equals(rightId));

            return await userRights.ToListAsync();
        }

        public async Task<bool> UpdateUserRight(String userId,String rightId, UserRight userRight)
        {
            var userRights = warehouseDbContext.UserRights.FirstOrDefault(o => o.UserID.Equals(userId)&&o.RightID.Equals(rightId));
            warehouseDbContext.Entry(userRights).CurrentValues.SetValues(userRight);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
