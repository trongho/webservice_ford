﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public  class IODetailservice : IIODetailService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public  IODetailservice(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(string IONumber)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.IODetails.FirstOrDefault(g => g.IONumber.Equals(IONumber));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(IODetail iODetail)
        {
            var entrys = warehouseDbContext.IODetails.AddAsync(iODetail);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string IONumber)
        {
            var entrys = warehouseDbContext.IODetails.Where(o => o.IONumber.Equals(IONumber));
            warehouseDbContext.IODetails.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<IODetail>> GetAll()
        {
            var entrys = warehouseDbContext.IODetails;
            return await entrys.ToListAsync();
        }

        public async Task<string> GetLastId()
        {
            var curentID = warehouseDbContext.IODetails.OrderByDescending(x => x.IONumber).Take(1).Select(x => x.IONumber).ToList().FirstOrDefault();
            return curentID;
        }

        public async Task<List<IODetail>> GetUnderId(string id)
        {
            var entrys = warehouseDbContext.IODetails.Where(u => u.IONumber.Equals(id));
            return await entrys.ToListAsync();
        }

        public async Task<List<IODetail>> GetUnderId(string IONumber, string goodsID, int Ordinal)
        {
            var entrys = warehouseDbContext.IODetails.Where(u => u.IONumber.Equals(IONumber) && u.GoodsID.Equals(goodsID) && u.Ordinal == Ordinal);
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string IONumber, string goodsID, int Ordinal, IODetail iODetail)
        {
            var entrys = warehouseDbContext.IODetails.FirstOrDefault(o => o.IONumber.Equals(IONumber) && o.GoodsID.Equals(goodsID) && o.Ordinal == Ordinal);
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(iODetail);
            warehouseDbContext.SaveChanges();
            return true;
        }
        public async Task<bool> checkDataChanged()
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.IODetails;
            foreach (IODetail iODetail in entrys.ToList())
            {
                if (iODetail.Status == "0")
                {
                    flag = true;
                    break;
                }
            }
            return flag;
        }
    }
}
