﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class UserRightOnReportService:IUserRightOnReportService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public UserRightOnReportService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(string userId, string reportId)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.UserRightOnReports.FirstOrDefault(g => g.UserID.Equals(userId) && g.ReportID.Equals(reportId));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> CreateUserRightOnReport(UserRightOnReport userRightOnReport)
        {
            var entry = warehouseDbContext.UserRightOnReports.AddAsync(userRightOnReport);
            warehouseDbContext.SaveChanges();

            return entry.IsCompleted;
        }

        public async Task<bool> DeleteUserRightOnReport(string userId, string reportId)
        {
            var entry = warehouseDbContext.UserRightOnReports.FirstOrDefault(o => o.UserID.Equals(userId) && o.ReportID.Equals(reportId));
            warehouseDbContext.UserRightOnReports.Remove(entry);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<UserRightOnReport>> GetRightByReportId(string id)
        {
            var entrys = warehouseDbContext.UserRightOnReports.Where(ur => ur.ReportID.Equals(id));

            return await entrys.ToListAsync();
        }

        public async Task<List<UserRightOnReport>> GetRightByUserId(string id)
        {
            var entrys = warehouseDbContext.UserRightOnReports.Where(ur => ur.UserID.Equals(id));

            return await entrys.ToListAsync();
        }

        public async Task<List<UserRightOnReport>> GetRightByUserReportId(string userId, string reportId)
        {
            var entrys = warehouseDbContext.UserRightOnReports.Where(ur => ur.UserID.Equals(userId) && ur.ReportID.Equals(reportId));

            return await entrys.ToListAsync();
        }

        public async Task<bool> UpdateUserRightOnReport(string userId, string reportId, UserRightOnReport userRightOnReport)
        {
            var entrys = warehouseDbContext.UserRightOnReports.FirstOrDefault(o => o.UserID.Equals(userId) && o.ReportID.Equals(reportId));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(userRightOnReport);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
