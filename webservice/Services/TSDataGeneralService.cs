﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class TSDataGeneralService: ITSDataGeneralService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public TSDataGeneralService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> Create(TSDataGeneral entry)
        {
            var entrys = warehouseDbContext.TSDataGenerals.AddAsync(entry);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string warehouseID, DateTime tallyDate)
        {
            var entrys = warehouseDbContext.TSDataGenerals.Where(o => o.WarehouseID.Equals(warehouseID)
                && o.TallyDate==tallyDate);
            warehouseDbContext.TSDataGenerals.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<TSDataGeneral>> GetMultiId(string warehouseID, DateTime tallyDate, int Ordinal, string goodsID)
        {
            var entrys = warehouseDbContext.TSDataGenerals.Where(u => u.WarehouseID.Equals(warehouseID) && u.GoodsID.Equals(goodsID) && u.Ordinal == Ordinal&&u.TallyDate==tallyDate);
            return await entrys.ToListAsync();
        }

        public async Task<List<TSDataGeneral>> GetUnderId(string warehouseID, DateTime TallyDate)
        {
            var entrys = warehouseDbContext.TSDataGenerals.Where(u => u.WarehouseID.Equals(warehouseID) && u.TallyDate == TallyDate);
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(String warehouseID, DateTime tallyDate, int Ordinal, String goodsID, TSDataGeneral entry)
        {
            var entrys = warehouseDbContext.TSDataGenerals.FirstOrDefault(o => o.WarehouseID.Equals(warehouseID) && o.GoodsID.Equals(goodsID) && o.Ordinal == Ordinal&&o.TallyDate==tallyDate);
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(entry);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
