﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class GoodsDataService:IGoodsDataService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public GoodsDataService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(string id)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.GoodsDatas.FirstOrDefault(g => g.GoodsID.Equals(id));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> checkExist(string id, string goodsName, string slPart)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.GoodsDatas.FirstOrDefault(g => g.GoodsID.Equals(id)&&(g.GoodsName.Equals(goodsName)||g.SLPart.Equals(slPart)));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(GoodsData entry)
        {
            var entrys = warehouseDbContext.GoodsDatas.AddAsync(entry);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = warehouseDbContext.GoodsDatas.FirstOrDefault(o => o.GoodsID.Equals(id));
            warehouseDbContext.GoodsDatas.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<GoodsData>> GetAll()
        {
            var entrys = warehouseDbContext.GoodsDatas;
            return await entrys.ToListAsync();
        }

        public async Task<List<GoodsData>> GetUnderId(string id)
        {
            var entrys = warehouseDbContext.GoodsDatas.Where(u => u.GoodsID.Equals(id));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string id, GoodsData entry)
        {
            var entrys = warehouseDbContext.GoodsDatas.FirstOrDefault(o => o.GoodsID.Equals(id));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(entry);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
