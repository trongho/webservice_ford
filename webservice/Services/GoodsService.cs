﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class GoodsService : IGoodsService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public GoodsService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(string id)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.Goods.FirstOrDefault(g => g.GoodsID.Equals(id));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(Goods goods)
        {
            var entrys = warehouseDbContext.Goods.AddAsync(goods);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = warehouseDbContext.Goods.FirstOrDefault(o => o.GoodsID.Equals(id));
            warehouseDbContext.Goods.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<Goods>> GetAllGoods()
        {
            var entrys = warehouseDbContext.Goods;
            return await entrys.ToListAsync();
        }

        public async Task<List<Goods>> GetUnderId(string id)
        {
            var entrys = warehouseDbContext.Goods.Where(u => u.GoodsID.Equals(id));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string id, Goods goods)
        {
            var entrys = warehouseDbContext.Goods.FirstOrDefault(o => o.GoodsID.Equals(id));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(goods);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
