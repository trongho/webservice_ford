﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class CGDataGeneralService: ICGDataGeneralService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public CGDataGeneralService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(string CGDNumber, String goodsID)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.CGDataGenerals.FirstOrDefault(g => g.CGDNumber.Equals(CGDNumber) && g.GoodsID.Equals(goodsID));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(CGDataGeneral CGDataGeneral)
        {
            var entrys = warehouseDbContext.CGDataGenerals.AddAsync(CGDataGeneral);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string CGDNumber)
        {
            var entrys = warehouseDbContext.CGDataGenerals.Where(o => o.CGDNumber.Equals(CGDNumber));
            warehouseDbContext.CGDataGenerals.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<CGDataGeneral>> GetAll()
        {
            var entrys = warehouseDbContext.CGDataGenerals;
            return await entrys.ToListAsync();
        }

        public async Task<string> GetLastId()
        {
            var curentID = warehouseDbContext.CGDataGenerals.OrderByDescending(x => x.CGDNumber).Take(1).Select(x => x.CGDNumber).ToList().FirstOrDefault();
            return curentID;
        }

        public async Task<List<CGDataGeneral>> GetUnderId(string CGDNumber)
        {
            var entrys = warehouseDbContext.CGDataGenerals.Where(u => u.CGDNumber.Equals(CGDNumber));
            return await entrys.ToListAsync();
        }

        public async Task<List<CGDataGeneral>> GetUnderId(string CGDNumber, string goodsID, int Ordinal)
        {
            var entrys = warehouseDbContext.CGDataGenerals.Where(u => u.CGDNumber.Equals(CGDNumber) && u.GoodsID.Equals(goodsID) && u.Ordinal == Ordinal);
            return await entrys.ToListAsync();
        }

        public async Task<List<CGDataGeneral>> GetUnderId(string CGDNumber, string goodsID)
        {
            var entrys = warehouseDbContext.CGDataGenerals.Where(u => u.CGDNumber.Equals(CGDNumber) && u.GoodsID.Equals(goodsID));
            return await entrys.ToListAsync();
        }

        public async Task<List<CGDataGeneral>> GetUnderIdCode(string CGDNumber, string goodsID, string IDCode)
        {
            int idcodeLenght = IDCode.Length;
            int percentIndex = IDCode.IndexOf("%");
            String s1 = IDCode.Substring(0, 1);
            String s2 = IDCode.Substring(percentIndex, 3);
            String s3 = IDCode.Substring(percentIndex + 3, idcodeLenght - s1.Length - s2.Length);

            s2 = s2.Replace(s2, "/");
            IDCode = s1 + s2 + s3;

            var entrys = warehouseDbContext.CGDataGenerals.Where(u => u.CGDNumber.Equals(CGDNumber) && u.GoodsID.Equals(goodsID) && u.IDCode == IDCode);
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string CGDNumber, string goodsID, int Ordinal, CGDataGeneral CGDataGeneral)
        {
            var entrys = warehouseDbContext.CGDataGenerals.FirstOrDefault(o => o.CGDNumber.Equals(CGDNumber) && o.GoodsID.Equals(goodsID) && o.Ordinal == Ordinal);
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(CGDataGeneral);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
