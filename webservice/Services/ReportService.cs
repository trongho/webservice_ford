﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class ReportService : IReportService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public ReportService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }
        public async Task<List<Report>> GetAllReport()
        {
            var entrys = warehouseDbContext.Reports;
            return await entrys.ToListAsync();
        }
    }
}
