﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class TSDetailService: ITSDetailService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public TSDetailService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> Create(TSDetail entry)
        {
            var entrys = warehouseDbContext.TSDetails.AddAsync(entry);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string TSNumber)
        {
            var entrys = warehouseDbContext.TSDetails.Where(o => o.TSNumber.Equals(TSNumber));
            warehouseDbContext.TSDetails.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<bool> Delete(string TSNumber, int Ordinal, string GoodsId)
        {
            var entrys = warehouseDbContext.TSDetails.Where(o => o.TSNumber.Equals(TSNumber)
                && o.GoodsID.Equals(GoodsId) && o.Ordinal==Ordinal);
            warehouseDbContext.TSDetails.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<TSDetail>> GetMultiId(string tsNumber, string goodsID, int Ordinal)
        {
            var entrys = warehouseDbContext.TSDetails.Where(u => u.TSNumber.Equals(tsNumber) && u.GoodsID.Equals(goodsID) && u.Ordinal == Ordinal);
            return await entrys.ToListAsync();
        }

        public async Task<List<TSDetail>> GetUnderId(string id)
        {
            var entrys = warehouseDbContext.TSDetails.Where(u => u.TSNumber.Equals(id));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string TSNumber, int Ordinal, string GoodsID, TSDetail entry)
        {
            var entrys = warehouseDbContext.TSDetails.FirstOrDefault(o => o.TSNumber.Equals(TSNumber) && o.GoodsID.Equals(GoodsID) && o.Ordinal == Ordinal);
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(entry);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
