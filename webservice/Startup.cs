using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Handlers;
using webservice.Helpers;
using webservice.Interfaces;
using webservice.Requirements;
using webservice.Services;

namespace webservice
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddDbContext<WarehouseDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("WarehouseDbConnectionString")));
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IRightService, RightService>();
            services.AddScoped<IUserRightService, UserRightService>();
            services.AddScoped<IGoodsLineService, GoodsLineService>();
            services.AddScoped<IScreenService, ScreenService>();
            services.AddScoped<IUserRightOnScreenService, UserRightOnScreenService>();
            services.AddScoped<IReportService, ReportService>();
            services.AddScoped<IUserRightOnReportService, UserRightOnReportService>();
            services.AddScoped<IBranchService, BranchService>();
            services.AddScoped<IGoodsService, GoodsService>();
            services.AddScoped<IWRRHeaderService, WRRHeaderService>();
            services.AddScoped<IWRRDetailService, WRRDetailService>();
            services.AddScoped<IWRHeaderService, WRHeaderService>();
            services.AddScoped<IWRDetailService, WRDetailService>();
            services.AddScoped<IWRDataHeaderService, WRDataHeaderService>();
            services.AddScoped<IWRDataDetailService, WRDataDetailService>();
            services.AddScoped<IWRDataGeneralService, WRDataGeneralService>();
            services.AddScoped<IHandlingStatusService, HandlingStatusService>();
            services.AddScoped<IOrderStatusService, OrderStatusService>();
            services.AddScoped<IWIRHeaderService, WIRHeaderService>();
            services.AddScoped<IWIRDetailService, WIRDetailService>();
            services.AddScoped<IIOHeaderService, IOHeaderService>();
            services.AddScoped<IIODetailService, IODetailservice>();
            services.AddScoped<IWIDataHeaderService, WIDataHeaderService>();
            services.AddScoped<IWIDataDetailService, WIDataDetailService>();
            services.AddScoped<IWIDataGeneralService, WIDataGeneralService>();
            services.AddScoped<IModalityService, ModalityService>();
            services.AddScoped<IInventoryService, InventoryService>();
            services.AddScoped<IInventoryOfLastMonthsService, InventoryOfLastMonthsService>();
            services.AddScoped<IWarehouseService, WarehouseService>();
            services.AddScoped<IColorService, ColorService>();
            services.AddScoped<IUnitService, UnitService>();
            services.AddScoped<ITallyDataService, TallyDataService>();
            services.AddScoped<ITSHeaderService, TSHeaderService>();
            services.AddScoped<ITSDetailService, TSDetailService>();
            services.AddScoped<ITSDataGeneralService, TSDataGeneralService>();
            services.AddScoped<IGGDataHeaderService, GGDataHeaderService>();
            services.AddScoped<IGGDataDetailService, GGDataDetailService>();
            services.AddScoped<IGGDataGeneralService, GGDataGeneralService>();
            services.AddScoped<ICGDataHeaderService, CGDataHeaderService>();
            services.AddScoped<ICGDataGeneralService, CGDataGeneralService>();
            services.AddScoped<IGoodsDataService, GoodsDataService>();
            services.AddScoped<IDataPrintHeaderService, DataPrintHeaderService>();
            services.AddScoped<IDataPrintGeneralService, DataPrintGeneralService>();
            services.AddScoped<IDataPrintDetailService, DataPrintDetailService>();
            services.AddScoped<ILocationService, LocationService>();
            services.AddScoped<IInventoryDataService, InventoryDataService>();
            services.AddScoped<ICountGoodsService, CountGoodsService>();


            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options =>
                    {
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidateAudience = true,
                            ValidateIssuerSigningKey = true,
                            ValidIssuer = TokenHelper.Issuer,
                            ValidAudience = TokenHelper.Audience,
                            IssuerSigningKey = new SymmetricSecurityKey(Convert.FromBase64String(TokenHelper.Secret))
                        };

                    });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("OnlyNonBlockedUser", policy =>
                {
                    policy.Requirements.Add(new UserStatusRequirement(false));

                });
            });

            services.AddSingleton<IAuthorizationHandler, UserBlockedStatusHandler>();
            services
    // more specific than AddMvc()
    .AddControllersWithViews()
    .SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
                endpoints.MapControllerRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });

        }
    }
}
