﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class LocationHelper
    {
        public static List<LocationModel> Covert(List<Location> entrys)
        {
            var models = entrys.ConvertAll(sc => new LocationModel
            {
                LocationID = sc.LocationID,
                WarehouseID=sc.WarehouseID,
                Description = sc.Description,
                Status = sc.Status,
                CreatedUserID = sc.CreatedUserID,
                CreatedDate = sc.CreatedDate,
                UpdatedUserID = sc.UpdatedUserID,
                UpdatedDate = sc.UpdatedDate,

            });

            return models;
        }
    }
}
