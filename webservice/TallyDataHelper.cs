﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class TallyDataHelper
    {
        public static List<TallyDataModel> Covert(List<TallyData> entrys)
        {
            var models = entrys.ConvertAll(sc => new TallyDataModel
            {
               TSNumber=sc.TSNumber,
               TSDate=sc.TSDate,
               GoodsID=sc.GoodsID,
               GoodsName=sc.GoodsName,
               Quantity=sc.Quantity,
               TotalQuantity=sc.TotalQuantity,
               TotalRow=sc.TotalRow,
               CreatorID=sc.CreatorID,
               CreatedDateTime=sc.CreatedDateTime,
               EditerID=sc.EditerID,
               EditedDateTime=sc.EditedDateTime,
               Status=sc.Status,
               No=sc.No,
            });

            return models;
        }
    }
}
