﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class WRRDetailHelper
    {
        public static List<WRRDetailModel> Covert(List<WRRDetail> entrys)
        {
            var models = entrys.ConvertAll(sc => new WRRDetailModel
            {
                  WRRNumber=sc.WRRNumber,
                  Ordinal=sc.Ordinal,
                  GoodsID=sc.GoodsID,
                  GoodsName=sc.GoodsName,
                  OtherGoodsName=sc.OtherGoodsName,
                  PackingVolume=sc.PackingVolume,
                  TotalQuantity=sc.TotalQuantity,
                  QuantityByItem=sc.QuantityByItem,
                  QuantityByPack=sc.QuantityByPack,
                  LocationID=sc.LocationID,
                  ScanOption=sc.ScanOption,
                  Note=sc.Note,
                  Status=sc.Status,
                  SupplierCode=sc.SupplierCode,
                  ASNNumber=sc.ASNNumber,
                  PackingSlip=sc.PackingSlip,
                  QuantityReceived=sc.QuantityReceived,
                   ReceiptStatus = sc.ReceiptStatus,
                   SLPart = sc.SLPart,
            });

            return models;
        }
    }
}
