﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class WIRHeaderHelper
    {
        public static List<WIRHeaderModel> Covert(List<WIRHeader> entrys)
        {
            var models = entrys.ConvertAll(sc => new WIRHeaderModel
            {
                WIRNumber = sc.WIRNumber,
                WIRDate = sc.WIRDate,
                ReferenceNumber = sc.ReferenceNumber,
                HandlingStatusID = sc.HandlingStatusID,
                HandlingStatusName = sc.HandlingStatusName,
                Note = sc.Note,
                BranchID = sc.BranchID,
                BranchName = sc.BranchName,
                TotalQuantity = sc.TotalQuantity,
                Status = sc.Status,
                CreatedUserID = sc.CreatedUserID,
                CreatedDate = sc.CreatedDate,
                UpdatedUserID = sc.UpdatedUserID,
                UpdatedDate = sc.UpdatedDate,
            });

            return models;
        }
    }
}
