﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class InventoryOfLastMonthsController : ControllerBase
    {
        private readonly IInventoryOfLastMonthsService service;

        public InventoryOfLastMonthsController(IInventoryOfLastMonthsService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = InventoryOfLastMonthsHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByMultiID/{Year}/{Month}/{WareHouseID}")]
        public async Task<IActionResult> GetUnderMultiId(Int16 year, Int16 month,String warehouseID)
        {
            var entrys = await service.GetUnderId(year,month,warehouseID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = InventoryOfLastMonthsHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByMultiGoodsID/{Year}/{Month}/{WareHouseID}/{GoodsID}")]
        public async Task<IActionResult> GetUnderMultiGoodsID(Int16 year, Int16 month, String warehouseID,String goodsID)
        {
            var entrys = await service.GetUnderId(year, month, warehouseID,goodsID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = InventoryOfLastMonthsHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] InventoryOfLastMonths inventoryOfLastMonths)
        {
            var entry = await service.Create(inventoryOfLastMonths);

            return CreatedAtAction(
                 nameof(Get), new {Year= inventoryOfLastMonths.Year,Month=inventoryOfLastMonths.Month,WarehouseID=inventoryOfLastMonths.WarehouseID,GoodsId=inventoryOfLastMonths.GoodsID }, entry);
        }


        [HttpPut]
        [Route("Put/{Year}/{Month}/{WareHouseID}/{GoodsID}")]
        public async Task<IActionResult> Update(Int16 year, Int16 month, String warehouseID, String goodsID, [FromBody] InventoryOfLastMonths inventoryOfLastMonths)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (year !=inventoryOfLastMonths.Year&&month!=inventoryOfLastMonths.Month&&warehouseID!=inventoryOfLastMonths.WarehouseID&&goodsID!=inventoryOfLastMonths.GoodsID)
            {
                return BadRequest();
            }

            await service.Update(year, month, warehouseID,goodsID, inventoryOfLastMonths);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{Year}/{Month}/{WareHouseID}")]
        public async Task<IActionResult> Delete(Int16 year, Int16 month, String warehouseID)
        {
            var entry = await service.Delete(year,month,warehouseID);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("CheckExist/{Year}/{Month}/{WareHouseID}")]
        public async Task<IActionResult> checkExist(Int16 year, Int16 month, String warehouseID)
        {
            var result = await service.checkExist(year, month, warehouseID);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("CheckExistWithGoods/{Year}/{Month}/{WareHouseID}/{goodsID}")]
        public async Task<IActionResult> checkExist(Int16 year, Int16 month, String warehouseID,String goodsID)
        {
            var result = await service.checkExist(year, month, warehouseID,goodsID);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }
    }
}
