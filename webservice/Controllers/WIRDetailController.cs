﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WIRDetailController : ControllerBase
    {
        private readonly IWIRDetailService service;

        public WIRDetailController(IWIRDetailService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = WIRDetailHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{WIRNumber}")]
        public async Task<IActionResult> GetWRRDetailUnderWIRNumber(String wIRNumber)
        {
            var entrys = await service.GetUnderId(wIRNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIRDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByMultiID/{WIRNumber}/{goodsID}/{ordinal}")]
        public async Task<IActionResult> GetUnderMultiId(String wIRNumber, String goodsID, int ordinal)
        {
            var entrys = await service.GetUnderId(wIRNumber, goodsID, ordinal);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIRDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] WIRDetail wIRDetail)
        {
            var entry = await service.Create(wIRDetail);

            return CreatedAtAction(
                 nameof(Get), new { WRRNumber = wIRDetail.WIRNumber }, entry);
        }


        [HttpPut]
        [Route("Put/{WIRNumber}/{goodsID}/{ordinal}/{goodsGroupID}")]
        public async Task<IActionResult> Update(String wIRNumber, String goodsID, int ordinal,String goodsGroupID, [FromBody] WIRDetail wIRDetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (wIRNumber != wIRDetail.WIRNumber)
            {
                return BadRequest();
            }

            await service.Update(wIRNumber, goodsID, ordinal,goodsGroupID, wIRDetail);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{wIRNumber}")]
        public async Task<IActionResult> Delete(String wIRNumber)
        {
            var entry = await service.Delete(wIRNumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("CheckExist/{wIRNumber}/")]
        public async Task<IActionResult> checkExist(String wIRNumber)
        {
            var result = await service.checkExist(wIRNumber);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("lastWIRNumber")]
        public async Task<IActionResult> GetLastWIRNumber()
        {
            var WIRNumber = await service.GetLastId();
            if (WIRNumber == null)
            {
                return NotFound();
            }

            return Ok(WIRNumber);
        }

        [HttpGet]
        [Route("GetByMultiID2/{WIRNumber}/{goodsID}/{ordinal}/{goodsGroupsID}")]
        public async Task<IActionResult> GetUnderMultiId2(String wIRNumber, String goodsID, int ordinal, String goodsGroupsID)
        {
            var entrys = await service.GetUnderId(wIRNumber, goodsID, ordinal, goodsGroupsID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIRDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByPickerName/{WIRNumber}/{pickerName}")]
        public async Task<IActionResult> GetUnderPickerName(String wIRNumber, String pickerName)
        {
            var entrys = await service.GetUnderPickerName(wIRNumber, pickerName);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIRDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByGoodsGroupID/{WIRNumber}/{goodsGroupID}")]
        public async Task<IActionResult> GetUnderGoodsGroupID(String wIRNumber, String goodsGroupID)
        {
            var entrys = await service.GetUnderGoodsGroupID(wIRNumber, goodsGroupID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIRDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }
    }
}
