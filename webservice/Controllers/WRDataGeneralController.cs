﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WRDataGeneralController : ControllerBase
    {
        private readonly IWRDataGeneralService service;

        public WRDataGeneralController(IWRDataGeneralService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = WRDataGeneralHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{WRDNumber}")]
        public async Task<IActionResult> GetWRDataGeneralUnderWRDNumber(String wRDNumber)
        {
            var entrys = await service.GetUnderId(wRDNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetBeetwenNgayNhanHang/{fromDate}/{toDate}")]
        public async Task<IActionResult> GetBeetwenNgayNhanHang(DateTime fromDate,DateTime toDate)
        {
            var entrys = await service.GetByNgayNhanHang(fromDate,toDate);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByMultiID/{WRDNumber}/{goodsID}/{ordinal}")]
        public async Task<IActionResult> GetUnderMultiId(String wRDNumber, String goodsID, int ordinal)
        {
            var entrys = await service.GetUnderId(wRDNumber, goodsID, ordinal);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByIDCode/{WRDNumber}/{goodsID}/{idCode}")]
        public async Task<IActionResult> GetUnderMultiIdCode(String wRDNumber, String goodsID, String IdCode)
        {
            var entrys = await service.GetUnderIdCode(wRDNumber, goodsID, IdCode);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByWRDNumberAndGoodsID/{WRDNumber}/{goodsID}")]
        public async Task<IActionResult> GetUnderMultiGoodsID(String wRDNumber, String goodsID)
        {
            var entrys = await service.GetUnderId(wRDNumber, goodsID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] WRDataGeneral WRDataGeneral)
        {
            var entry = await service.Create(WRDataGeneral);

            return CreatedAtAction(
                 nameof(Get), new { WRDNumber = WRDataGeneral.WRDNumber }, entry);
        }


        [HttpPut]
        [Route("Put/{WRDNumber}/{goodsID}/{ordinal}")]
        public async Task<IActionResult> Update(String wRDNumber, String goodsID, int ordinal, [FromBody] WRDataGeneral WRDataGeneral)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (wRDNumber != WRDataGeneral.WRDNumber)
            {
                return BadRequest();
            }

            await service.Update(wRDNumber, goodsID, ordinal, WRDataGeneral);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{wRDNumber}")]
        public async Task<IActionResult> Delete(String wRDNumber)
        {
            var entry = await service.Delete(wRDNumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("CheckExist/{wRDNumber}/{goodsID}")]
        public async Task<IActionResult> checkExist(String wRDNumber,String goodsID)
        {
            var result = await service.checkExist(wRDNumber,goodsID);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("CheckGoodsIDDuplicate/{wRDNumber}/{goodsID}")]
        public async Task<IActionResult> CheckGoodsIDDuplicate(String wRDNumber, String goodsID)
        {
            var result = await service.checkGoodsIDDuplicate(wRDNumber, goodsID);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("CheckIDCodeIsScaned/{wRDNumber}/{goodsID}/{idCode}")]
        public async Task<IActionResult> checkIDCodeIsScaned(String wRDNumber, String goodsID,String idCode)
        {
            var result = await service.checkIDCodeIsScaned(wRDNumber,idCode, goodsID);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("lastWRDNumber")]
        public async Task<IActionResult> GetLastWRDNumber()
        {
            var WRDNumber = await service.GetLastId();
            if (WRDNumber == null)
            {
                return NotFound();
            }

            return Ok(WRDNumber);
        }

        [HttpGet]
        [Route("CheckQuantity/{wrdNumber}/{editedDateTime}")]
        public async Task<IActionResult> CheckQuantity(String wrdNumber,DateTime editedDateTime)
        {
            var entry = await service.checkQuantity(wrdNumber,editedDateTime);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("GetTotalQuantity/{wRDNumber}")]
        public async Task<IActionResult> GetTotalQuantity(String wRDNumber)
        {
            var result = await service.getTotalQuantity(wRDNumber);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }
        [HttpGet]
        [Route("GetTotalGoods/{wRDNumber}")]
        public async Task<IActionResult> GetTotalGoods(String wRDNumber)
        {
            var result = await service.getTotalGoods(wRDNumber);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }


    }
}
