﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DataPrintDetailController : ControllerBase
    {

        private readonly IDataPrintDetailService service;

        public DataPrintDetailController(IDataPrintDetailService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = DataPrintDetailHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{dataPrintNumber}")]
        public async Task<IActionResult> GetDataPrintDetailUnderdataPrintNumber(String dataPrintNumber)
        {
            var entrys = await service.GetUnderId(dataPrintNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = DataPrintDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByMultiID/{dataPrintNumber}/{goodsID}/{ordinal}")]
        public async Task<IActionResult> GetUnderMultiId(String dataPrintNumber, String goodsID, int ordinal)
        {
            var entrys = await service.GetUnderId(dataPrintNumber, goodsID, ordinal);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = DataPrintDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }


        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] DataPrintDetail DataPrintDetail)
        {
            var entry = await service.Create(DataPrintDetail);

            return CreatedAtAction(
                 nameof(Get), new { dataPrintNumber = DataPrintDetail.DataPrintNumber }, entry);
        }


        [HttpPut]
        [Route("Put/{dataPrintNumber}/{goodsID}/{ordinal}")]
        public async Task<IActionResult> Update(String dataPrintNumber, String goodsID, int ordinal, [FromBody] DataPrintDetail DataPrintDetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (dataPrintNumber != DataPrintDetail.DataPrintNumber)
            {
                return BadRequest();
            }

            await service.Update(dataPrintNumber, goodsID, ordinal, DataPrintDetail);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{dataPrintNumber}")]
        public async Task<IActionResult> Delete(String dataPrintNumber)
        {
            var entry = await service.Delete(dataPrintNumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("CheckExist/{dataPrintNumber}/{goodsID}")]
        public async Task<IActionResult> checkExist(String dataPrintNumber, String goodsID, int ordinal)
        {
            var result = await service.checkExist(dataPrintNumber, goodsID, ordinal);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }
    }
}
