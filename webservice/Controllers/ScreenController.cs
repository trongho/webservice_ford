﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ScreenController:ControllerBase
    {
        private readonly IScreenService screenService;

        public ScreenController(IScreenService screenService)
        {
            this.screenService = screenService;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await screenService.GetAllScreen();
            if (entrys == null)
            {
                return NotFound();
            }

            var screenModels = ScreenHelper.CovertScreens(entrys);

            return Ok(screenModels);
        }
    }
}
