﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class WRHeaderController : ControllerBase
    {
        private readonly IWRHeaderService wRHeaderService;

        public WRHeaderController(IWRHeaderService wRHeaderService)
        {
            this.wRHeaderService = wRHeaderService;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await wRHeaderService.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = WRHeaderHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{wRNumber}")]
        public async Task<IActionResult> GetWRHeaderUnderWRRNumber(String wRNumber)
        {
            var entrys = await wRHeaderService.GetUnderId(wRNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByMonthYear/{*wRDate}")]
        public async Task<IActionResult> GetWRHeaderUnderWRDate(String wrDate)
        {
            var entrys = await wRHeaderService.GetUnderWRDate(wrDate);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByLastMonthYear/{*wRDate}")]
        public async Task<IActionResult> GetWRHeaderUnderLastWRDate(String wrDate)
        {
            var entrys = await wRHeaderService.GetUnderLastWRDate(wrDate);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] WRHeader wRHeader)
        {
            var entry = await wRHeaderService.Create(wRHeader);

            return CreatedAtAction(
                 nameof(Get), new { WRRNumber = wRHeader.WRNumber }, entry);
        }

        [HttpPut]
        [Route("Put/{WRNumber}")]
        public async Task<IActionResult> Update(String wRNumber, [FromBody] WRHeader wRHeader)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (wRNumber != wRHeader.WRNumber)
            {
                return BadRequest();
            }

            await wRHeaderService.Update(wRNumber, wRHeader);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{wRNumber}")]
        public async Task<IActionResult> Delete(String wRNumber)
        {
            var entry = await wRHeaderService.Delete(wRNumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("CheckExist/{wRNumber}/")]
        public async Task<IActionResult> checkExist(String wRNumber)
        {
            var result = await wRHeaderService.checkExist(wRNumber);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("lastWRNumber")]
        public async Task<IActionResult> GetLastWRNumber()
        {
            var WRNumber = await wRHeaderService.GetLastId();
            if (WRNumber == null)
            {
                return NotFound();
            }

            return Ok(WRNumber);
        }

        [HttpGet]
        [Route("FilterByDate/{fromDate}/{toDate}")]
        public async Task<IActionResult> GetWRHeaderUnderDate(DateTime fromDate, DateTime toDate)
        {
            var entrys = await wRHeaderService.GetUnderDate(fromDate, toDate);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByBranch/{branchID}")]
        public async Task<IActionResult> GetWRHeaderUnderBranchID(String branchID)
        {
            var entrys = await wRHeaderService.GetUnderBranchID(branchID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByHandlingStatus/{handlingStatusID}")]
        public async Task<IActionResult> GetWRHeaderUnderHandlingStatusID(String handlingStatusID)
        {
            var entrys = await wRHeaderService.GetUnderHandlingStatusID(handlingStatusID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByModality/{modalityID}")]
        public async Task<IActionResult> GetWRHeaderUnderModalityID(String modalityID)
        {
            var entrys = await wRHeaderService.GetUnderModalityID(modalityID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }
    }
}
