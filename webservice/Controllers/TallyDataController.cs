﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TallyDataController : ControllerBase
    {
        private readonly ITallyDataService service;

        public TallyDataController(ITallyDataService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("GetByID/{id}")]
        public async Task<IActionResult> GetUnderID(String id)
        {
            var entrys = await service.GetUnderId(id);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = TallyDataHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByMultiID/{TSNumber}/{GoodsID}/{No}")]
        public async Task<IActionResult> GetUnderMultiId(String TSNumber, String goodsID, int no)
        {
            var entrys = await service.GetMultiId(TSNumber, goodsID, no);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = TallyDataHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] TallyData entry)
        {
            var entrys = await service.Create(entry);

            return CreatedAtAction(
                 nameof(GetUnderID), new { TSNumber = entry.TSNumber,GoodsID=entry.GoodsID,No=entry.No }, entry);
        }

        [HttpPut]
        [Route("Put/{TSNumber}/{GoodsID}/{No}")]
        public async Task<IActionResult> Update(String TSNumber, String GoodsID, int No, [FromBody] TallyData entry)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (TSNumber != entry.TSNumber)
            {
                return BadRequest();
            }

            await service.Update(TSNumber, GoodsID,No, entry);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IActionResult> Delete(String id)
        {
            var entry = await service.Delete(id);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpDelete]
        [Route("DeleteDetail/{TSNumber}/{GoodsID}/{No}")]
        public async Task<IActionResult> DeleteDetail(String TSNumber, String GoodsID, int No)
        {
            var entry = await service.Delete(TSNumber, GoodsID,No);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("lastID")]
        public async Task<IActionResult> GetLastId()
        {
            var id = await service.GetLastId();
            if (id == null)
            {
                return NotFound();
            }

            return Ok(id);
        }
    }
}
