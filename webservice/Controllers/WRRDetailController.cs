﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WRRDetailController : ControllerBase
    {
        private readonly IWRRDetailService wRRDetailService;

        public WRRDetailController(IWRRDetailService wRRDetailService)
        {
            this.wRRDetailService = wRRDetailService;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await wRRDetailService.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = WRRDetailHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{WRRNumber}")]
        public async Task<IActionResult> GetWRRDetailUnderWRRNumber(String wRRNumber)
        {
            var entrys = await wRRDetailService.GetUnderId(wRRNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRRDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByMultiID/{WRRNumber}/{goodsID}/{ordinal}")]
        public async Task<IActionResult> GetUnderMultiId(String wRRNumber, String goodsID, int ordinal)
        {
            var entrys = await wRRDetailService.GetUnderId(wRRNumber, goodsID, ordinal);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRRDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] WRRDetail wRRDetail)
        {
            var entry = await wRRDetailService.Create(wRRDetail);

            return CreatedAtAction(
                 nameof(Get), new { WRRNumber = wRRDetail.WRRNumber }, entry);
        }


        [HttpPut]
        [Route("Put/{WRRNumber}/{goodsID}/{ordinal}")]
        public async Task<IActionResult> Update(String wRRNumber, String goodsID, int ordinal, [FromBody] WRRDetail wRRDetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (wRRNumber != wRRDetail.WRRNumber)
            {
                return BadRequest();
            }

            await wRRDetailService.Update(wRRNumber,goodsID,ordinal, wRRDetail);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{wRRNumber}")]
        public async Task<IActionResult> Delete(String wRRNumber)
        {
            var entry = await wRRDetailService.Delete(wRRNumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("CheckExist/{wRRNumber}/")]
        public async Task<IActionResult> checkExist(String wRRNumber)
        {
            var result = await wRRDetailService.checkExist(wRRNumber);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("lastWRRNumber")]
        public async Task<IActionResult> GetLastWRRNumber()
        {
            var WRRNumber = await wRRDetailService.GetLastId();
            if (WRRNumber == null)
            {
                return NotFound();
            }

            return Ok(WRRNumber);
        }
    }
}
