﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CGDataGeneralController : ControllerBase
    {
        private readonly ICGDataGeneralService service;

        public CGDataGeneralController(ICGDataGeneralService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = CGDataGeneralHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{CGDNumber}")]
        public async Task<IActionResult> GetCGDataGeneralUnderCGDNumber(String CGDNumber)
        {
            var entrys = await service.GetUnderId(CGDNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = CGDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByMultiID/{CGDNumber}/{goodsID}/{ordinal}")]
        public async Task<IActionResult> GetUnderMultiId(String CGDNumber, String goodsID, int ordinal)
        {
            var entrys = await service.GetUnderId(CGDNumber, goodsID, ordinal);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = CGDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByIDCode/{CGDNumber}/{goodsID}/{*idCode}")]
        public async Task<IActionResult> GetUnderMultiIdCode(String CGDNumber, String goodsID, String IdCode)
        {
            var entrys = await service.GetUnderIdCode(CGDNumber, goodsID, IdCode);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = CGDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByCGDNumberAndGoodsID/{CGDNumber}/{goodsID}")]
        public async Task<IActionResult> GetUnderMultiGoodsID(String CGDNumber, String goodsID)
        {
            var entrys = await service.GetUnderId(CGDNumber, goodsID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = CGDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] CGDataGeneral CGDataGeneral)
        {
            var entry = await service.Create(CGDataGeneral);

            return CreatedAtAction(
                 nameof(Get), new { CGDNumber = CGDataGeneral.CGDNumber }, entry);
        }


        [HttpPut]
        [Route("Put/{CGDNumber}/{goodsID}/{ordinal}")]
        public async Task<IActionResult> Update(String CGDNumber, String goodsID, int ordinal, [FromBody] CGDataGeneral CGDataGeneral)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (CGDNumber != CGDataGeneral.CGDNumber)
            {
                return BadRequest();
            }

            await service.Update(CGDNumber, goodsID, ordinal, CGDataGeneral);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{CGDNumber}")]
        public async Task<IActionResult> Delete(String CGDNumber)
        {
            var entry = await service.Delete(CGDNumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("CheckExist/{CGDNumber}/{goodsID}")]
        public async Task<IActionResult> checkExist(String CGDNumber, String goodsID)
        {
            var result = await service.checkExist(CGDNumber, goodsID);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("lastCGDNumber")]
        public async Task<IActionResult> GetLastCGDNumber()
        {
            var CGDNumber = await service.GetLastId();
            if (CGDNumber == null)
            {
                return NotFound();
            }

            return Ok(CGDNumber);
        }
    }
}
