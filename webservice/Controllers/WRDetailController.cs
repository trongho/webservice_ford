﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WRDetailController : ControllerBase
    {
        private readonly IWRDetailService wRDetailService;

        public WRDetailController(IWRDetailService wRDetailService)
        {
            this.wRDetailService = wRDetailService;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await wRDetailService.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = WRDetailHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{WRNumber}")]
        public async Task<IActionResult> GetWRDetailUnderWRNumber(String wRNumber)
        {
            var entrys = await wRDetailService.GetUnderId(wRNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByMultiID/{WRNumber}/{goodsID}/{ordinal}")]
        public async Task<IActionResult> GetUnderMultiId(String wRNumber, String goodsID, int ordinal)
        {
            var entrys = await wRDetailService.GetUnderId(wRNumber, goodsID, ordinal);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] WRDetail wRDetail)
        {
            var entry = await wRDetailService.Create(wRDetail);

            return CreatedAtAction(
                 nameof(Get), new { WRNumber = wRDetail.WRNumber }, entry);
        }


        [HttpPut]
        [Route("Put/{WRNumber}/{goodsID}/{ordinal}")]
        public async Task<IActionResult> Update(String wRNumber, String goodsID, int ordinal, [FromBody] WRDetail wRDetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (wRNumber != wRDetail.WRNumber)
            {
                return BadRequest();
            }

            await wRDetailService.Update(wRNumber, goodsID, ordinal, wRDetail);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{wRNumber}")]
        public async Task<IActionResult> Delete(String wRNumber)
        {
            var entry = await wRDetailService.Delete(wRNumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("CheckExist/{wRNumber}")]
        public async Task<IActionResult> checkExist(String wRNumber)
        {
            var result = await wRDetailService.checkExist(wRNumber);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("lastWRNumber")]
        public async Task<IActionResult> GetLastWRNumber()
        {
            var WRNumber = await wRDetailService.GetLastId();
            if (WRNumber == null)
            {
                return NotFound();
            }

            return Ok(WRNumber);
        }

        [HttpGet]
        [Route("CheckDataChanged")]
        public async Task<IActionResult> CheckDataChanged()
        {
            var result = await wRDetailService.checkDataChanged();
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }
    }
}
