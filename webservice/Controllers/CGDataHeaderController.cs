﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
        [ApiController]
        [Route("api/[controller]")]
        public class CGDataHeaderController : ControllerBase
        {
            private readonly ICGDataHeaderService CGDataHeaderService;

            public CGDataHeaderController(ICGDataHeaderService CGDataHeaderService)
            {
                this.CGDataHeaderService = CGDataHeaderService;
            }

            [HttpGet]
            [Route("")]
            public async Task<IActionResult> Get()
            {
                var entrys = await CGDataHeaderService.GetAll();
                if (entrys == null)
                {
                    return NotFound();
                }
                var entryModels = CGDataHeaderHelper.Covert(entrys);
                return Ok(entryModels);
            }

            [HttpGet]
            [Route("GetByID/{CGDNumber}")]
            public async Task<IActionResult> GetCGDataHeaderUnderCGDNumber(String CGDNumber)
            {
                var entrys = await CGDataHeaderService.GetUnderId(CGDNumber);
                if (entrys == null)
                {
                    return NotFound();
                }

                var entryModels = CGDataHeaderHelper.Covert(entrys);

                return Ok(entryModels);
            }

            [HttpPost]
            [Route("Post")]
            public async Task<IActionResult> Create([FromBody] CGDataHeader CGDataHeader)
            {
                var entry = await CGDataHeaderService.Create(CGDataHeader);

                return CreatedAtAction(
                     nameof(Get), new { WRRNumber = CGDataHeader.CGDNumber }, entry);
            }

            [HttpPut]
            [Route("Put/{CGDNumber}")]
            public async Task<IActionResult> Update(String CGDNumber, [FromBody] CGDataHeader CGDataHeader)
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                if (CGDNumber != CGDataHeader.CGDNumber)
                {
                    return BadRequest();
                }

                await CGDataHeaderService.Update(CGDNumber, CGDataHeader);

                return new NoContentResult();
            }

            [HttpDelete]
            [Route("Delete/{CGDNumber}")]
            public async Task<IActionResult> Delete(String CGDNumber)
            {
                var entry = await CGDataHeaderService.Delete(CGDNumber);
                if (entry == null)
                {
                    return NotFound();
                }

                return Ok(entry);
            }

            [HttpGet]
            [Route("CheckExist/{CGDNumber}/")]
            public async Task<IActionResult> checkExist(String CGDNumber)
            {
                var result = await CGDataHeaderService.checkExist(CGDNumber);
                if (result == null)
                {
                    return NotFound();
                }

                return Ok(result);
            }

            [HttpGet]
            [Route("lastCGDNumber")]
            public async Task<IActionResult> GetLastCGDNumber()
            {
                var CGDNumber = await CGDataHeaderService.GetLastId();
                if (CGDNumber == null)
                {
                    return NotFound();
                }

                return Ok(CGDNumber);
            }

            [HttpGet]
            [Route("FilterByDate/{fromDate}/{toDate}")]
            public async Task<IActionResult> GetCGDataHeaderUnderDate(DateTime fromDate, DateTime toDate)
            {
                var entrys = await CGDataHeaderService.GetUnderDate(fromDate, toDate);
                if (entrys == null)
                {
                    return NotFound();
                }

                var entryModels = CGDataHeaderHelper.Covert(entrys);

                return Ok(entryModels);
            }

            [HttpGet]
            [Route("FilterByBranch/{branchID}")]
            public async Task<IActionResult> GetCGDataHeaderUnderBranchID(String branchID)
            {
                var entrys = await CGDataHeaderService.GetUnderBranchID(branchID);
                if (entrys == null)
                {
                    return NotFound();
                }

                var entryModels = CGDataHeaderHelper.Covert(entrys);

                return Ok(entryModels);
            }

            [HttpGet]
            [Route("FilterByHandlingStatus/{handlingStatusID}")]
            public async Task<IActionResult> GetWRRHeaderUnderHandlingStatusID(String handlingStatusID)
            {
                var entrys = await CGDataHeaderService.GetUnderHandlingStatusID(handlingStatusID);
                if (entrys == null)
                {
                    return NotFound();
                }

                var entryModels = CGDataHeaderHelper.Covert(entrys);

                return Ok(entryModels);
            }

            [HttpGet]
            [Route("FilterByOrderStatus/{orderStatusID}")]
            public async Task<IActionResult> GetCGDataHeaderUnderOrderStatusID(String orderStatusID)
            {
                var entrys = await CGDataHeaderService.GetUnderOrderStatusID(orderStatusID);
                if (entrys == null)
                {
                    return NotFound();
                }

                var entryModels = CGDataHeaderHelper.Covert(entrys);

                return Ok(entryModels);
            }
        }
}
