﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WIRHeaderController : ControllerBase
    {
        private readonly IWIRHeaderService wIRHeaderService;

        public WIRHeaderController(IWIRHeaderService wIRHeaderService)
        {
            this.wIRHeaderService = wIRHeaderService;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await wIRHeaderService.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = WIRHeaderHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{wIRNumber}")]
        public async Task<IActionResult> GetWRRHeaderUnderWIRNumber(String wIRNumber)
        {
            var entrys = await wIRHeaderService.GetUnderId(wIRNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIRHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByDate/{fromDate}/{toDate}")]
        public async Task<IActionResult> GetWRRHeaderUnderDate(DateTime fromDate, DateTime toDate)
        {
            var entrys = await wIRHeaderService.GetUnderDate(fromDate, toDate);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIRHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByBranch/{branchID}")]
        public async Task<IActionResult> GetWRRHeaderUnderBranchID(String branchID)
        {
            var entrys = await wIRHeaderService.GetUnderBranchID(branchID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIRHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByHandlingStatus/{handlingStatusID}")]
        public async Task<IActionResult> GetWRRHeaderUnderHandlingStatusID(String handlingStatusID)
        {
            var entrys = await wIRHeaderService.GetUnderHandlingStatusID(handlingStatusID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIRHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] WIRHeader wIRHeader)
        {
            var entry = await wIRHeaderService.Create(wIRHeader);

            return CreatedAtAction(
                 nameof(Get), new { WRRNumber = wIRHeader.WIRNumber }, entry);
        }

        [HttpPut]
        [Route("Put/{WIRNumber}")]
        public async Task<IActionResult> Update(String wIRNumber, [FromBody] WIRHeader wIRHeader)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (wIRNumber != wIRHeader.WIRNumber)
            {
                return BadRequest();
            }

            await wIRHeaderService.Update(wIRNumber, wIRHeader);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{wIRNumber}")]
        public async Task<IActionResult> Delete(String wIRNumber)
        {
            var entry = await wIRHeaderService.Delete(wIRNumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("CheckExist/{wIRNumber}/")]
        public async Task<IActionResult> checkExist(String wIRNumber)
        {
            var result = await wIRHeaderService.checkExist(wIRNumber);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("lastWIRNumber")]
        public async Task<IActionResult> GetLastWIRNumber()
        {
            var WRRNumber = await wIRHeaderService.GetLastId();
            if (WRRNumber == null)
            {
                return NotFound();
            }

            return Ok(WRRNumber);
        }

        [HttpGet]
        [Route("Filter/{branchID}/{fromDate}/{toDate}/{handlingStatusID}")]
        public async Task<IActionResult> Filter(String branchID, DateTime fromDate, DateTime toDate, String handlingStatusID)
        {
            var entrys = await wIRHeaderService.Filter(branchID, fromDate, toDate, handlingStatusID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIRHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByDateAndHandlingStatus/{fromDate}/{toDate}/{handlingStatusID}")]
        public async Task<IActionResult> FilterByDateAndHandlingStatus(DateTime fromDate, DateTime toDate, String handlingStatusID)
        {
            var entrys = await wIRHeaderService.FilterByDateAndHandlingStatus(fromDate, toDate, handlingStatusID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIRHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByDateAndBranch/{fromDate}/{toDate}/{branchID}")]
        public async Task<IActionResult> FilterByDateAndBranch(DateTime fromDate, DateTime toDate, String branchID)
        {
            var entrys = await wIRHeaderService.FilterByDateAndBranch(fromDate, toDate, branchID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIRHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }
    }
}
