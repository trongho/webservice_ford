﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WIDataGeneralController : ControllerBase
    {
        private readonly IWIDataGeneralService service;

        public WIDataGeneralController(IWIDataGeneralService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = WIDataGeneralHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{WIDNumber}")]
        public async Task<IActionResult> GetWRDataGeneralUnderWRDNumber(String wIDNumber)
        {
            var entrys = await service.GetUnderId(wIDNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByMultiID/{WIDNumber}/{goodsID}/{ordinal}")]
        public async Task<IActionResult> GetUnderMultiId(String wIDNumber, String goodsID, int ordinal)
        {
            var entrys = await service.GetUnderId(wIDNumber, goodsID, ordinal);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByGoodsGroupIDAndGoodsID/{WIDNumber}/{goodsID}/{ordinal}/{goodsGroupsID}")]
        public async Task<IActionResult> GetByGoodsGroupIDAndGoodsID(String wIDNumber, String goodsID, int ordinal,String goodsGroupsID)
        {
            var entrys = await service.GetUnderId(wIDNumber, goodsID, ordinal,goodsGroupsID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByIDCode/{WIDNumber}/{goodsID}/{*idCode}")]
        public async Task<IActionResult> GetUnderMultiIdCode(String wIDNumber, String goodsID, String IdCode)
        {
            var entrys = await service.GetUnderIdCode(wIDNumber, goodsID, IdCode);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByWIDNumberAndGoodsID/{WIDNumber}/{goodsID}")]
        public async Task<IActionResult> GetUnderMultiGoodsID(String wIDNumber, String goodsID)
        {
            var entrys = await service.GetUnderId(wIDNumber, goodsID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByWIDNumberAndGoodsIDAndGoodsGroupID/{WIDNumber}/{goodsID}/{goodsGroupID}")]
        public async Task<IActionResult> GetUnderMultiID(String wIDNumber, String goodsID,String goodsGroupID)
        {
            var entrys = await service.GetUnderId(wIDNumber, goodsID,goodsGroupID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByPickerName/{WIDNumber}/{pickerName}")]
        public async Task<IActionResult> GetUnderPickerName(String wIDNumber, String pickerName)
        {
            var entrys = await service.GetUnderPickerName(wIDNumber,pickerName);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByGoodsGroupID/{WIDNumber}/{goodsGroupID}")]
        public async Task<IActionResult> GetUnderGoodsGroupID(String wIDNumber, String goodsGroupID)
        {
            var entrys = await service.GetUnderGoodsGroupID(wIDNumber,goodsGroupID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByScanOption/{WIDNumber}/{ScanOption}")]
        public async Task<IActionResult> GetUnderScanOption(String wIDNumber,Int16 scanOption)
        {
            var entrys = await service.GetUnderScanOption(wIDNumber,scanOption);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("Filter/{WIDNumber}/{GoodsGroupID}/{PickerName}/{ScanOption}")]
        public async Task<IActionResult> Filter(String wIDNumber,String goodsGroupID,String pickerName, Int16 scanOption)
        {
            var entrys = await service.Filter(wIDNumber,goodsGroupID,pickerName, scanOption);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByGoodsGroupIDAndpickerName/{WIDNumber}/{GoodsGroupID}/{PickerName}")]
        public async Task<IActionResult> FilterByGoodsGroupIDAndpickerName(String wIDNumber, String goodsGroupID, String pickerName)
        {
            var entrys = await service.FilterByGoodsGroupIDAndPickerName(wIDNumber, goodsGroupID, pickerName);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByGoodsGroupIDAndScanOption/{WIDNumber}/{GoodsGroupID}/{ScanOption}")]
        public async Task<IActionResult> FilterByGoodsGroupIDAndScanOption(String wIDNumber, String goodsGroupID, Int16 scanOption)
        {
            var entrys = await service.FilterByGoodsGroupIDAndScanOption(wIDNumber, goodsGroupID, scanOption);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByPickerNameAndScanOption/{WIDNumber}/{PickerName}/{ScanOption}")]
        public async Task<IActionResult> FilterByPickerNameAndScanOption(String wIDNumber, String pickerName, Int16 scanOption)
        {
            var entrys = await service.FilterByGoodsPickerNameAndScanOption(wIDNumber,pickerName, scanOption);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }


        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] WIDataGeneral WIDataGeneral)
        {
            var entry = await service.Create(WIDataGeneral);

            return CreatedAtAction(
                 nameof(Get), new { WRDNumber = WIDataGeneral.WIDNumber }, entry);
        }


        [HttpPut]
        [Route("Put/{WIDNumber}/{goodsID}/{ordinal}/{goodsGroupID}")]
        public async Task<IActionResult> Update(String wIDNumber, String goodsID, int ordinal,String goodsGroupID, [FromBody] WIDataGeneral WIDataGeneral)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (wIDNumber != WIDataGeneral.WIDNumber)
            {
                return BadRequest();
            }

            await service.Update(wIDNumber, goodsID, ordinal,goodsGroupID, WIDataGeneral);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{wIDNumber}")]
        public async Task<IActionResult> Delete(String wIDNumber)
        {
            var entry = await service.Delete(wIDNumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("CheckExist/{wIDNumber}/{goodsID}")]
        public async Task<IActionResult> checkExist(String wIDNumber, String goodsID)
        {
            var result = await service.checkExist(wIDNumber, goodsID);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("lastWIDNumber")]
        public async Task<IActionResult> GetLastWIDNumber()
        {
            var WIDNumber = await service.GetLastId();
            if (WIDNumber == null)
            {
                return NotFound();
            }

            return Ok(WIDNumber);
        }

        [HttpGet]
        [Route("getGoodsGroupIDs/{wIDNumber}")]
        public async Task<IActionResult> GetGoodsGroupIDs(String widNumber)
        {
            var result = await service.GetGoodsGroupID(widNumber);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("getPickerNames/{wIDNumber}")]
        public async Task<IActionResult> GetPickerNames(String widNumber)
        {
            var result = await service.GetPickerName(widNumber);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }
    }
}
