﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class IODetailController : ControllerBase
    {
        private readonly IIODetailService service;

        public IODetailController(IIODetailService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = IODetailHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{IONumber}")]
        public async Task<IActionResult> GetIODetailUnderIONumber(String IONumber)
        {
            var entrys = await service.GetUnderId(IONumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = IODetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByMultiID/{IONumber}/{goodsID}/{ordinal}")]
        public async Task<IActionResult> GetUnderMultiId(String IONumber, String goodsID, int ordinal)
        {
            var entrys = await service.GetUnderId(IONumber, goodsID, ordinal);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = IODetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] IODetail IODetail)
        {
            var entry = await service.Create(IODetail);

            return CreatedAtAction(
                 nameof(Get), new { IONumber = IODetail.IONumber }, entry);
        }


        [HttpPut]
        [Route("Put/{IONumber}/{goodsID}/{ordinal}")]
        public async Task<IActionResult> Update(String IONumber, String goodsID, int ordinal, [FromBody] IODetail IODetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (IONumber != IODetail.IONumber)
            {
                return BadRequest();
            }

            await service.Update(IONumber, goodsID, ordinal, IODetail);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{IONumber}")]
        public async Task<IActionResult> Delete(String IONumber)
        {
            var entry = await service.Delete(IONumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("CheckExist/{IONumber}/")]
        public async Task<IActionResult> checkExist(String IONumber)
        {
            var result = await service.checkExist(IONumber);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("lastIONumber")]
        public async Task<IActionResult> GetLastIONumber()
        {
            var IONumber = await service.GetLastId();
            if (IONumber == null)
            {
                return NotFound();
            }

            return Ok(IONumber);
        }

        [HttpGet]
        [Route("CheckDataChanged")]
        public async Task<IActionResult> CheckDataChanged()
        {
            var result = await service.checkDataChanged();
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }
    }
}
