﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class DataPrintGeneralController : ControllerBase
    {
        private readonly IDataPrintGeneralService service;

        public DataPrintGeneralController(IDataPrintGeneralService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = DataPrintGeneralHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{dataPrintNumber}")]
        public async Task<IActionResult> GetDataPrintGeneralUnderdataPrintNumber(String dataPrintNumber)
        {
            var entrys = await service.GetUnderId(dataPrintNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = DataPrintGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByMultiID/{dataPrintNumber}/{goodsID}/{ordinal}")]
        public async Task<IActionResult> GetUnderMultiId(String dataPrintNumber, String goodsID, int ordinal)
        {
            var entrys = await service.GetUnderId(dataPrintNumber, goodsID, ordinal);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = DataPrintGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }


        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] DataPrintGeneral DataPrintGeneral)
        {
            var entry = await service.Create(DataPrintGeneral);

            return CreatedAtAction(
                 nameof(Get), new { dataPrintNumber = DataPrintGeneral.DataPrintNumber }, entry);
        }


        [HttpPut]
        [Route("Put/{dataPrintNumber}/{goodsID}/{ordinal}")]
        public async Task<IActionResult> Update(String dataPrintNumber, String goodsID, int ordinal, [FromBody] DataPrintGeneral DataPrintGeneral)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (dataPrintNumber != DataPrintGeneral.DataPrintNumber)
            {
                return BadRequest();
            }

            await service.Update(dataPrintNumber, goodsID, ordinal, DataPrintGeneral);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{dataPrintNumber}")]
        public async Task<IActionResult> Delete(String dataPrintNumber)
        {
            var entry = await service.Delete(dataPrintNumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("CheckExist/{dataPrintNumber}/{goodsID}")]
        public async Task<IActionResult> checkExist(String dataPrintNumber, String goodsID,int ordinal)
        {
            var result = await service.checkExist(dataPrintNumber, goodsID,ordinal);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }
    }
}
