﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BranchController:ControllerBase
    {
        private readonly IBranchService branchService;

        public BranchController(IBranchService branchService)
        {
            this.branchService = branchService;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await branchService.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels =BranchHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{branchId}")]
        public async Task<IActionResult> GetUnderId(String branchId)
        {
            var entrys = await branchService.GetUnderId(branchId);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = BranchHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] Branch branch)
        {
            var entry = await branchService.Create(branch);

            return CreatedAtAction(
                 nameof(Get), new { id = branch.BranchID }, entry);
        }

        [HttpPut]
        [Route("Put/{branchId}")]
        public async Task<IActionResult> Update(String branchId, [FromBody] Branch branch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (branchId != branch.BranchID)
            {
                return BadRequest();
            }

            await branchService.Update(branchId,branch);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{branchId}")]
        public async Task<IActionResult> Delete(String branchId)
        {
            var entry = await branchService.Delete(branchId);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }
    }
}
