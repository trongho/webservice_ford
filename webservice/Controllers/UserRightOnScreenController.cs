﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserRightOnScreenController:ControllerBase
    {
        private readonly IUserRightOnScreenService userRightOnScreenService;

        public UserRightOnScreenController(IUserRightOnScreenService userRightOnScreenService)
        {
            this.userRightOnScreenService = userRightOnScreenService;
        }

        [HttpGet]
        [Route("GetByUserID/{userId}")]
        public async Task<IActionResult> GetUserRightUnderUserId(String userId)
        {
            var entrys = await userRightOnScreenService.GetRightByUserId(userId);
            if (entrys == null)
            {
                return NotFound();
            }

            var models = UserRightOnScreenHelper.Covert(entrys);

            return Ok(models);
        }

        [HttpGet]
        [Route("GetByScreenID/{screenId}")]
        public async Task<IActionResult> GetUserRightUnderScreenId(String screenId)
        {
            var entrys = await userRightOnScreenService.GetRightByScreenId(screenId);
            if (entrys == null)
            {
                return NotFound();
            }

            var models = UserRightOnScreenHelper.Covert(entrys);

            return Ok(models);
        }

        [HttpGet]
        [Route("GetByID/{userId}/{screenId}")]
        public async Task<IActionResult> GetUserRightUnderId(String userId, String screenId)
        {
            var entrys = await userRightOnScreenService.GetRightByUserScreentId(userId, screenId);
            if (entrys == null)
            {
                return NotFound();
            }

            var models= UserRightOnScreenHelper.Covert(entrys);

            return Ok(models);
        }

        [HttpPost]
        [Route("PostUserRightOnScreen")]
        public async Task<IActionResult> CreateUserRightOnScreen([FromBody] UserRightOnScreen userRightOnScreen)
        {
            var entrys = await userRightOnScreenService.CreateUserRightOnScreen(userRightOnScreen);

            return CreatedAtAction(
                 nameof(GetUserRightUnderId), new { userID = userRightOnScreen.UserID,screenId=userRightOnScreen.ScreenID }, userRightOnScreen);
        }

        [HttpPut]
        [Route("PutUserRightOnScreen/{userId}/{screenId}")]
        public async Task<IActionResult> UpdateUserRightOnScreen(String userId,String screenId, [FromBody] UserRightOnScreen userRightOnScreen)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (userId != userRightOnScreen.UserID&&screenId!=userRightOnScreen.ScreenID)
            {
                return BadRequest();
            }

            await userRightOnScreenService.UpdateUserRightOnScreen(userId,screenId,userRightOnScreen);

            return new NoContentResult();
        }


        [HttpDelete]
        [Route("DeleteUserRightOnScreen/{userId}/{screenId}")]
        public async Task<IActionResult> DeleteUserRightOnScreen(String userId,String screenId)
        {
            var entrys = await userRightOnScreenService.DeleteUserRightOnScreen(userId,screenId);
            if (entrys == null)
            {
                return NotFound();
            }

            return Ok(entrys);
        }

        [HttpGet]
        [Route("CheckExist/{userId}/{screenId}")]
        public async Task<IActionResult> checkExist(String userId,String screenId)
        {
            var result = await userRightOnScreenService.checkExist(userId,screenId);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }


    }
}
