﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WRDataHeaderController : ControllerBase
    {
        private readonly IWRDataHeaderService wRDataHeaderService;

        public WRDataHeaderController(IWRDataHeaderService wRDataHeaderService)
        {
            this.wRDataHeaderService = wRDataHeaderService;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await wRDataHeaderService.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = WRDataHeaderHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{wRDNumber}")]
        public async Task<IActionResult> GetWRDataHeaderUnderWRDNumber(String wRDNumber)
        {
            var entrys = await wRDataHeaderService.GetUnderId(wRDNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRDataHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] WRDataHeader wRDataHeader)
        {
            var entry = await wRDataHeaderService.Create(wRDataHeader);

            return CreatedAtAction(
                 nameof(Get), new { WRRNumber = wRDataHeader.WRDNumber }, entry);
        }

        [HttpPut]
        [Route("Put/{WRDNumber}")]
        public async Task<IActionResult> Update(String wRDNumber, [FromBody] WRDataHeader wRDataHeader)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (wRDNumber != wRDataHeader.WRDNumber)
            {
                return BadRequest();
            }

            await wRDataHeaderService.Update(wRDNumber, wRDataHeader);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{wRDNumber}")]
        public async Task<IActionResult> Delete(String wRDNumber)
        {
            var entry = await wRDataHeaderService.Delete(wRDNumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("CheckExist/{wRDNumber}/")]
        public async Task<IActionResult> checkExist(String wRDNumber)
        {
            var result = await wRDataHeaderService.checkExist(wRDNumber);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("lastWRDNumber")]
        public async Task<IActionResult> GetLastWRDNumber()
        {
            var WRDNumber = await wRDataHeaderService.GetLastId();
            if (WRDNumber == null)
            {
                return NotFound();
            }

            return Ok(WRDNumber);
        }

        [HttpGet]
        [Route("FilterByDate/{fromDate}/{toDate}")]
        public async Task<IActionResult> GetWRDataHeaderUnderDate(DateTime fromDate, DateTime toDate)
        {
            var entrys = await wRDataHeaderService.GetUnderDate(fromDate, toDate);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRDataHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByBranch/{branchID}")]
        public async Task<IActionResult> GetWRDataHeaderUnderBranchID(String branchID)
        {
            var entrys = await wRDataHeaderService.GetUnderBranchID(branchID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRDataHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByHandlingStatus/{handlingStatusID}")]
        public async Task<IActionResult> GetWRRHeaderUnderHandlingStatusID(String handlingStatusID)
        {
            var entrys = await wRDataHeaderService.GetUnderHandlingStatusID(handlingStatusID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRDataHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByOrderStatus/{orderStatusID}")]
        public async Task<IActionResult> GetWRDataHeaderUnderOrderStatusID(String orderStatusID)
        {
            var entrys = await wRDataHeaderService.GetUnderOrderStatusID(orderStatusID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRDataHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("CheckTotalQuantity/{wRDNumber}")]
        public async Task<IActionResult> CheckTotalQuantity(String id)
        {
            var entry = await wRDataHeaderService.checkTotalQuantity(id);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("Filter/{branchID}/{fromDate}/{toDate}/{handlingStatusID}")]
        public async Task<IActionResult> Filter(String branchID,DateTime fromDate,DateTime toDate, String handlingStatusID)
        {
            var entrys = await wRDataHeaderService.Filter(branchID,fromDate,toDate,handlingStatusID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRDataHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByDateAndHandlingStatus/{fromDate}/{toDate}/{handlingStatusID}")]
        public async Task<IActionResult> FilterByDateAndHandlingStatus(DateTime fromDate, DateTime toDate, String handlingStatusID)
        {
            var entrys = await wRDataHeaderService.FilterByDateAndHandlingStatus( fromDate, toDate, handlingStatusID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRDataHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByDateAndBranch/{fromDate}/{toDate}/{branchID}")]
        public async Task<IActionResult> FilterByDateAndBranch( DateTime fromDate, DateTime toDate, String branchID)
        {
            var entrys = await wRDataHeaderService.FilterByDateAndBranch(fromDate, toDate,branchID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRDataHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }


    }
}
