﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TSHeaderController : ControllerBase
    {
        private readonly ITSHeaderService service;

        public TSHeaderController(ITSHeaderService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("GetByID/{id}")]
        public async Task<IActionResult> GetUnderID(String id)
        {
            var entrys = await service.GetUnderId(id);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = TSHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] TSHeader entry)
        {
            var entrys = await service.Create(entry);

            return CreatedAtAction(
                 nameof(GetUnderID), new { TSNumber = entry.TSNumber }, entry);
        }

        [HttpPut]
        [Route("Put/{id}")]
        public async Task<IActionResult> Update(String id, [FromBody] TSHeader entry)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != entry.TSNumber)
            {
                return BadRequest();
            }

            await service.Update(id, entry);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IActionResult> Delete(String id)
        {
            var entry = await service.Delete(id);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("lastID")]
        public async Task<IActionResult> GetLastUserId()
        {
            var id = await service.GetLastId();
            if (id == null)
            {
                return NotFound();
            }

            return Ok(id);
        }

        [HttpGet]
        [Route("FilterByDate/{fromDate}/{toDate}")]
        public async Task<IActionResult> GetWRHeaderUnderDate(DateTime fromDate, DateTime toDate)
        {
            var entrys = await service.GetUnderDate(fromDate, toDate);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = TSHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByBranch/{branchID}")]
        public async Task<IActionResult> GetWRHeaderUnderBranchID(String branchID)
        {
            var entrys = await service.GetUnderBranchID(branchID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = TSHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByHandlingStatus/{handlingStatusID}")]
        public async Task<IActionResult> GetWRHeaderUnderHandlingStatusID(String handlingStatusID)
        {
            var entrys = await service.GetUnderHandlingStatusID(handlingStatusID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = TSHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

    }

}
