﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DataPrintHeaderController:ControllerBase
    {
        private readonly IDataPrintHeaderService service;
        public DataPrintHeaderController(IDataPrintHeaderService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = DataPrintHeaderHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{dataPrintNumber}")]
        public async Task<IActionResult> GetWRDataHeaderUnderdataPrintNumber(String dataPrintNumber)
        {
            var entrys = await service.GetUnderId(dataPrintNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = DataPrintHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] DataPrintHeader DataPrintHeader)
        {
            var entry = await service.Create(DataPrintHeader);

            return CreatedAtAction(
                 nameof(Get), new { WIRNumber = DataPrintHeader.DataPrintNumber }, entry);
        }

        [HttpPut]
        [Route("Put/{dataPrintNumber}")]
        public async Task<IActionResult> Update(String dataPrintNumber, [FromBody] DataPrintHeader DataPrintHeader)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (dataPrintNumber != DataPrintHeader.DataPrintNumber)
            {
                return BadRequest();
            }

            await service.Update(dataPrintNumber, DataPrintHeader);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{dataPrintNumber}")]
        public async Task<IActionResult> Delete(String dataPrintNumber)
        {
            var entry = await service.Delete(dataPrintNumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("CheckExist/{dataPrintNumber}/")]
        public async Task<IActionResult> checkExist(String dataPrintNumber)
        {
            var result = await service.checkExist(dataPrintNumber);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("lastDataPrintNumber")]
        public async Task<IActionResult> GetLastdataPrintNumber()
        {
            var dataPrintNumber = await service.GetLastId();
            if (dataPrintNumber == null)
            {
                return NotFound();
            }

            return Ok(dataPrintNumber);
        }
    }
}
