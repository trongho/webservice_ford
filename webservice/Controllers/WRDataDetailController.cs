﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WRDataDetailController : ControllerBase
    {
        private readonly IWRDataDetailService wRDataDetailService;

        public WRDataDetailController(IWRDataDetailService wRDataDetailService)
        {
            this.wRDataDetailService = wRDataDetailService;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await wRDataDetailService.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = WRDataDetailHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{WRDNumber}")]
        public async Task<IActionResult> GetWRDataDetailUnderWRDNumber(String wRDNumber)
        {
            var entrys = await wRDataDetailService.GetUnderId(wRDNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRDataDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByMultiID/{WRDNumber}/{goodsID}/{ordinal}")]
        public async Task<IActionResult> GetUnderMultiId(String wRDNumber, String goodsID, int ordinal)
        {
            var entrys = await wRDataDetailService.GetUnderId(wRDNumber, goodsID, ordinal);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRDataDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByIDCode/{WRDNumber}/{goodsID}/{*idCode}")]
        public async Task<IActionResult> GetUnderMultiIdCode(String wRDNumber, String goodsID, String IdCode)
        {
            var entrys = await wRDataDetailService.GetUnderIdCode(wRDNumber, goodsID, IdCode);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRDataDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] WRDataDetail wRDataDetail)
        {
            var entry = await wRDataDetailService.Create(wRDataDetail);

            return CreatedAtAction(
                 nameof(Get), new { WRDNumber = wRDataDetail.WRDNumber }, entry);
        }


        [HttpPut]
        [Route("Put/{WRDNumber}/{goodsID}/{ordinal}")]
        public async Task<IActionResult> Update(String wRDNumber, String goodsID, int ordinal, [FromBody] WRDataDetail wRDataDetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (wRDNumber != wRDataDetail.WRDNumber)
            {
                return BadRequest();
            }

            await wRDataDetailService.Update(wRDNumber, goodsID, ordinal, wRDataDetail);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{wRDNumber}")]
        public async Task<IActionResult> Delete(String wRDNumber)
        {
            var entry = await wRDataDetailService.Delete(wRDNumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }


        [HttpGet]
        [Route("CheckExist/{wRDNumber}/{goodsID}")]
        public async Task<IActionResult> checkExist(String wRDNumber,String goodsID)
        {
            var result = await wRDataDetailService.checkExist(wRDNumber,goodsID);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("lastWRDNumber")]
        public async Task<IActionResult> GetLastWRDNumber()
        {
            var WRDNumber = await wRDataDetailService.GetLastId();
            if (WRDNumber == null)
            {
                return NotFound();
            }

            return Ok(WRDNumber);
        }
    }
}
