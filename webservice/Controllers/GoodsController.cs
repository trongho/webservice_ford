﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class GoodsController:ControllerBase
    {
        private readonly IGoodsService goodsService;

        public GoodsController(IGoodsService goodsService)
        {
            this.goodsService = goodsService;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await goodsService.GetAllGoods();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = GoodsHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{id}")]
        public async Task<IActionResult> GetUnderID(String id)
        {
            var entrys = await goodsService.GetUnderId(id);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = GoodsHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] Goods goods)
        {
            var entry = await goodsService.Create(goods);

            return CreatedAtAction(
                 nameof(Get), new { GoodsID = goods.GoodsID }, entry);
        }

        [HttpPut]
        [Route("Put/{id}")]
        public async Task<IActionResult> Update(String id, [FromBody] Goods goods)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != goods.GoodsID)
            {
                return BadRequest();
            }

            await goodsService.Update(id, goods);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IActionResult> Delete(String id)
        {
            var entry = await goodsService.Delete(id);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("CheckExist/{id}/")]
        public async Task<IActionResult> checkExist(String id)
        {
            var result = await goodsService.checkExist(id);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

    }
}
