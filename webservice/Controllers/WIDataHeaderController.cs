﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WIDataHeaderController : ControllerBase
    {
        private readonly IWIDataHeaderService service;

        public WIDataHeaderController(IWIDataHeaderService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = WIDataHeaderHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{wIDNumber}")]
        public async Task<IActionResult> GetWRDataHeaderUnderWIDNumber(String wIDNumber)
        {
            var entrys = await service.GetUnderId(wIDNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] WIDataHeader wIDataHeader)
        {
            var entry = await service.Create(wIDataHeader);

            return CreatedAtAction(
                 nameof(Get), new { WIRNumber = wIDataHeader.WIDNumber }, entry);
        }

        [HttpPut]
        [Route("Put/{WIDNumber}")]
        public async Task<IActionResult> Update(String wIDNumber, [FromBody] WIDataHeader wIDataHeader)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (wIDNumber != wIDataHeader.WIDNumber)
            {
                return BadRequest();
            }

            await service.Update(wIDNumber, wIDataHeader);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{wIDNumber}")]
        public async Task<IActionResult> Delete(String wIDNumber)
        {
            var entry = await service.Delete(wIDNumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("CheckExist/{wIDNumber}/")]
        public async Task<IActionResult> checkExist(String wIDNumber)
        {
            var result = await service.checkExist(wIDNumber);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("lastWIDNumber")]
        public async Task<IActionResult> GetLastWIDNumber()
        {
            var WIDNumber = await service.GetLastId();
            if (WIDNumber == null)
            {
                return NotFound();
            }

            return Ok(WIDNumber);
        }

        [HttpGet]
        [Route("FilterByDate/{fromDate}/{toDate}")]
        public async Task<IActionResult> GetWIDataHeaderUnderDate(DateTime fromDate, DateTime toDate)
        {
            var entrys = await service.GetUnderDate(fromDate, toDate);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByBranch/{branchID}")]
        public async Task<IActionResult> GetWIDataHeaderUnderBranchID(String branchID)
        {
            var entrys = await service.GetUnderBranchID(branchID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByHandlingStatus/{handlingStatusID}")]
        public async Task<IActionResult> GetWIDataHeaderUnderHandlingStatusID(String handlingStatusID)
        {
            var entrys = await service.GetUnderHandlingStatusID(handlingStatusID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByOrderStatus/{orderStatusID}")]
        public async Task<IActionResult> GetWIDataHeaderUnderOrderStatusID(String orderStatusID)
        {
            var entrys = await service.GetUnderOrderStatusID(orderStatusID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("Filter/{branchID}/{fromDate}/{toDate}/{handlingStatusID}")]
        public async Task<IActionResult> Filter(String branchID, DateTime fromDate, DateTime toDate, String handlingStatusID)
        {
            var entrys = await service.Filter(branchID, fromDate, toDate, handlingStatusID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByDateAndHandlingStatus/{fromDate}/{toDate}/{handlingStatusID}")]
        public async Task<IActionResult> FilterByDateAndHandlingStatus(DateTime fromDate, DateTime toDate, String handlingStatusID)
        {
            var entrys = await service.FilterByDateAndHandlingStatus(fromDate, toDate, handlingStatusID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByDateAndBranch/{fromDate}/{toDate}/{branchID}")]
        public async Task<IActionResult> FilterByDateAndBranch(DateTime fromDate, DateTime toDate, String branchID)
        {
            var entrys = await service.FilterByDateAndBranch(fromDate, toDate, branchID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }
    }
}
