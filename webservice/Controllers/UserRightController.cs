﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserRightController:ControllerBase
    {
        private readonly IUserRightService userRightService;

        public UserRightController(IUserRightService userRightService)
        {
            this.userRightService = userRightService;
        }

        [HttpGet]
        [Route("Auth/GetByUserID/{userId}")]
        [Authorize(Policy = "OnlyNonBlockedUser")]
        public async Task<IActionResult> Get(String id)
        {
            var claimsIdentity = HttpContext.User.Identity as ClaimsIdentity;

            var claim = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier);

            if (claim == null)
            {
                return Unauthorized("Invalid user");
            }

            var userRights = await userRightService.GetUserRightByUserId(claim.Value);

            if (userRights == null || !userRights.Any())
            {
                return BadRequest($"No order was found");
            }

            return Ok(userRights);
        }

        [HttpGet]
        [Route("GetByUserID/{userId}")]
        public async Task<IActionResult> GetUserUnderUserId(String userId)
        {
            var userRights = await userRightService.GetUserRightByUserId(userId);
            if (userRights == null)
            {
                return NotFound();
            }

            var userRightModels = UserRightHelper.CovertUserRights(userRights);

            return Ok(userRightModels);
        }


        [HttpGet]
        [Route("GetByRightID/{rightId}")]
        public async Task<IActionResult> GetUserUnderRightId(String rightId)
        {
            var userRights = await userRightService.GetUserRightByRightId(rightId);
            if (userRights == null)
            {
                return NotFound();
            }

            var userRightModels = UserRightHelper.CovertUserRights(userRights);

            return Ok(userRightModels);
        }

        [HttpGet]
        [Route("GetByUserRightID/{userId}/{rightId}")]
        public async Task<IActionResult> GetUserUnderId(String userId,String rightId)
        {
            var userRights = await userRightService.GetUserRightByUserRightId(userId,rightId);
            if (userRights == null)
            {
                return NotFound();
            }

            var userRightModels = UserRightHelper.CovertUserRights(userRights);

            return Ok(userRightModels);
        }

        [HttpPost]
        [Route("PostUserRight")]
        public async Task<IActionResult> CreateUserRight([FromBody] UserRight userRight)
        {
            var users = await userRightService.CreateUserRight(userRight);

            return CreatedAtAction(
                 nameof(Get), new { userID = userRight.UserID,RighID=userRight.RightID }, userRight);
        }

        [HttpPut]
        [Route("PutUserRight/{userId}/{rightId}")]
        public async Task<IActionResult> UpdateUserRight(String userId, String rightId, [FromBody] UserRight userRight)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (userId != userRight.UserID && rightId != userRight.RightID)
            {
                return BadRequest();
            }

            await userRightService.UpdateUserRight(userId,rightId,userRight);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("DeleteUserRight/{userId}/{rightId}")]
        public async Task<IActionResult> DeleteUserRight(String userId, String rightId)
        {
            var entrys = await userRightService.DeleteUserRight(userId,rightId);
            if (entrys == null)
            {
                return NotFound();
            }

            return Ok(entrys);
        }
    }
}
