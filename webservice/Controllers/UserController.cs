﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;
using webservice.Requests;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserService userService;

        public UserController(IUserService userService)
        {
            this.userService = userService
;
        }
        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var users = await userService.GetAllUser();
            if (users == null)
            {
                return NotFound();
            }

            var userModels = UserHelper.CovertUsers(users);

            return Ok(userModels);
        }

        [HttpGet]
        [Route("GetByID/{userId}")]
        public async Task<IActionResult> GetUserUnderId(String userId)
        {
            var users = await userService.GetUserUnderId(userId);
            if (users == null)
            {
                return NotFound();
            }

            var userModels = UserHelper.CovertUsers(users);

            return Ok(userModels);
        }

        [HttpGet]
        [Route("Username/{username}")]
        public async Task<IActionResult> GetUserUnderUsername(String username)
        {
            var users = await userService.GetUserUnderUsername(username);
            if (users == null)
            {
                return NotFound();
            }

            var userModels = UserHelper.CovertUsers(users);

            return Ok(userModels);
        }

        [HttpPost]
        [Route("PostUser")]
        public async Task<IActionResult> CreateUser([FromBody] User user)
        {
            var users = await userService.CreateUser(user);

            return CreatedAtAction(
                 nameof(Get), new { id = user.UserID}, user);
        }

        [HttpPut]
        [Route("PutUser/{userId}")]
        public async Task<IActionResult> UpdateUser(String userId, [FromBody] User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (userId != user.UserID)
            {
                return BadRequest();
            }

            await userService.UpdateUser(userId,user);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("DeleteUser/{userId}")]
        public async Task<IActionResult> DeleteUser(String userId)
        {
            var users = await userService.DeleteUser(userId);
            if (users == null)
            {
                return NotFound();
            }

            return Ok(users);
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] LoginRequest loginRequest)
        {
            if (loginRequest == null || string.IsNullOrEmpty(loginRequest.Username) || string.IsNullOrEmpty(loginRequest.Password))
            {
                return BadRequest("Missing login details");
            }

            var loginResponse = await userService.Login(loginRequest);

            if (loginResponse == null)
            {
                return BadRequest($"Invalid credentials");
            }

            return Ok(loginResponse);
        }

        [HttpGet]
        [Route("lastUserId")]
        public async Task<IActionResult> GetLastUserId()
        {
            var userID = await userService.GetLastUserId();
            if (userID == null)
            {
                return NotFound();
            }

            return Ok(userID);
        }

        [HttpGet]
        [Route("CheckExist/{userId}")]
        public async Task<IActionResult> checkExist(String userId)
        {
            var result = await userService.checkExist(userId);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }
    }
}
