﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class InventoryController : ControllerBase
    {
        private readonly IInventoryService service;

        public InventoryController(IInventoryService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = InventoryHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByMultiID/{Year}/{Month}/{WareHouseID}")]
        public async Task<IActionResult> GetUnderMultiId(Int16 year, Int16 month,String warehouseID)
        {
            var entrys = await service.GetUnderId(year,month,warehouseID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = InventoryHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByMultiGoodsID/{Year}/{Month}/{WareHouseID}/{GoodsID}")]
        public async Task<IActionResult> GetUnderMultiGoodsID(Int16 year, Int16 month, String warehouseID,String goodsID)
        {
            var entrys = await service.GetUnderId(year, month, warehouseID,goodsID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = InventoryHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] Inventory inventory)
        {
            var entry = await service.Create(inventory);

            return CreatedAtAction(
                 nameof(Get), new {Year= inventory.Year,Month=inventory.Month,WarehouseID=inventory.WarehouseID,GoodsId=inventory.GoodsID }, entry);
        }


        [HttpPut]
        [Route("Put/{Year}/{Month}/{WareHouseID}/{GoodsID}")]
        public async Task<IActionResult> Update(Int16 year, Int16 month, String warehouseID, String goodsID, [FromBody] Inventory inventory)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (year !=inventory.Year&&month!=inventory.Month&&warehouseID!=inventory.WarehouseID&&goodsID!=inventory.GoodsID)
            {
                return BadRequest();
            }

            await service.Update(year, month, warehouseID,goodsID, inventory);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{Year}/{Month}/{WareHouseID}")]
        public async Task<IActionResult> Delete(Int16 year, Int16 month, String warehouseID)
        {
            var entry = await service.Delete(year,month,warehouseID);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpDelete]
        [Route("Delete2/{Year}/{Month}/{WareHouseID}/{goodsID}")]
        public async Task<IActionResult> Delete2(Int16 year, Int16 month, String warehouseID,String goodsID)
        {
            var entry = await service.Delete(year, month, warehouseID,goodsID);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("CheckExist/{Year}/{Month}/{WareHouseID}")]
        public async Task<IActionResult> checkExist(Int16 year, Int16 month, String warehouseID)
        {
            var result = await service.checkExist(year, month, warehouseID);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("CheckExistWithGoods/{Year}/{Month}/{WareHouseID}/{goodsID}")]
        public async Task<IActionResult> checkExist(Int16 year, Int16 month, String warehouseID,String goodsID)
        {
            var result = await service.checkExist(year, month, warehouseID,goodsID);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }
    }
}
