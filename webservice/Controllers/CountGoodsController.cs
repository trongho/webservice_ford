﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CountGoodsController : ControllerBase
    {
        private readonly ICountGoodsService service;

        public CountGoodsController(ICountGoodsService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = CountGoodsHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{locationID}")]
        public async Task<IActionResult> GetUnderID(String locationID)
        {
            var entrys = await service.GetUnderId(locationID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = CountGoodsHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByMultiID/{locationID}/{goodsID}")]
        public async Task<IActionResult> GetUnderMultiId(String locationID, String goodsID)
        {
            var entrys = await service.GetUnderId(locationID, goodsID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = CountGoodsHelper.Covert(entrys);

            return Ok(entryModels);
        }


        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] CountGoods CountGoods)
        {
            var entry = await service.Create(CountGoods);

            return CreatedAtAction(
                 nameof(Get), new { LocationID = CountGoods.LocationID,GoodsID = CountGoods.GoodsID }, entry) ;
        }


        [HttpPut]
        [Route("Put/{locationID}/{goodsID}")]
        public async Task<IActionResult> Update(String locationID, String goodsID,[FromBody] CountGoods CountGoods)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (locationID != CountGoods.LocationID&&goodsID!=CountGoods.GoodsID)
            {
                return BadRequest();
            }

            await service.Update(locationID, goodsID,CountGoods);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("DeleteByID/{locationID}")]
        public async Task<IActionResult> Delete(String locationID)
        {
            var entry = await service.Delete(locationID);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpDelete]
        [Route("DeleteByMultiID/{locationID}/{goodsID}")]
        public async Task<IActionResult> Delete(String locationID,String goodsID)
        {
            var entry = await service.Delete(locationID,goodsID);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("CheckExist/{locationID}/{goodsID}")]
        public async Task<IActionResult> checkExist(String locationID, String goodsID)
        {
            var result = await service.checkExist(locationID, goodsID);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

    }
}
