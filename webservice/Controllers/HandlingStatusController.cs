﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class HandlingStatusController : ControllerBase
    {
        private readonly IHandlingStatusService handlingStatusService;

        public HandlingStatusController(IHandlingStatusService handlingStatusService)
        {
            this.handlingStatusService = handlingStatusService;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await handlingStatusService.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = HandlingStatusHelper.Covert(entrys);
            return Ok(entryModels);
        }
    }
}
