﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Entities
{
    public class WarehouseDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Right> Rights { get; set; }
        public DbSet<UserRight> UserRights { get; set; }
        public DbSet<GoodsLine> GoodsLines { get; set; }
        public DbSet<Screen> Screens { get; set; }
        public DbSet<UserRightOnScreen> UserRightOnScreens { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<UserRightOnReport> UserRightOnReports { get; set; }
        public DbSet<Branch> Branchs { get; set; }
        public DbSet<Goods> Goods { get; set; }
        public DbSet<WRRHeader> WRRHeaders { get; set; }
        public DbSet<WRRDetail> WRRDetails { get; set; }
        public DbSet<WRHeader> WRHeaders { get; set; }
        public DbSet<WRDetail> WRDetails { get; set; }
        public DbSet<WRDataHeader> WRDataHeaders { get; set; }
        public DbSet<WRDataDetail> WRDataDetails { get; set; }
        public DbSet<WRDataGeneral> WRDataGenerals { get; set; }
        public DbSet<WIRHeader> WIRHeaders { get; set; }
        public DbSet<WIRDetail> WIRDetails { get; set; }
        public DbSet<IOHeader> IOHeaders { get; set; }
        public DbSet<IODetail> IODetails { get; set; }
        public DbSet<WIDataHeader> WIDataHeaders { get; set; }
        public DbSet<WIDataDetail> WIDataDetails { get; set; }
        public DbSet<WIDataGeneral> WIDataGenerals { get; set; }
        public DbSet<HandlingStatus> HandlingStatuss { get; set; }
        public DbSet<OrderStatus> OrderStatuss { get; set; }
        public DbSet<Modality> Modalitys { get; set; }
        public DbSet<Inventory>Inventorys { get; set; }
        public DbSet<InventoryOfLastMonths> InventoryOfLastMonthss { get; set; }
        public DbSet<Warehouse> Warehouses { get; set; }
        public DbSet<Color> Colors { get; set; }
        public DbSet<Unit> Units { get; set; }
        public DbSet<TallyData> TallyDatas { get; set; }
        public DbSet<TSHeader> TSHeaders { get; set; }
        public DbSet<TSDetail> TSDetails { get; set; }
        public DbSet<TSDataGeneral> TSDataGenerals { get; set; }
        public DbSet<GGDataHeader> GGDataHeaders { get; set; }
        public DbSet<GGDataDetail> GGDataDetails { get; set; }
        public DbSet<GGDataGeneral> GGDataGenerals { get; set; }
        public DbSet<CGDataHeader> CGDataHeaders { get; set; }
        public DbSet<CGDataGeneral> CGDataGenerals { get; set; }
        public DbSet<GoodsData> GoodsDatas { get; set; }
        public DbSet<DataPrintHeader> DataPrintHeaders { get; set; }
        public DbSet<DataPrintGeneral> DataPrintGenerals { get; set; }
        public DbSet<DataPrintDetail> DataPrintDetails { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<InventoryData> InventoryDatas { get; set; }
        public DbSet<CountGoods> CountGoodss { get; set; }
        public WarehouseDbContext(DbContextOptions<WarehouseDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<Right>().ToTable("Right");
            modelBuilder.Entity<UserRight>().ToTable("UserRight").HasKey(e=>new { e.UserID,e.RightID });
            modelBuilder.Entity<GoodsLine>().ToTable("GoodsLine").HasKey(e => e.GoodsLineID);
            modelBuilder.Entity<Screen>().ToTable("Screen").HasKey(e => e.ScreenID);
            modelBuilder.Entity<UserRightOnScreen>().ToTable("UserRightOnScreen").HasKey(e => new { e.UserID, e.ScreenID });
            modelBuilder.Entity<Report>().ToTable("Report").HasKey(e => e.ReportID);
            modelBuilder.Entity<UserRightOnReport>().ToTable("UserRightOnReport").HasKey(e => new { e.UserID, e.ReportID });
            modelBuilder.Entity<Branch>().ToTable("Branch").HasKey(e => e.BranchID);

            modelBuilder.Entity<Goods>().ToTable("Goods").HasKey(e => e.GoodsID);
            modelBuilder.Entity<Goods>().Property(p =>p.PurchaseUnitRate).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Goods>().Property(p => p.RetailUnitRate).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Goods>().Property(p => p.WholesaleUnitRate).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Goods>().Property(p => p.Length).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Goods>().Property(p => p.Width).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Goods>().Property(p => p.Height).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Goods>().Property(p => p.Weight).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Goods>().Property(p => p.Diameter).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Goods>().Property(p => p.Gauge).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Goods>().Property(p => p.Volume).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Goods>().Property(p => p.Density).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Goods>().Property(p => p.VATInput).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Goods>().Property(p => p.VATOutput).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Goods>().Property(p => p.ImportTax).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Goods>().Property(p => p.SpecialTax).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Goods>().Property(p => p.TransportRate).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Goods>().Property(p => p.OtherRate).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Goods>().Property(p => p.DiscountInput).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Goods>().Property(p => p.DiscountOutput).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Goods>().Property(p => p.CostPrice).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Goods>().Property(p => p.RetailSalePrice).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Goods>().Property(p => p.InternalSalePrice).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Goods>().Property(p => p.Margin).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Goods>().Property(p => p.DiscountInternal).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Goods>().Property(p => p.MinQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Goods>().Property(p => p.MaxQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Goods>().Property(p => p.WholesalePrice).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<WRRHeader>().ToTable("WRRHeader").HasKey(e => e.WRRNumber);
            modelBuilder.Entity<WRRHeader>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<WRRDetail>().ToTable("WRRDetail").HasKey(e => new { e.WRRNumber, e.GoodsID,e.Ordinal});
            modelBuilder.Entity<WRRDetail>().Property(p => p.PackingVolume).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRRDetail>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRRDetail>().Property(p => p.QuantityByItem).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRRDetail>().Property(p => p.QuantityByPack).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRRDetail>().Property(p => p.QuantityReceived).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<WRHeader>().ToTable("WRHeader").HasKey(e => e.WRNumber);
            modelBuilder.Entity<WRHeader>().Property(p => p.TotalDiscountAmount).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRHeader>().Property(p => p.TotalAmount).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRHeader>().Property(p => p.TotalAmountExcludedVAT).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRHeader>().Property(p => p.TotalAmountIncludedVAT).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRHeader>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRHeader>().Property(p => p.TotalVATAmount).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<WRDetail>().ToTable("WRDetail").HasKey(e => new { e.WRNumber, e.GoodsID,e.Ordinal});
            modelBuilder.Entity<WRDetail>().Property(p => p.UnitRate).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.Quantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.PriceIncludedVAT).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.PriceExcludedVAT).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.Price).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.Discount).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.DiscountAmount).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.VAT).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.VATAmount).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.AmountExcludedVAT).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.AmountIncludedVAT).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.Amount).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.QuantityReceived).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.PriceIncludedVAT).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.PriceExcludedVAT).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.PriceReceived).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.DiscountReceived).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.DiscountAmountReceived).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.VATReceived).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.VATAmountReceived).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.AmountExcludedVATReceived).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.AmountIncludedVATReceived).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.AmountReceived).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.QuantityOrdered).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.OrderQuantityReceived).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.ReceiptQuantityIssued).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.ReceiptQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.ReceiptAmount).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.AverageCost).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.PriceExcludedVATReceived).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.PriceIncludedVATReceived).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<WRDataHeader>().ToTable("WRDataHeader").HasKey(e => e.WRDNumber);
            modelBuilder.Entity<WRDataHeader>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataHeader>().Property(p => p.TotalQuantityOrg).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<WRDataDetail>().ToTable("WRDataDetail").HasKey(e => new { e.WRDNumber, e.GoodsID,e.Ordinal});
            modelBuilder.Entity<WRDataDetail>().Property(p => p.Quantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataDetail>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataDetail>().Property(p => p.TotalGoods).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataDetail>().Property(p => p.QuantityOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataDetail>().Property(p => p.TotalQuantityOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataDetail>().Property(p => p.TotalGoodsOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataDetail>().Property(p => p.PackingVolume).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataDetail>().Property(p => p.QuantityByPack).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataDetail>().Property(p => p.QuantityByItem).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataDetail>().Property(p => p.PackingQuantity).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<WRDataGeneral>().ToTable("WRDataGeneral").HasKey(e => new { e.WRDNumber, e.GoodsID, e.Ordinal });
            modelBuilder.Entity<WRDataGeneral>().Property(p => p.Quantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataGeneral>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataGeneral>().Property(p => p.TotalGoods).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataGeneral>().Property(p => p.QuantityOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataGeneral>().Property(p => p.TotalQuantityOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataGeneral>().Property(p => p.TotalGoodsOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataGeneral>().Property(p => p.PackingVolume).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataGeneral>().Property(p => p.QuantityByPack).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataGeneral>().Property(p => p.QuantityByItem).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataGeneral>().Property(p => p.PackingQuantity).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<HandlingStatus>().ToTable("HandlingStatus").HasKey(e => e.HandlingStatusID);

            modelBuilder.Entity<Warehouse>().ToTable("Warehouse").HasKey(e => e.WarehouseID);


            modelBuilder.Entity<OrderStatus>().ToTable("OrderStatus").HasKey(e => e.OrderStatusID);

            modelBuilder.Entity<Modality>().ToTable("Modality").HasKey(e => e.ModalityID);

            modelBuilder.Entity<WIRHeader>().ToTable("WIRHeader").HasKey(e => e.WIRNumber);
            modelBuilder.Entity<WIRHeader>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<WIRDetail>().ToTable("WIRDetail").HasKey(e => new { e.WIRNumber, e.GoodsID, e.Ordinal,e.GoodsGroupID });
            modelBuilder.Entity<WIRDetail>().Property(p => p.PackingVolume).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIRDetail>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIRDetail>().Property(p => p.QuantityByItem).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIRDetail>().Property(p => p.QuantityByPack).HasColumnType("decimal(18,3)");


            modelBuilder.Entity<IOHeader>().ToTable("IOHeader").HasKey(e => e.IONumber);
            modelBuilder.Entity<IOHeader>().Property(p => p.TotalDiscountAmount).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IOHeader>().Property(p => p.TotalAmount).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IOHeader>().Property(p => p.TotalAmountExcludedVAT).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IOHeader>().Property(p => p.TotalAmountIncludedVAT).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IOHeader>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IOHeader>().Property(p => p.TotalVATAmount).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<IODetail>().ToTable("IODetail").HasKey(e => new { e.IONumber, e.GoodsID, e.Ordinal });
            modelBuilder.Entity<IODetail>().Property(p => p.UnitRate).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.Quantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.PriceIncludedVAT).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.PriceExcludedVAT).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.Price).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.Discount).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.DiscountAmount).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.VAT).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.VATAmount).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.AmountExcludedVAT).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.AmountIncludedVAT).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.Amount).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.QuantityIssued).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.PriceIncludedVAT).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.PriceExcludedVAT).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.PriceIssued).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.DiscountIssued).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.DiscountAmountIssued).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.VATIssued).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.VATAmountIssued).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.AmountExcludedVATIssued).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.AmountIncludedVATIssued).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.AmountIssued).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.QuantityOrdered).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.OrderQuantityIssued).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.IssueQuantityReceived).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.IssueQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.IssueAmount).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.AverageCost).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.PriceExcludedVATIssued).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.PriceIncludedVATIssued).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<WIDataHeader>().ToTable("WIDataHeader").HasKey(e => e.WIDNumber);
            modelBuilder.Entity<WIDataHeader>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIDataHeader>().Property(p => p.TotalQuantityOrg).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<WIDataDetail>().ToTable("WIDataDetail").HasKey(e => new { e.WIDNumber, e.GoodsID, e.Ordinal });
            modelBuilder.Entity<WIDataDetail>().Property(p => p.Quantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIDataDetail>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIDataDetail>().Property(p => p.TotalGoods).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIDataDetail>().Property(p => p.QuantityOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIDataDetail>().Property(p => p.TotalQuantityOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIDataDetail>().Property(p => p.TotalGoodsOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIDataDetail>().Property(p => p.PackingVolume).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIDataDetail>().Property(p => p.QuantityByPack).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIDataDetail>().Property(p => p.QuantityByItem).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIDataDetail>().Property(p => p.PackingQuantity).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<WIDataGeneral>().ToTable("WIDataGeneral").HasKey(e => new { e.WIDNumber, e.GoodsID, e.Ordinal ,e.GoodsGroupID});
            modelBuilder.Entity<WIDataGeneral>().Property(p => p.Quantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIDataGeneral>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIDataGeneral>().Property(p => p.TotalGoods).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIDataGeneral>().Property(p => p.QuantityOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIDataGeneral>().Property(p => p.TotalQuantityOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIDataGeneral>().Property(p => p.TotalGoodsOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIDataGeneral>().Property(p => p.PackingVolume).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIDataGeneral>().Property(p => p.QuantityByPack).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIDataGeneral>().Property(p => p.QuantityByItem).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIDataGeneral>().Property(p => p.PackingQuantity).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<Inventory>().ToTable("Inventory").HasKey(e => new { e.Year, e.Month, e.WarehouseID, e.GoodsID });

            modelBuilder.Entity<InventoryOfLastMonths>().ToTable("InventoryOfLastMonths").HasKey(e => new { e.Year, e.Month, e.WarehouseID, e.GoodsID });

            modelBuilder.Entity<Color>().ToTable("Color").HasKey(e => e.ColorID);

            modelBuilder.Entity<Unit>().ToTable("Unit").HasKey(e => e.UnitID);

            modelBuilder.Entity<TallyData>().ToTable("TallyData").HasKey(e => new { e.TSNumber, e.GoodsID, e.No });
            modelBuilder.Entity<TallyData>().Property(p => p.Quantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<TallyData>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<TallyData>().Property(p => p.TotalRow).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<TSHeader>().ToTable("TSHeader").HasKey(e => e.TSNumber);
            modelBuilder.Entity<TSHeader>().Property(p => p.TotalDiscountAmount).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<TSHeader>().Property(p => p.TotalVATAmount).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<TSHeader>().Property(p => p.TotalAmountExcludedVAT).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<TSHeader>().Property(p => p.TotalAmountIncludedVAT).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<TSHeader>().Property(p => p.TotalAmount).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<TSHeader>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<TSDetail>().ToTable("TSDetail").HasKey(e => new { e.TSNumber, e.Ordinal, e.GoodsID });
            modelBuilder.Entity<TSDetail>().Property(p => p.UnitRate).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<TSDetail>().Property(p => p.Quantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<TSDetail>().Property(p => p.PriceExcludedVAT).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<TSDetail>().Property(p => p.PriceIncludedVAT).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<TSDetail>().Property(p => p.Price).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<TSDetail>().Property(p => p.Discount).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<TSDetail>().Property(p => p.DiscountAmount).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<TSDetail>().Property(p => p.VAT).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<TSDetail>().Property(p => p.VATAmount).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<TSDetail>().Property(p => p.AmountExcludedVAT).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<TSDetail>().Property(p => p.AmountIncludedVAT).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<TSDetail>().Property(p => p.Amount).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<TSDataGeneral>().ToTable("TSDataGeneral").HasKey(e => new { e.WarehouseID, e.TallyDate, e.Ordinal,e.GoodsID });
            modelBuilder.Entity<TSDataGeneral>().Property(p => p.TallyPrice).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<TSDataGeneral>().Property(p => p.TallyQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<TSDataGeneral>().Property(p => p.TallyAmount).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<TSDataGeneral>().Property(p => p.LedgerQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<TSDataGeneral>().Property(p => p.LedgerAmount).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<TSDataGeneral>().Property(p => p.DifferenceQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<TSDataGeneral>().Property(p => p.DifferenceAmount).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<GGDataHeader>().ToTable("GGDataHeader").HasKey(e => e.GGDNumber);
            modelBuilder.Entity<GGDataHeader>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataHeader>().Property(p => p.TotalQuantityOrg).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<GGDataDetail>().ToTable("GGDataDetail").HasKey(e => new { e.GGDNumber, e.GoodsID, e.Ordinal });
            modelBuilder.Entity<GGDataDetail>().Property(p => p.Quantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataDetail>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataDetail>().Property(p => p.TotalGoods).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataDetail>().Property(p => p.QuantityOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataDetail>().Property(p => p.TotalQuantityOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataDetail>().Property(p => p.TotalGoodsOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataDetail>().Property(p => p.PackingVolume).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataDetail>().Property(p => p.QuantityByPack).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataDetail>().Property(p => p.QuantityByItem).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<GGDataGeneral>().ToTable("GGDataGeneral").HasKey(e => new { e.GGDNumber, e.GoodsID, e.Ordinal });
            modelBuilder.Entity<GGDataGeneral>().Property(p => p.Quantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataGeneral>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataGeneral>().Property(p => p.TotalGoods).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataGeneral>().Property(p => p.QuantityOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataGeneral>().Property(p => p.TotalQuantityOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataGeneral>().Property(p => p.TotalGoodsOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataGeneral>().Property(p => p.PackingVolume).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataGeneral>().Property(p => p.QuantityByPack).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataGeneral>().Property(p => p.QuantityByItem).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<CGDataHeader>().ToTable("CGDataHeader").HasKey(e => e.CGDNumber);
            modelBuilder.Entity<CGDataHeader>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<CGDataHeader>().Property(p => p.TotalQuantityOrg).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<CGDataGeneral>().ToTable("CGDataGeneral").HasKey(e => new { e.CGDNumber, e.GoodsID, e.Ordinal });
            modelBuilder.Entity<CGDataGeneral>().Property(p => p.Quantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<CGDataGeneral>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<CGDataGeneral>().Property(p => p.TotalGoods).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<CGDataGeneral>().Property(p => p.QuantityOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<CGDataGeneral>().Property(p => p.TotalQuantityOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<CGDataGeneral>().Property(p => p.TotalGoodsOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<CGDataGeneral>().Property(p => p.PackingVolume).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<CGDataGeneral>().Property(p => p.QuantityByPack).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<CGDataGeneral>().Property(p => p.QuantityByItem).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<CGDataGeneral>().Property(p => p.PackingQuantity).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<GoodsData>().ToTable("GoodsData").HasKey(e => e.GoodsID);

            modelBuilder.Entity<DataPrintHeader>().ToTable("DataPrintHeader").HasKey(e => e.DataPrintNumber);
            modelBuilder.Entity<DataPrintHeader>().Property(p => p.TotalGoodsID).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<DataPrintHeader>().Property(p => p.TotalGoodsIDPrinted).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<DataPrintHeader>().Property(p => p.TotalLabel).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<DataPrintHeader>().Property(p => p.TotalLabelPrinted).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<DataPrintGeneral>().ToTable("DataPrintGeneral").HasKey(e => new { e.DataPrintNumber, e.GoodsID, e.Ordinal });
            modelBuilder.Entity<DataPrintGeneral>().Property(p => p.Quantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<DataPrintGeneral>().Property(p => p.PackingQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<DataPrintGeneral>().Property(p => p.PackingVolume).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<DataPrintDetail>().ToTable("DataPrintDetail").HasKey(e => new { e.DataPrintNumber, e.GoodsID, e.Ordinal });
            modelBuilder.Entity<DataPrintDetail>().Property(p => p.Quantity).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<Location>().ToTable("Location").HasKey(e => e.LocationID);

            modelBuilder.Entity<InventoryData>().ToTable("InventoryData").HasKey(e => e.GoodsID);
            modelBuilder.Entity<InventoryData>().Property(p => p.Quantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<InventoryData>().Property(p => p.Amount).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<CountGoods>().ToTable("CountGoods").HasKey(e => new { e.LocationID, e.GoodsID});
            modelBuilder.Entity<CountGoods>().Property(p => p.Quantity).HasColumnType("decimal(18,3)");
            



        }

    }
}
