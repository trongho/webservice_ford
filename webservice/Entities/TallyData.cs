﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Entities
{
    public class TallyData
    {
      public String TSNumber{get;set;}
      public String? TSDate{get;set;}
      public String GoodsID{get;set;}
      public String? GoodsName{get;set;}
      public Decimal? Quantity{get;set;}
      public Decimal? TotalQuantity{get;set;}
      public Decimal? TotalRow{get;set;}
      public String? CreatorID{get;set;}
      public String? CreatedDateTime{get;set;}
      public String? EditerID{get;set;}
      public String? EditedDateTime{get;set;}
      public String? Status{get;set;}
      public int No{get;set;}
    }
}
