﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Entities
{
    public class InventoryOfLastMonths
    {
        public Int16 Year { get; set; }
        public Int16 Month { get; set; }
        public String WarehouseID { get; set; }
        public String GoodsID { get; set; }
        public Decimal? OpeningStockQuantity { get; set; }
        public Decimal? OpeningStockAmount { get; set; }
        public Decimal? ReceiptQuantity { get; set; }
        public Decimal? ReceiptAmount { get; set; }
        public Decimal? IssueQuantity { get; set; }
        public Decimal? IssueAmount { get; set; }
        public Decimal? ClosingStockQuantity { get; set; }
        public Decimal? ClosingStockAmount { get; set; }
        public Decimal? AverageCost { get; set; }
        public String? Status { get; set; }
    }
}
