﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Entities
{
    public class Screen
    {
        public String ScreenID { get; set; }
        public String? ScreenName { get; set; }
        public String? ScreenNameEN { get; set; }
        public Int16? Ordinal { get; set; }
    }
}
