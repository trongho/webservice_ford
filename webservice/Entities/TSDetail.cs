﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Entities
{
    public class TSDetail
    {
         public String TSNumber {get;set;}
         public int Ordinal {get;set;}
         public String GoodsID {get;set;}
         public String? GoodsName {get;set;}
         public String? TallyUnitID {get;set;}
         public Decimal? UnitRate {get;set;}
         public String? StockUnitID {get;set;}
         public Decimal? Quantity {get;set;}
         public Decimal? PriceIncludedVAT {get;set;}
         public Decimal? PriceExcludedVAT {get;set;}
         public Decimal? Price {get;set;}
         public Decimal? Discount {get;set;}
         public Decimal? DiscountAmount {get;set;}
         public Decimal? VAT {get;set;}
         public Decimal? VATAmount {get;set;}
         public Decimal? AmountExcludedVAT {get;set;}
         public Decimal? AmountIncludedVAT {get;set;}
         public Decimal? Amount {get;set;}
         public DateTime? ExpiryDate {get;set;}
         public String? SerialNumber {get;set;}
         public String? Note {get;set;}
         public String? Status {get;set;}
    }
}
