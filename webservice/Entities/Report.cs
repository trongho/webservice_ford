﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Entities
{
    public class Report
    {
        public String ReportID { get; set; }
        public String? ReportName { get; set; }
        public String? ReportNameEN { get; set; }
        public Int16? Ordinal { get; set; }
    }
}
