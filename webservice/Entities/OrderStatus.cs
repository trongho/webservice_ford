﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Entities
{
    public class OrderStatus
    {
      public String OrderStatusID {get;set;}
      public String? OrderStatusName {get;set;}
      public String? OrderStatusNameEN {get;set;}
    }
}
