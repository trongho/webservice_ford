﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Entities
{
    public class Goods
    {
      public String GoodsID{get;set;}
      public String? GoodsName{get;set;}
      public String? OtherGoodsName{get;set;}
      public String? StockUnitID{get;set;}
      public String? GoodsTypeID{get;set;}
      public String? GoodsLineID{get;set;}
      public String? GoodsGroupID{get;set;}
      public String? GoodsCategoryID{get;set;}
      public String? InternalGoodsID{get;set;}
      public String? GeneralGoodsID{get;set;}
      public String? Model{get;set;}
      public String? SerialNumber{get;set;}
      public  DateTime? ProductionDate{get;set;}
      public DateTime? ExpiryDate {get;set;}
      public DateTime? WarrantyEndDate {get;set;}
      public Int16? ScanOption{get;set;}
      public Int16? PLUType{get;set;}
      public String? Packing{get;set;}
      public String? Description{get;set;}
      public String? GoodsStatus{get;set;}
      public String? SupplierID{get;set;}
      public String? ManufacturerID{get;set;}
      public String? PurchaseUnitID{get;set;}
      public Decimal? PurchaseUnitRate{get;set;}
      public String? RetailUnitID{get;set;}
      public Decimal? RetailUnitRate{get;set;}
      public String? WholesaleUnitID{get;set;}
      public Decimal? WholesaleUnitRate {get;set;}
      public String? StyleID{get;set;}
      public String? SizeID{get;set;}
      public String? ColorID{get;set;}
      public String? SeasonID{get;set;}
      public String? MaterialID{get;set;}
      public Decimal? Length{get;set;}
      public String? LengthUnit{get;set;}
      public Decimal? Width{get;set;}
      public String? WidthUnit{get;set;}
      public Decimal? Height{get;set;}
      public String? HeightUnit{get;set;}
      public Decimal? Weight{get;set;}
      public String? WeightUnit{get;set;}
      public Decimal? Diameter{get;set;}
      public String? DiameterUnit{get;set;}
      public Decimal? Gauge{get;set;}
      public String? GaugeUnit{get;set;}
      public Decimal? Volume{get;set;}
      public String? VolumeUnit{get;set;}
      public Decimal? Density{get;set;}
      public Decimal? VATInput{get;set;}
      public Decimal? VATOutput{get;set;}
      public Decimal? ImportTax {get;set;}
      public Decimal? SpecialTax{get;set;}
      public Decimal? TransportRate{get;set;}
      public Decimal? OtherRate{get;set;}
      public Decimal? DiscountInput {get;set;}
      public Decimal? DiscountOutput{get;set;}
      public Decimal? CostPrice{get;set;}
      public Decimal? RetailSalePrice{get;set;}
      public Decimal? WholesalePrice{get;set;}
      public Decimal? InternalSalePrice{get;set;}
      public Decimal? Margin{get;set;}
      public Decimal? DiscountInternal{get;set;}
      public Decimal? MinQuantity{get;set;}
      public Decimal? MaxQuantity{get;set;}
      public String? Status{get;set;}
      public String? CreatedUserID{get;set;}
      public DateTime? CreatedDate{get;set;}
      public String? UpdatedUserID{get;set;}
      public DateTime? UpdatedDate{get;set;}
    }
}
