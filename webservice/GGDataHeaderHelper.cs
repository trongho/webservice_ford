﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class GGDataHeaderHelper
    {
        public static List<GGDataHeaderModel> Covert(List<GGDataHeader> entrys)
        {
            var models = entrys.ConvertAll(sc => new GGDataHeaderModel
            {
                GGDNumber = sc.GGDNumber,
                GGDDate = sc.GGDDate,
                ReferenceNumber = sc.ReferenceNumber,
                WIRNumber = sc.WIRNumber,
                WIRReference = sc.WIRReference,
                HandlingStatusID = sc.HandlingStatusID,
                HandlingStatusName = sc.HandlingStatusName,
                Note = sc.Note,
                BranchID = sc.BranchID,
                BranchName = sc.BranchName,
                TotalQuantity = sc.TotalQuantity,
                TotalQuantityOrg = sc.TotalQuantityOrg,
                Status = sc.Status,
                CreatedUserID = sc.CreatedUserID,
                CreatedDate = sc.CreatedDate,
                UpdatedUserID = sc.UpdatedUserID,
                UpdatedDate = sc.UpdatedDate,
            });

            return models;
        }
    }
}
