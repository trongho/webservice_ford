﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Models
{
    public class ModalityModel
    {
        public Int16? Ordinal { get; set; }
        public String ModalityID { get; set; }
        public String? ModalityName { get; set; }
        public String? ModalityNameEN { get; set; }
        public String? Description { get; set; }
        public String? Type { get; set; }
        public String? Status { get; set; }
    }
}
