﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Models
{
    public class WRRHeaderModel
    {
        public String WRRNumber { get; set; }
        public DateTime? WRRDate { get; set; }
        public String? ReferenceNumber { get; set; }
        public String? HandlingStatusID { get; set; }
        public String? HandlingStatusName { get; set; }
        public String? Note { get; set; }
        public String? BranchID { get; set; }
        public String? BranchName { get; set; }
        public Decimal? TotalQuantity { get; set; }
        public String? Status { get; set; }
        public String? CreatedUserID { get; set; }
        public DateTime? CreatedDate { get; set; }
        public String? UpdatedUserID { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
