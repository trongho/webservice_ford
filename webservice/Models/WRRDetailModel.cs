﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Models
{
    public class WRRDetailModel
    {
        public String WRRNumber { get; set; }
        public int Ordinal { get; set; }
        public String GoodsID { get; set; }
        public String? GoodsName { get; set; }
        public String? OtherGoodsName { get; set; }
        public Decimal? PackingVolume { get; set; }
        public Decimal? TotalQuantity { get; set; }
        public Decimal? QuantityByItem { get; set; }
        public Decimal? QuantityByPack { get; set; }
        public String? LocationID { get; set; }
        public Int16? ScanOption { get; set; }
        public String? Note { get; set; }
        public String? Status { get; set; }
        public String? SupplierCode { get; set; }
        public String? ASNNumber { get; set; }
        public String? PackingSlip { get; set; }
        public Decimal? QuantityReceived { get; set; }
        public String? ReceiptStatus { get; set; }
        public String? SLPart { get; set; }
    }
}
