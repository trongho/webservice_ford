﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Models
{
    public class CountGoodsModel
    {
        public String LocationID { get; set; }
        public String GoodsID { get; set; }
        public String? GoodsName { get; set; }
        public Decimal? Quantity { get; set; }
        public String? Status { get; set; }
        public DateTime? CountDate { get; set; }
    }
}
