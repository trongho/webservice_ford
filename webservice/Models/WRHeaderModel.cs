﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Models
{
    public class WRHeaderModel
    {
        public String WRNumber { get; set; }
        public String? ModalityID { get; set; }
        public String? ModalityName { get; set; }
        public DateTime? WRDate { get; set; }
        public String? ReferenceNumber { get; set; }
        public String? WRRNumber { get; set; }
        public String? WRRReference { get; set; }
        public String? WarehouseID { get; set; }
        public String? WarehouseName { get; set; }
        public String? SupplierID { get; set; }
        public String? SupplierName { get; set; }
        public String? CustomerID { get; set; }
        public String? CustomerName { get; set; }
        public String? OrderNumber { get; set; }
        public DateTime? OrderDate { get; set; }
        public String? ContractNumber { get; set; }
        public DateTime? ContractDate { get; set; }
        public String? IONumber { get; set; }
        public String? PriceTypeID { get; set; }
        public String? PriceTypeName { get; set; }
        public String? PaymentTypeID { get; set; }
        public String? PaymentTypeName { get; set; }
        public String? HandlingStatusID { get; set; }
        public String? HandlingStatusName { get; set; }
        public String? BranchID { get; set; }
        public String? BranchName { get; set; }
        public String? LocalOrderType { get; set; }
        public Decimal? TotalDiscountAmount { get; set; }
        public Decimal? TotalVATAmount { get; set; }
        public Decimal? TotalAmountExcludedVAT { get; set; }
        public Decimal? TotalAmountIncludedVAT { get; set; }
        public Decimal? TotalAmount { get; set; }
        public Decimal? TotalQuantity { get; set; }
        public String? Note { get; set; }
        public String? CheckerID1 { get; set; }
        public String? CheckerName1 { get; set; }
        public String? CheckerID2 { get; set; }
        public String? CheckerName2 { get; set; }
        public String? Status { get; set; }
        public String? CreatedUserID { get; set; }
        public DateTime? CreatedDate { get; set; }
        public String? UpdatedUserID { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
