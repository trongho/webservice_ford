﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Models
{
    public class UserModel
    {
        public String UserID { get; set; }
        public String Role { get; set; }
        public String UserName { get; set; }
        public String? Adress { get; set; }
        public String? Position { get; set; }
        public String? BranchID { get; set; }
        public String? BranchName { get; set; }
        public String? Email { get; set; }
        public String LoginName { get; set; }
        public String? Password { get; set; }
        public String? PasswordSalt { get; set; }
        public String? Description { get; set; }
        public String? Status { get; set; }
        public String? CreatedUserID { get; set; }
        public DateTime? CreatedDate { get; set; }
        public String? UpdatedUserID { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool Active { get; set; }
        public bool Blocked { get; set; }
    }
}
