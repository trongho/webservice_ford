﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Models
{
    public class TSDataGeneralModel
    {
        public String? BranchID { get; set; }
        public String WarehouseID { get; set; }
        public String? WarehouseName { get; set; }
        public DateTime TallyDate { get; set; }
        public int Ordinal { get; set; }
        public String GoodsID { get; set; }
        public String? GoodsName { get; set; }
        public String? StockUnitID { get; set; }
        public Decimal? TallyPrice { get; set; }
        public Decimal? TallyQuantity { get; set; }
        public Decimal? TallyAmount { get; set; }
        public Decimal? LedgerQuantity { get; set; }
        public Decimal? LedgerAmount { get; set; }
        public Decimal? DifferenceQuantity { get; set; }
        public Decimal? DifferenceAmount { get; set; }
        public String? Status { get; set; }
    }
}
