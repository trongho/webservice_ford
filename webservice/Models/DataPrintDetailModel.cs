﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Models
{
    public class DataPrintDetailModel
    {
        public String DataPrintNumber { get; set; }
        public String GoodsID { get; set; }
        public int Ordinal { get; set; }
        public Decimal? Quantity { get; set; }
        public String? Status { get; set; }
        public String? DateString { get; set; }
    }
}
