﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Models
{
    public class WRDetailModel
    {
        public String WRNumber { get; set; }
        public int Ordinal { get; set; }
        public String GoodsID { get; set; }
        public String? GoodsName { get; set; }
        public String? ReceiptUnitID { get; set; }
        public Decimal? UnitRate { get; set; }
        public String? StockUnitID { get; set; }
        public Decimal? Quantity { get; set; }
        public Decimal? PriceIncludedVAT { get; set; }
        public Decimal? PriceExcludedVAT { get; set; }
        public Decimal? Price { get; set; }
        public Decimal? Discount { get; set; }
        public Decimal? DiscountAmount { get; set; }
        public Decimal? VAT { get; set; }
        public Decimal? VATAmount { get; set; }
        public Decimal? AmountExcludedVAT { get; set; }
        public Decimal? AmountIncludedVAT { get; set; }
        public Decimal? Amount { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public String? SerialNumber { get; set; }
        public String? Note { get; set; }
        public Decimal? QuantityReceived { get; set; }
        public Decimal? PriceIncludedVATReceived { get; set; }
        public Decimal? PriceExcludedVATReceived { get; set; }
        public Decimal? PriceReceived { get; set; }
        public Decimal? DiscountReceived { get; set; }
        public Decimal? DiscountAmountReceived { get; set; }
        public Decimal? VATReceived { get; set; }
        public Decimal? VATAmountReceived { get; set; }
        public Decimal? AmountExcludedVATReceived { get; set; }
        public Decimal? AmountIncludedVATReceived { get; set; }
        public Decimal? AmountReceived { get; set; }
        public Decimal? QuantityOrdered { get; set; }
        public Decimal? OrderQuantityReceived { get; set; }
        public Decimal? ReceiptQuantityIssued { get; set; }
        public Decimal? ReceiptQuantity { get; set; }
        public Decimal? ReceiptAmount { get; set; }
        public Decimal? AverageCost { get; set; }
        public String? Status { get; set; }
        public String? SupplierCode { get; set; }
        public String? ASNNumber { get; set; }
        public String? PackingSlip { get; set; }
    }
}
