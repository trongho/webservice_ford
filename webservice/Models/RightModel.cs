﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Models
{
    public class RightModel
    {
        public String RightID { get; set; }
        public int Ordinal { get; set; }
        public String RightName { get; set; }
        public String RightNameEN { get; set; }
    }
}
