﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Models
{
    public class DataPrintHeaderModel
    {
        public String DataPrintNumber { get; set; }
        public DateTime? DataPrintDate { get; set; }
        public String? Status { get; set; }
        public Decimal? TotalGoodsID { get; set; }
        public Decimal? TotalGoodsIDPrinted { get; set; }
        public Decimal? TotalLabel { get; set; }
        public Decimal? TotalLabelPrinted { get; set; }
        public String? CreatedUserID { get; set; }
        public DateTime? CreatedDate { get; set; }
        public String? UpdatedUserID { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public String? HandlingStatusID { get; set; }
        public String? HandlingStatusName { get; set; }
    }
}
