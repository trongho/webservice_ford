﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Models
{
    public class LocationModel
    {
        public String LocationID { get; set; }
        public String WarehouseID { get; set; }
        public String Description { get; set; }
        public String Status { get; set; }
        public String? CreatedUserID { get; set; }
        public DateTime? CreatedDate { get; set; }
        public String? UpdatedUserID { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
